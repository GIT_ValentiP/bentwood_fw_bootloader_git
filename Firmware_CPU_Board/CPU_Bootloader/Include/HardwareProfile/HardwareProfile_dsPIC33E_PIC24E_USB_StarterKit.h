// Copyright (c) 2002-2010,  Microchip Technology Inc.
//
// Microchip licenses this software to you solely for use with Microchip
// products.  The software is owned by Microchip and its licensors, and
// is protected under applicable copyright laws.  All rights reserved.
//
// SOFTWARE IS PROVIDED "AS IS."  MICROCHIP EXPRESSLY DISCLAIMS ANY
// WARRANTY OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING BUT
// NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  IN NO EVENT SHALL
// MICROCHIP BE LIABLE FOR ANY INCIDENTAL, SPECIAL, INDIRECT OR
// CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, HARM TO YOUR
// EQUIPMENT, COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY
// OR SERVICES, ANY CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED
// TO ANY DEFENSE THEREOF), ANY CLAIMS FOR INDEMNITY OR CONTRIBUTION,
// OR OTHER SIMILAR COSTS.
//
// To the fullest extent allowed by law, Microchip and its licensors
// liability shall not exceed the amount of fees, if any, that you
// have paid directly to Microchip to use this software.
//
// MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE
// OF THESE TERMS.	


#ifndef __HARDWAREPROFILE_PIC32MX_USB_STARTER_KIT_H__
#define __HARDWAREPROFILE_PIC32MX_USB_STARTER_KIT_H__



 	/*******************************************************************/
    /******** USB stack hardware selection options *********************/
    /*******************************************************************/
    //This section is the set of definitions required by the MCHPFSUSB
    //  framework.  These definitions tell the firmware what mode it is
    //  running in, and where it can find the results to some information
    //  that the stack needs.
    //These definitions are required by every application developed with
    //  this revision of the MCHPFSUSB framework.  Please review each
    //  option carefully and determine which options are desired/required
    //  for your application.



    /*******************************************************************/
    /*******************************************************************/
    /*******************************************************************/
    /******** Application specific definitions *************************/
    /*******************************************************************/
    /*******************************************************************/
    /*******************************************************************/

    /** Board definition ***********************************************/
    //These defintions will tell the main() function which board is
    //  currently selected.  This will allow the application to add
    //  the correct configuration bits as well use the correct
    //  initialization functions for the board.  These defitions are only
    //  required for this demo.  They are not required in
    //  final application design.


	
	#define mLED                LATAbits.LATA10//era LATDbits.LATD2
    #define mLED2               LATAbits.LATA4 
    #define mLED3               LATAbits.LATA5 
    #define mLED4               LATDbits.LATD8 
    #define mLED5               LATDbits.LATD9 
    #define mLED6               LATDbits.LATD11 

    #define BlinkLED1_fast()    (mLED = ((TMR1 & 0x2000) != 0))

	

    #define BlinkLED1()         (mLED = ((TMR1 & 0x8000) != 0))
	#define BlinkLED2()         (mLED2 = ((TMR1 & 0x8000) != 0))
    #define BlinkLED3()         (mLED3 = ((TMR1 & 0x8000) != 0))
    #define BlinkLED4()         (mLED4 = ((TMR1 & 0x8000) != 0))
    #define BlinkLED5()         (mLED5 = ((TMR1 & 0x8000) != 0))
    #define BlinkLED6()         (mLED6 = ((TMR1 & 0x8000) != 0))

    #define InitLED()           (TRISAbits.TRISA10 = 0)   					
	#define InitLED2()          (TRISAbits.TRISA4 = 0) 
    #define InitLED3()          (TRISAbits.TRISA5 = 0) 
    #define InitLED4()          (TRISDbits.TRISD8 = 0) 
    #define InitLED5()          (TRISDbits.TRISD9 = 0) 
    #define InitLED6()          (TRISDbits.TRISD11 = 0) 

	#define ReadSwitchStatus()  (PORTDbits.RD1)// dipswitch SW1
	
	#if defined(TRANSPORT_LAYER_USB_HOST)
	
		// ******************* MDD File System Required Definitions ********************
		// Select your MDD File System interface type
		// This library currently only supports a single physical interface layer
		// In this example we are going to use the USB so we only need the USB definition
		// *****************************************************************************
		#define USE_USB_INTERFACE               // USB host MSD library
	#endif
	
#endif
