/********************************************************************
                       @copyright Microchip 2011
********************************************************************/
#include "USB/usb.h"
#include "USB/usb_host_msd.h"
#include "USB/usb_host_msd_scsi.h"
#include "MDD File System/FSIO.h"
#include "usb_host_bootloader.h"
#include "NVMem.h"
#include <p24Exxxx.h>


 int FGS __attribute__((space(prog), address(0xF80004))) = 0xFFCF ;
//_FGS(
//    GWRP_OFF &           // General Segment Write-Protect bit (General Segment may be written)
//    GSS_OFF &            // General Segment Code-Protect bit (General Segment Code protect is disabled)
//    GSSK_OFF             // General Segment Key bits (General Segment Write Protection and Code Protection is Disabled)
//);
 int FOSCSEL __attribute__((space(prog), address(0xF80006))) = 0xFF7B ;
//_FOSCSEL(
//    FNOSC_PRIPLL &       // Initial Oscillator Source Selection bits (Primary Oscillator (XT, HS, EC) with PLL)
//    IESO_OFF             // Two-speed Oscillator Start-up Enable bit (Start up with user-selected oscillator source)
//);
 int FOSC __attribute__((space(prog), address(0xF80008))) = 0xFF9A ;
//_FOSC(
//    POSCMD_HS &          // Primary Oscillator Mode Select bits (HS Crystal Oscillator Mode)
//    OSCIOFNC_ON &        // OSC2 Pin Function bit (OSC2 is general purpose digital I/O pin)
//    IOL1WAY_OFF &        // Peripheral pin select configuration (Allow multiple reconfigurations)
//    FCKSM_CSDCMD         // Clock Switching Mode bits (Both Clock switching and Fail-safe Clock Monitor are disabled)
//);
 int FWDT __attribute__((space(prog), address(0xF8000A))) = 0xFF7F ;
//_FWDT(
//    WDTPOST_PS32768 &    // Watchdog Timer Postscaler bits (1:32,768)
//    WDTPRE_PR128 &       // Watchdog Timer Prescaler bit (1:128)
//    PLLKEN_ON &          // PLL Lock Wait Enable bit (Clock switch to PLL source will wait until the PLL lock signal is valid.)
//    WINDIS_OFF &         // Watchdog Timer Window Enable bit (Watchdog Timer in Non-Window mode)
//    FWDTEN_OFF           // Watchdog Timer Enable bit (Watchdog timer enabled/disabled by user software)
//);
 int FPOR __attribute__((space(prog), address(0xF8000C))) = 0xFFD7 ;
//_FPOR(
//    FPWRT_PWR128 &       // Power-on Reset Timer Value Select bits (128ms)
//    BOREN_OFF &          // Brown-out Reset (BOR) Detection Enable bit (BOR is disabled)
//    ALTI2C1_OFF &        // Alternate I2C pins for I2C1 (SDA1/SCK1 pins are selected as the I/O pins for I2C1)
//    ALTI2C2_OFF          // Alternate I2C pins for I2C2 (SDA2/SCK2 pins are selected as the I/O pins for I2C2)
//);
 //int FICD __attribute__((space(prog), address(0xF8000E))) = 0xFFDD ;   // parte dalla Primary Flash
 int FICD __attribute__((space(prog), address(0xF8000E))) = 0xFFD9 ;     // parte dalla Auxiliary Flash
//_FICD(
//    ICS_PGD3 &           // ICD Communication Channel Select bits (Communicate on PGC3/EMUC3 and PGD3/EMUD3)
//    RSTPRI_PF &          // Reset Target Vector Select bit (Device will obtain reset instruction from Primary flash)
//    JTAGEN_OFF           // JTAG Enable bit (JTAG is disabled)
//);
 int FAS __attribute__((space(prog), address(0xF80010))) = 0xFFCF ;
//_FAS(
//    AWRP_OFF &           // Auxiliary Segment Write-protect bit (Aux Flash may be written)
//    APL_OFF &            // Auxiliary Segment Code-protect bit (Aux Flash Code protect is disabled)
//    APLK_OFF             // Auxiliary Segment Key bits (Aux Flash Write Protection and Code Protection is Disabled)
//);//*/



/******************************************************************************
                      Macro da personalizzare
*******************************************************************************/
#define TIME_CHECK_BOOT                 10
#define FW_NAME_FILE                    "ktronic.hex"


/******************************************************************************
Macro
*******************************************************************************/
#define SWITCH_PRESSED 0
#define AUX_FLASH_BASE_ADRS				(0x7FC000)
#define AUX_FLASH_END_ADRS				(0x7FFFFF)
#define DEV_CONFIG_REG_BASE_ADDRESS 	(0xF80000)
#define DEV_CONFIG_REG_END_ADDRESS   	(0xF80012)

/******************************************************************************
Global Variables
*******************************************************************************/
FSFILE * myFile;
BYTE myData[512];
size_t numBytes;
volatile BOOL deviceAttached;
UINT pointer = 0;
UINT readBytes;
T_REC record;
unsigned char  asciiBuffer[1024];
unsigned char  asciiRec[200];
unsigned char  hexRec[100];
unsigned long int ProgAddress_test;
unsigned long int ProgAddress_test2;
unsigned char flag_valid_app=0;
unsigned char flag_usb_attached=0;
unsigned long int count_timer=0;
unsigned char flag_timer=0;
unsigned char test_fs_init=0;
unsigned char test_file_open=0; 
unsigned char test_erase_flash=0;
unsigned char error_valid_app=0;
unsigned char test_boot_1=0;
unsigned char test_boot_2=0;
unsigned char test_boot_3=0;
unsigned char test_boot_4=0;
unsigned char test_boot_5=0;

/****************************************************************************
Prototypes
*****************************************************************************/
void ConvertAsciiToHex(UINT8* asciiRec, UINT8* hexRec);
void Init_board(void);
void JumpToApp(void);
BOOL ValidAppPresent(void);
void ChetTimeoutBoot(void);


/****************************************************************************
 * FIRMWARE - MAIN
*****************************************************************************/

int main(void)
{
unsigned int i;
     
    Init_board();                                       // Inizializzazione periferiche board
    deviceAttached = FALSE;                             // Initializzazione variabile USB
    flag_valid_app=0;                                   // Inizializzazione variabile che mi dice se c'e' una applicazione valida
    count_timer=TIME_CHECK_BOOT;                        // Piu' alto metto questo parametro piu' tempo controllo prima di passare alla applicazione
    
	if(ValidAppPresent())                               // Controlla che ci sia una applicazione valida
    {
        flag_valid_app=1;                               // Lo segno su un flag 
    }
    USBInitialize(0);                                   // Initializzazion stack usb

    while(1)                                            // Infinite loop
    {
        USBTasks();                                     // USB stack process deve girare nel while
        if(USBHostMSDSCSIMediaDetect())                 // mi indica se la USB e' collegata alla scheda
        {
            deviceAttached = TRUE;                      // Setto la variabile che mi indica USB collegata
            count_timer=50;                             // Ridondante
            mLED2=1;                                    // DEBUG LED - accendo il led2
            
            if(FSInit())                                // Controllo se il dispositivo collegato e' nel formato corretto
            {
                myFile = FSfopen(FW_NAME_FILE,"r");     // Apro hex file
            
                if(myFile != NULL)                      // ricontrolo che il file non sia vuoto
                {
                    mLED3=1;                            // accendo il led
                    test_boot_3++;
#if 1                                                   // Mettere a 0 per debug USB senza il bootloader
                    EraseFlash();                       // Cancello il blocco di flash
                    
                    record.status = REC_NOT_FOUND;      // Initialize the state-machine to read the records.

                    while(1)                            // Passo alla programmazine dell'applicativo
                    {
                        USBTasks();                     // USB task deve girare 
 
        				BlinkLED6();                    // DEBUG LED - faccio lampeggiare il led 6
                                                        // Leggo 512 bytes alla volta e li metto su un buffer
                        readBytes = FSfread((void *)&asciiBuffer[pointer],1,512,myFile);
                        
                        if(readBytes == 0)
                        {                               // Nothing to read. Come out of this loop - break; - Jump to start of application
                                                        // Disable all enabled interrupts (only USB) before jumping to the application code.
                            FSfclose(myFile);           // Chiudo il file aperto
                            IEC5bits.USB1IE = 0;
                            
                            JumpToApp();                // Passo direttamente all'appliczione programmata
                            break;
                        }

                        for(i = 0; i < (readBytes + pointer); i ++)
                        {                               // This state machine seperates-out the valid hex records from the read 512 bytes.
                            switch(record.status)
                            {
                                case REC_FLASHED:
                                case REC_NOT_FOUND:
                                    if(asciiBuffer[i] == ':')
                                    {                   // Ho trovato nel buffer un record di 512 bytes
                                        record.start = &asciiBuffer[i];
                                        record.len = 0;
                                        record.status = REC_FOUND_BUT_NOT_FLASHED;
                                    }
                                    break;
                                case REC_FOUND_BUT_NOT_FLASHED:
                                    if((asciiBuffer[i] == 0x0A) || (asciiBuffer[i] == 0xFF))
                                    {                   // Ho un record completo (0x0A e' new line e 0xFF e' End of file)
                                                        // Start the hex conversion from element (: e' lo start del record hex)
                                        ConvertAsciiToHex(&record.start[1],hexRec);
                                        WriteHexRecord2Flash(hexRec);
                                        record.status = REC_FLASHED;
                                    }
                                    break;
                            }
                            record.len ++;              // Passo al prossimo byte nel buffer
                        }

                        if(record.status == REC_FOUND_BUT_NOT_FLASHED)
                        {
                                                        // We still have a half read record in the buffer. The next half part of the record is read 
                                                        // when we read 512 bytes of data from the next file read. 
                            memcpy(asciiBuffer, record.start, record.len);
                            pointer = record.len;
                            record.status = REC_NOT_FOUND;
                        }
                        else
                        {
                            pointer = 0;
                        }
                    }                                   // fine while(1)
#endif
                }                                       // fine if(myFile != NULL)
                else
                {
                    test_file_open=2;                   // Debug
                }
            }                                           // fine if(FSInit())
            else
            {
                test_fs_init=2;                         // Debug
            }
        }                                               // fine if(USBHostMSDSCSIMediaDetect())
        else                                            // ancora non ho visto la usb
        {
            ChetTimeoutBoot();                          // Non vedo USB - piccolo timeoutprima di passare all'applicaione valida (se presente))
        }
    }                                                   // fine while(1)
    return 0;
}


/********************************************************************
* Initializzazione scheda. Clock   60MIPS operation.
********************************************************************/
void Init_board(void)
{
	PLLFBD = 58;                                        // M=60  //OK      
    CLKDIVbits.PLLPOST = 0;                             // N1=2  //OK  
    CLKDIVbits.PLLPRE = 0;                              // N2=2  //OK  
    OSCTUN = 0;                                         // Tune FRC oscillator, if FRC is used  //OK  

    RCONbits.SWDTEN = 0;                                // Disable Watch Dog Timer
                                                        // Clock switching to incorporate PLL
    __builtin_write_OSCCONH(0x03);                      // Initiate Clock Switch to Primary  //OK  

                                                        // Oscillator with PLL (NOSC=0b011)
    __builtin_write_OSCCONL(0x01);                      // Start clock switching  //OK  
    while(OSCCONbits.COSC != 0b011);                    // OK  

    while(OSCCONbits.LOCK != 1)                         // Wait for Clock switch to occur
    { };                                                //	Wait for PLL to lock
    
	
    ACLKCON3 = 0x24C1;                                  // USB h/w initialization
    ACLKDIV3 = 0x7;                                     // OK  
    
    ACLKCON3bits.ENAPLL = 1;                            // OK  
    while(ACLKCON3bits.APLLCK != 1);                    // OK  
    
    ANSELBbits.ANSB5 = 0;                               // Power USB device.  
    TRISBbits.TRISB5 = 0;                               // check  
    LATBbits.LATB5 = 1;                                 // check  

    T1CONbits.TON = 0;                                  //Timer setup for LED blinking.
    T1CONbits.TGATE = 0;
    T1CONbits.TCKPS = 3;
    T1CONbits.TCS = 0;
    PR1 = 0xFFFF;
    T1CONbits.TON = 1;
    
    InitLED();                                          // Init leds
	InitLED2(); 
    InitLED3(); 
	InitLED4(); 
    InitLED5(); 
    InitLED6();  
}	

/********************************************************************
* Salto all'applicazione
********************************************************************/
void JumpToApp(void)
{	
	void (*fptr)(void);
	fptr = (void (*)(void))USER_APP_RESET_ADDRESS;     // 0x200 per torchio fw
	fptr();
}	

/********************************************************************
* controllo se saltare all'applicativo
********************************************************************/
void ChetTimeoutBoot(void)
{
    if((TMR1 & 0x8000) != 0)
    {
        if(flag_timer==0)
        {
            flag_timer=1;
            if(count_timer>0)
            {
                count_timer--; 
            }
        }
    }
    else
    {
        flag_timer=0;
    }

    if(count_timer==0)
    {
        if(flag_valid_app)
        {
            JumpToApp();                        // Vado all'applicazione 
        }
        else
        {
            BlinkLED1();                       // E' passato il tempo ma non ho applicazione valida Blink LED 1    
        }
    }
    else
    {
        BlinkLED1_fast();                       // Blink LED 1 veloce mentre aspetto timeout   
    }
}

/****************************************************************************
  Function:
    BOOL USB_ApplicationEventHandler( BYTE address, USB_EVENT event,
                void *data, DWORD size )

  Summary:
    This is the application event handler.  It is called when the stack has
    an event that needs to be handled by the application layer rather than
    by the client driver.

  Description:
    This is the application event handler.  It is called when the stack has
    an event that needs to be handled by the application layer rather than
    by the client driver.  If the application is able to handle the event, it
    returns TRUE.  Otherwise, it returns FALSE.

  Precondition:
    None

  Parameters:
    BYTE address    - Address of device where event occurred
    USB_EVENT event - Identifies the event that occured
    void *data      - Pointer to event-specific data
    DWORD size      - Size of the event-specific data

  Return Values:
    TRUE    - The event was handled
    FALSE   - The event was not handled

  Remarks:
    The application may also implement an event handling routine if it
    requires knowledge of events.  To do so, it must implement a routine that
    matches this function signature and define the USB_HOST_APP_EVENT_HANDLER
    macro as the name of that function.
  ***************************************************************************/

BOOL USB_ApplicationEventHandler( BYTE address, USB_EVENT event, void *data, DWORD size )
{
    switch( event )
    {
        case EVENT_VBUS_REQUEST_POWER:
            // The data pointer points to a byte that represents the amount of power
            // requested in mA, divided by two.  If the device wants too much power, we reject it.
            test_boot_1++;
            return TRUE;

        case EVENT_VBUS_RELEASE_POWER:
            deviceAttached = FALSE;                     // Turn off Vbus power. This means that the device was removed
            return TRUE;
            break;

        case EVENT_HUB_ATTACH:
            return TRUE;
            break;

        case EVENT_UNSUPPORTED_DEVICE:
            return TRUE;
            break;

        case EVENT_CANNOT_ENUMERATE:
            return TRUE;
            break;

        case EVENT_CLIENT_INIT_ERROR:
            return TRUE;
            break;

        case EVENT_OUT_OF_MEMORY:
            return TRUE;
            break;

        case EVENT_UNSPECIFIED_ERROR:                   // This should never be generated.
            return TRUE;
            break;

        default:
            break;
    }

    return FALSE;
}



/********************************************************************
* ConvertAsciiToHex()
********************************************************************/
void ConvertAsciiToHex(UINT8* asciiRec, UINT8* hexRec)
{
UINT8 i = 0;
UINT8 k = 0;
UINT8 hex;
	
	while((asciiRec[i] >= 0x30) && (asciiRec[i] <= 0x66))
	{
		if(asciiRec[i] < 0x3A)
		{
			hex = asciiRec[i] & 0x0F;
		}
		else
		{
			hex = 0x09 + (asciiRec[i] & 0x0F);						
		}

		k = i%2;
		if(k)
		{
			hexRec[i/2] |= hex;
		}
		else
		{
			hexRec[i/2] = (hex << 4) & 0xF0;
		}	
		i++;		
	}	
}

/********************************************************************
* Cancellazione blocco Flash
********************************************************************/
void EraseFlash(void)
{
	UINT result;
    			
	result = NVMemBlockErase();
                                                        // Assert on NV error. This must be caught during debug phase.
	while(result!=0);		           	     
}



/********************************************************************
* Scrittura record HEX su Flash
********************************************************************/
void WriteHexRecord2Flash(UINT8* HexRecord)
{
static T_HEX_RECORD HexRecordSt;
UINT8 Checksum = 0;
UINT i;
UINT32 WrData;
UINT32 ProgAddress;
UINT Result;

	HexRecord = &HexRecord[0];
	HexRecordSt.RecDataLen = HexRecord[0];
	HexRecordSt.RecType = HexRecord[3];	
	HexRecordSt.Data = &HexRecord[4];

	Checksum = 0;                                       // Hex Record checksum check.
	for(i = 0; i < HexRecordSt.RecDataLen + 5; i++)
	{
		Checksum += HexRecord[i];
	}	
	
    if(Checksum != 0)
    {
                                                        //Error. Hex record Checksum mismatch.
	} 
	else
	{                                                   // Hex record checksum OK.
		switch(HexRecordSt.RecType)
		{
			case DATA_RECORD:                           //Record Type 00, data record.
				HexRecordSt.Address.byte.MB = 0;
				HexRecordSt.Address.byte.UB = 0;
				HexRecordSt.Address.byte.HB = HexRecord[1];
				HexRecordSt.Address.byte.LB = HexRecord[2];
                                                        // Derive the address.
				HexRecordSt.Address.Val = HexRecordSt.Address.Val + HexRecordSt.ExtLinAddress.Val + HexRecordSt.ExtSegAddress.Val;
				
                ProgAddress_test=HexRecordSt.Address.Val;// DEBUG
                
				while(HexRecordSt.RecDataLen)           // Loop till all bytes are done.
				{                                       // Convert the Physical address to Virtual address. 
					ProgAddress = (HexRecordSt.Address.Val/2);
					ProgAddress_test2=ProgAddress;      // DEBUG
                                                        // Make sure we are not writing boot area and device configuration bits.
					if(((ProgAddress < AUX_FLASH_BASE_ADRS) || (ProgAddress > AUX_FLASH_END_ADRS))
					   && ((ProgAddress < DEV_CONFIG_REG_BASE_ADDRESS) || (ProgAddress > DEV_CONFIG_REG_END_ADDRESS)))
					{
						if(HexRecordSt.RecDataLen < 4)
						{                               // Sometimes record data length will not be in multiples of 4. Appending 0xFF will make sure that..
                                                        // we don't write junk data in such cases.
							WrData = 0xFFFFFFFF;
							memcpy(&WrData, HexRecordSt.Data, HexRecordSt.RecDataLen);	
						}
						else
						{	
							memcpy(&WrData, HexRecordSt.Data, 4);
						}		
                                                        // Write the data into flash.	
						Result = NVMemWriteWord(ProgAddress, WrData);	
                                                        // Assert on error. This must be caught during debug phase.		
						while(Result!=0);					
					}	
	                          
					HexRecordSt.Address.Val += 4;       // Increment the address.
					HexRecordSt.Data += 4;              // Increment the data pointer.
					if(HexRecordSt.RecDataLen > 3)      // Decrement data len.
					{
						HexRecordSt.RecDataLen -= 4;
					}	
					else
					{
						HexRecordSt.RecDataLen = 0;
					}	
				}
				break;
			
			case EXT_SEG_ADRS_RECORD:                   // Record Type 02, defines 4th to 19th bits of the data address.
			    HexRecordSt.ExtSegAddress.byte.MB = 0;
				HexRecordSt.ExtSegAddress.byte.UB = HexRecordSt.Data[0];
				HexRecordSt.ExtSegAddress.byte.HB = HexRecordSt.Data[1];
				HexRecordSt.ExtSegAddress.byte.LB = 0;
                                                       // Reset linear address.
				HexRecordSt.ExtLinAddress.Val = 0;
				break;
				
			case EXT_LIN_ADRS_RECORD:                   // Record Type 04, defines 16th to 31st bits of the data address. 
				HexRecordSt.ExtLinAddress.byte.MB = HexRecordSt.Data[0];
				HexRecordSt.ExtLinAddress.byte.UB = HexRecordSt.Data[1];
				HexRecordSt.ExtLinAddress.byte.HB = 0;
				HexRecordSt.ExtLinAddress.byte.LB = 0;
                                                        // Reset segment address.
				HexRecordSt.ExtSegAddress.Val = 0;
				break;
				
			case END_OF_FILE_RECORD:                    // Record Type 01, defines the end of file record.
			default: 
				HexRecordSt.ExtSegAddress.Val = 0;
				HexRecordSt.ExtLinAddress.Val = 0;
				break;
		}		
	}	
}	

/********************************************************************
* Controllo applicazione valida
********************************************************************/
BOOL ValidAppPresent(void)
{
	volatile DWORD AppPtr;
	
	TBLPAG = 0x00;

	AppPtr = ((DWORD)__builtin_tblrdh(USER_APP_RESET_ADDRESS) << 16) ;
	AppPtr = AppPtr | ((DWORD)__builtin_tblrdl(USER_APP_RESET_ADDRESS)) ;

	if(AppPtr == 0xFFFFFF)
	{
		return FALSE;
	}
	else
	{
		return TRUE;
	}
}			
/*********************End of File************************************/
