#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/Host_Boot.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/Host_Boot.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../../Source/USB/usb_config.c "../../Source/MDD File System/FSIO.c" ../../Source/USB/usb_host_msd_scsi.c ../../Source/USB/usb_host_msd.c ../../Source/USB/usb_host.c ../../Source/NVMem.c ../../Source/main.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/313519312/usb_config.o ${OBJECTDIR}/_ext/1382283020/FSIO.o ${OBJECTDIR}/_ext/313519312/usb_host_msd_scsi.o ${OBJECTDIR}/_ext/313519312/usb_host_msd.o ${OBJECTDIR}/_ext/313519312/usb_host.o ${OBJECTDIR}/_ext/1787047461/NVMem.o ${OBJECTDIR}/_ext/1787047461/main.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/313519312/usb_config.o.d ${OBJECTDIR}/_ext/1382283020/FSIO.o.d ${OBJECTDIR}/_ext/313519312/usb_host_msd_scsi.o.d ${OBJECTDIR}/_ext/313519312/usb_host_msd.o.d ${OBJECTDIR}/_ext/313519312/usb_host.o.d ${OBJECTDIR}/_ext/1787047461/NVMem.o.d ${OBJECTDIR}/_ext/1787047461/main.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/313519312/usb_config.o ${OBJECTDIR}/_ext/1382283020/FSIO.o ${OBJECTDIR}/_ext/313519312/usb_host_msd_scsi.o ${OBJECTDIR}/_ext/313519312/usb_host_msd.o ${OBJECTDIR}/_ext/313519312/usb_host.o ${OBJECTDIR}/_ext/1787047461/NVMem.o ${OBJECTDIR}/_ext/1787047461/main.o

# Source Files
SOURCEFILES=../../Source/USB/usb_config.c ../../Source/MDD File System/FSIO.c ../../Source/USB/usb_host_msd_scsi.c ../../Source/USB/usb_host_msd.c ../../Source/USB/usb_host.c ../../Source/NVMem.c ../../Source/main.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/Host_Boot.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=24EP512GU810
MP_LINKER_FILE_OPTION=,--script="..\pic24e.gld"
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/313519312/usb_config.o: ../../Source/USB/usb_config.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/313519312" 
	@${RM} ${OBJECTDIR}/_ext/313519312/usb_config.o.d 
	@${RM} ${OBJECTDIR}/_ext/313519312/usb_config.o.ok ${OBJECTDIR}/_ext/313519312/usb_config.o.err 
	@${RM} ${OBJECTDIR}/_ext/313519312/usb_config.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/313519312/usb_config.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DDEMO_BOARD_USB_STARTER_KIT -DTRANSPORT_LAYER_USB_HOST -I".." -I"../../Include" -I"../../Include/HardwareProfile" -I"../../Include/MDD File System" -I"../../Include/USB" -I"." -Os -MMD -MF "${OBJECTDIR}/_ext/313519312/usb_config.o.d" -o ${OBJECTDIR}/_ext/313519312/usb_config.o ../../Source/USB/usb_config.c    
	
${OBJECTDIR}/_ext/1382283020/FSIO.o: ../../Source/MDD\ File\ System/FSIO.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1382283020" 
	@${RM} ${OBJECTDIR}/_ext/1382283020/FSIO.o.d 
	@${RM} ${OBJECTDIR}/_ext/1382283020/FSIO.o.ok ${OBJECTDIR}/_ext/1382283020/FSIO.o.err 
	@${RM} ${OBJECTDIR}/_ext/1382283020/FSIO.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1382283020/FSIO.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DDEMO_BOARD_USB_STARTER_KIT -DTRANSPORT_LAYER_USB_HOST -I".." -I"../../Include" -I"../../Include/HardwareProfile" -I"../../Include/MDD File System" -I"../../Include/USB" -I"." -Os -MMD -MF "${OBJECTDIR}/_ext/1382283020/FSIO.o.d" -o ${OBJECTDIR}/_ext/1382283020/FSIO.o "../../Source/MDD File System/FSIO.c"    
	
${OBJECTDIR}/_ext/313519312/usb_host_msd_scsi.o: ../../Source/USB/usb_host_msd_scsi.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/313519312" 
	@${RM} ${OBJECTDIR}/_ext/313519312/usb_host_msd_scsi.o.d 
	@${RM} ${OBJECTDIR}/_ext/313519312/usb_host_msd_scsi.o.ok ${OBJECTDIR}/_ext/313519312/usb_host_msd_scsi.o.err 
	@${RM} ${OBJECTDIR}/_ext/313519312/usb_host_msd_scsi.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/313519312/usb_host_msd_scsi.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DDEMO_BOARD_USB_STARTER_KIT -DTRANSPORT_LAYER_USB_HOST -I".." -I"../../Include" -I"../../Include/HardwareProfile" -I"../../Include/MDD File System" -I"../../Include/USB" -I"." -Os -MMD -MF "${OBJECTDIR}/_ext/313519312/usb_host_msd_scsi.o.d" -o ${OBJECTDIR}/_ext/313519312/usb_host_msd_scsi.o ../../Source/USB/usb_host_msd_scsi.c    
	
${OBJECTDIR}/_ext/313519312/usb_host_msd.o: ../../Source/USB/usb_host_msd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/313519312" 
	@${RM} ${OBJECTDIR}/_ext/313519312/usb_host_msd.o.d 
	@${RM} ${OBJECTDIR}/_ext/313519312/usb_host_msd.o.ok ${OBJECTDIR}/_ext/313519312/usb_host_msd.o.err 
	@${RM} ${OBJECTDIR}/_ext/313519312/usb_host_msd.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/313519312/usb_host_msd.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DDEMO_BOARD_USB_STARTER_KIT -DTRANSPORT_LAYER_USB_HOST -I".." -I"../../Include" -I"../../Include/HardwareProfile" -I"../../Include/MDD File System" -I"../../Include/USB" -I"." -Os -MMD -MF "${OBJECTDIR}/_ext/313519312/usb_host_msd.o.d" -o ${OBJECTDIR}/_ext/313519312/usb_host_msd.o ../../Source/USB/usb_host_msd.c    
	
${OBJECTDIR}/_ext/313519312/usb_host.o: ../../Source/USB/usb_host.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/313519312" 
	@${RM} ${OBJECTDIR}/_ext/313519312/usb_host.o.d 
	@${RM} ${OBJECTDIR}/_ext/313519312/usb_host.o.ok ${OBJECTDIR}/_ext/313519312/usb_host.o.err 
	@${RM} ${OBJECTDIR}/_ext/313519312/usb_host.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/313519312/usb_host.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DDEMO_BOARD_USB_STARTER_KIT -DTRANSPORT_LAYER_USB_HOST -I".." -I"../../Include" -I"../../Include/HardwareProfile" -I"../../Include/MDD File System" -I"../../Include/USB" -I"." -Os -MMD -MF "${OBJECTDIR}/_ext/313519312/usb_host.o.d" -o ${OBJECTDIR}/_ext/313519312/usb_host.o ../../Source/USB/usb_host.c    
	
${OBJECTDIR}/_ext/1787047461/NVMem.o: ../../Source/NVMem.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1787047461" 
	@${RM} ${OBJECTDIR}/_ext/1787047461/NVMem.o.d 
	@${RM} ${OBJECTDIR}/_ext/1787047461/NVMem.o.ok ${OBJECTDIR}/_ext/1787047461/NVMem.o.err 
	@${RM} ${OBJECTDIR}/_ext/1787047461/NVMem.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1787047461/NVMem.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DDEMO_BOARD_USB_STARTER_KIT -DTRANSPORT_LAYER_USB_HOST -I".." -I"../../Include" -I"../../Include/HardwareProfile" -I"../../Include/MDD File System" -I"../../Include/USB" -I"." -Os -MMD -MF "${OBJECTDIR}/_ext/1787047461/NVMem.o.d" -o ${OBJECTDIR}/_ext/1787047461/NVMem.o ../../Source/NVMem.c    
	
${OBJECTDIR}/_ext/1787047461/main.o: ../../Source/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1787047461" 
	@${RM} ${OBJECTDIR}/_ext/1787047461/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1787047461/main.o.ok ${OBJECTDIR}/_ext/1787047461/main.o.err 
	@${RM} ${OBJECTDIR}/_ext/1787047461/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1787047461/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DDEMO_BOARD_USB_STARTER_KIT -DTRANSPORT_LAYER_USB_HOST -I".." -I"../../Include" -I"../../Include/HardwareProfile" -I"../../Include/MDD File System" -I"../../Include/USB" -I"." -Os -MMD -MF "${OBJECTDIR}/_ext/1787047461/main.o.d" -o ${OBJECTDIR}/_ext/1787047461/main.o ../../Source/main.c    
	
else
${OBJECTDIR}/_ext/313519312/usb_config.o: ../../Source/USB/usb_config.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/313519312" 
	@${RM} ${OBJECTDIR}/_ext/313519312/usb_config.o.d 
	@${RM} ${OBJECTDIR}/_ext/313519312/usb_config.o.ok ${OBJECTDIR}/_ext/313519312/usb_config.o.err 
	@${RM} ${OBJECTDIR}/_ext/313519312/usb_config.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/313519312/usb_config.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DDEMO_BOARD_USB_STARTER_KIT -DTRANSPORT_LAYER_USB_HOST -I".." -I"../../Include" -I"../../Include/HardwareProfile" -I"../../Include/MDD File System" -I"../../Include/USB" -I"." -Os -MMD -MF "${OBJECTDIR}/_ext/313519312/usb_config.o.d" -o ${OBJECTDIR}/_ext/313519312/usb_config.o ../../Source/USB/usb_config.c    
	
${OBJECTDIR}/_ext/1382283020/FSIO.o: ../../Source/MDD\ File\ System/FSIO.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1382283020" 
	@${RM} ${OBJECTDIR}/_ext/1382283020/FSIO.o.d 
	@${RM} ${OBJECTDIR}/_ext/1382283020/FSIO.o.ok ${OBJECTDIR}/_ext/1382283020/FSIO.o.err 
	@${RM} ${OBJECTDIR}/_ext/1382283020/FSIO.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1382283020/FSIO.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DDEMO_BOARD_USB_STARTER_KIT -DTRANSPORT_LAYER_USB_HOST -I".." -I"../../Include" -I"../../Include/HardwareProfile" -I"../../Include/MDD File System" -I"../../Include/USB" -I"." -Os -MMD -MF "${OBJECTDIR}/_ext/1382283020/FSIO.o.d" -o ${OBJECTDIR}/_ext/1382283020/FSIO.o "../../Source/MDD File System/FSIO.c"    
	
${OBJECTDIR}/_ext/313519312/usb_host_msd_scsi.o: ../../Source/USB/usb_host_msd_scsi.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/313519312" 
	@${RM} ${OBJECTDIR}/_ext/313519312/usb_host_msd_scsi.o.d 
	@${RM} ${OBJECTDIR}/_ext/313519312/usb_host_msd_scsi.o.ok ${OBJECTDIR}/_ext/313519312/usb_host_msd_scsi.o.err 
	@${RM} ${OBJECTDIR}/_ext/313519312/usb_host_msd_scsi.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/313519312/usb_host_msd_scsi.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DDEMO_BOARD_USB_STARTER_KIT -DTRANSPORT_LAYER_USB_HOST -I".." -I"../../Include" -I"../../Include/HardwareProfile" -I"../../Include/MDD File System" -I"../../Include/USB" -I"." -Os -MMD -MF "${OBJECTDIR}/_ext/313519312/usb_host_msd_scsi.o.d" -o ${OBJECTDIR}/_ext/313519312/usb_host_msd_scsi.o ../../Source/USB/usb_host_msd_scsi.c    
	
${OBJECTDIR}/_ext/313519312/usb_host_msd.o: ../../Source/USB/usb_host_msd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/313519312" 
	@${RM} ${OBJECTDIR}/_ext/313519312/usb_host_msd.o.d 
	@${RM} ${OBJECTDIR}/_ext/313519312/usb_host_msd.o.ok ${OBJECTDIR}/_ext/313519312/usb_host_msd.o.err 
	@${RM} ${OBJECTDIR}/_ext/313519312/usb_host_msd.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/313519312/usb_host_msd.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DDEMO_BOARD_USB_STARTER_KIT -DTRANSPORT_LAYER_USB_HOST -I".." -I"../../Include" -I"../../Include/HardwareProfile" -I"../../Include/MDD File System" -I"../../Include/USB" -I"." -Os -MMD -MF "${OBJECTDIR}/_ext/313519312/usb_host_msd.o.d" -o ${OBJECTDIR}/_ext/313519312/usb_host_msd.o ../../Source/USB/usb_host_msd.c    
	
${OBJECTDIR}/_ext/313519312/usb_host.o: ../../Source/USB/usb_host.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/313519312" 
	@${RM} ${OBJECTDIR}/_ext/313519312/usb_host.o.d 
	@${RM} ${OBJECTDIR}/_ext/313519312/usb_host.o.ok ${OBJECTDIR}/_ext/313519312/usb_host.o.err 
	@${RM} ${OBJECTDIR}/_ext/313519312/usb_host.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/313519312/usb_host.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DDEMO_BOARD_USB_STARTER_KIT -DTRANSPORT_LAYER_USB_HOST -I".." -I"../../Include" -I"../../Include/HardwareProfile" -I"../../Include/MDD File System" -I"../../Include/USB" -I"." -Os -MMD -MF "${OBJECTDIR}/_ext/313519312/usb_host.o.d" -o ${OBJECTDIR}/_ext/313519312/usb_host.o ../../Source/USB/usb_host.c    
	
${OBJECTDIR}/_ext/1787047461/NVMem.o: ../../Source/NVMem.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1787047461" 
	@${RM} ${OBJECTDIR}/_ext/1787047461/NVMem.o.d 
	@${RM} ${OBJECTDIR}/_ext/1787047461/NVMem.o.ok ${OBJECTDIR}/_ext/1787047461/NVMem.o.err 
	@${RM} ${OBJECTDIR}/_ext/1787047461/NVMem.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1787047461/NVMem.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DDEMO_BOARD_USB_STARTER_KIT -DTRANSPORT_LAYER_USB_HOST -I".." -I"../../Include" -I"../../Include/HardwareProfile" -I"../../Include/MDD File System" -I"../../Include/USB" -I"." -Os -MMD -MF "${OBJECTDIR}/_ext/1787047461/NVMem.o.d" -o ${OBJECTDIR}/_ext/1787047461/NVMem.o ../../Source/NVMem.c    
	
${OBJECTDIR}/_ext/1787047461/main.o: ../../Source/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1787047461" 
	@${RM} ${OBJECTDIR}/_ext/1787047461/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1787047461/main.o.ok ${OBJECTDIR}/_ext/1787047461/main.o.err 
	@${RM} ${OBJECTDIR}/_ext/1787047461/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1787047461/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -DDEMO_BOARD_USB_STARTER_KIT -DTRANSPORT_LAYER_USB_HOST -I".." -I"../../Include" -I"../../Include/HardwareProfile" -I"../../Include/MDD File System" -I"../../Include/USB" -I"." -Os -MMD -MF "${OBJECTDIR}/_ext/1787047461/main.o.d" -o ${OBJECTDIR}/_ext/1787047461/main.o ../../Source/main.c    
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/Host_Boot.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    ../pic24e.gld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -omf=elf -mcpu=$(MP_PROCESSOR_OPTION)  -D__DEBUG  -o dist/${CND_CONF}/${IMAGE_TYPE}/Host_Boot.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}         -Wl,--defsym=__MPLAB_BUILD=1,--heap=2048,--stack=1024,-L"..",-L".",-Map="${DISTDIR}/Host_Boot.X.${IMAGE_TYPE}.map",--report-mem$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1
else
dist/${CND_CONF}/${IMAGE_TYPE}/Host_Boot.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   ../pic24e.gld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -omf=elf -mcpu=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/Host_Boot.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}         -Wl,--defsym=__MPLAB_BUILD=1,--heap=2048,--stack=1024,-L"..",-L".",-Map="${DISTDIR}/Host_Boot.X.${IMAGE_TYPE}.map",--report-mem$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION)
	${MP_CC_DIR}\\pic30-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/Host_Boot.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} -omf=elf
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
