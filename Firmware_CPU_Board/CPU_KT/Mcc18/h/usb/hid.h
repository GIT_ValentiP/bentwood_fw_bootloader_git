/*********************************************************************
 *
 *             Microchip USB C18 Firmware -  HID Version 1.0
 *
 *********************************************************************
 * FileName:        hid.h
 * Dependencies:    See INCLUDES section below
 * Processor:       PIC18
 * Compiler:        C18 2.30.01+
 * Company:         Microchip Technology, Inc.
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Rawin Rojvanit       19/11/04    Original.
 * Dario Greggio	      28/02/05    .
 ********************************************************************/
#ifndef HID_H
#define HID_H

/** I N C L U D E S **********************************************************/
#include <typedefs.h>

/** D E F I N I T I O N S ****************************************************/

/* Class-Specific Requests */
#define GET_REPORT      0x01
#define GET_IDLE        0x02
#define GET_PROTOCOL    0x03
#define SET_REPORT      0x09
#define SET_IDLE        0x0A
#define SET_PROTOCOL    0x0B

/* Class Descriptor Types */
#define DSC_HID         0x21
#define DSC_RPT         0x22
#define DSC_PHY         0x23

// HID report types      --  Microchip should add these definitions to usb_function_hid.h
#define HID_REPORT_TYPE_INPUT         0x01
#define HID_REPORT_TYPE_OUTPUT        0x02
#define HID_REPORT_TYPE_FEATURE       0x03
//http://www.microchip.com/forums/tm.aspx?m=492439&mpage=1&key=&#493632

/* Protocol Selection */
#define BOOT_PROTOCOL   0x00
#define RPT_PROTOCOL    0x01


/* HID Interface Class Code */
#define HID_INTF                    0x03

/* HID Interface Class SubClass Codes */
#define BOOT_INTF_SUBCLASS          0x01

/* HID Interface Class Protocol Codes */
#define HID_PROTOCOL_NONE           0x00
#define HID_PROTOCOL_KEYBOARD       0x01
#define HID_PROTOCOL_MOUSE          0x02
#define HID_PROTOCOL_KEYBOARD_MMEDIA1       0x07
#define HID_PROTOCOL_KEYBOARD_MMEDIA2       0x0C

/******************************************************************************
 * Macro:           (bit) mHIDRxIsBusy(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        This macro is used to check if HID OUT endpoint is
 *                  busy (owned by SIE) or not.
 *                  Typical Usage: if(mHIDRxIsBusy())
 *
 * Note:            None
 *****************************************************************************/
#define mHIDRxIsBusy()              HID_BD_OUT.Stat.UOWN
#define mHIDRx2IsBusy()             HID_BD_OUT_2.Stat.UOWN

/******************************************************************************
 * Macro:           (bit) mHIDTxIsBusy(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        This macro is used to check if HID IN endpoint is
 *                  busy (owned by SIE) or not.
 *                  Typical Usage: if(mHIDTxIsBusy())
 *
 * Note:            None
 *****************************************************************************/
#define mHIDTxIsBusy()              HID_BD_IN.Stat.UOWN
#ifdef USE_2_IN_ENDPOINT
#define mHIDTx2IsBusy()             HID_BD_IN_2.Stat.UOWN
#endif

/******************************************************************************
 * Macro:           byte mHIDGetRptRxLength(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          mHIDGetRptRxLength returns hid_rpt_rx_len
 *
 * Side Effects:    None
 *
 * Overview:        mHIDGetRptRxLength is used to retrieve the number of bytes
 *                  copied to user's buffer by the most recent call to
 *                  HIDRxReport function.
 *
 * Note:            None
 *****************************************************************************/
#define mHIDGetRptRxLength()        hid_rpt_rx_len
#define mHIDGetRptRx2Length()        hid_rpt_rx_len2

/** S T R U C T U R E S ******************************************************/
typedef struct _USB_HID_DSC_HEADER {
  byte bDscType;
  word wDscLength;
	} USB_HID_DSC_HEADER;

typedef struct _USB_HID_DSC {
  byte bLength;
	byte bDscType; 
	word bcdHID;
  byte bCountryCode;
	byte bNumDsc;
  USB_HID_DSC_HEADER hid_dsc_header[HID_NUM_OF_DSC];
  /*
   * HID_NUM_OF_DSC is defined in autofiles\usbcfg.h
   */
	} USB_HID_DSC;

/** E X T E R N S ************************************************************/
extern byte hid_rpt_rx_len;
#ifdef USE_2_OUT_ENDPOINT
extern byte hid_rpt_rx_len2;
#endif

/** P U B L I C  P R O T O T Y P E S *****************************************/
void HIDInitEP(void);
void USBCheckHIDRequest(void);
void HIDTxReport(byte *buffer, byte len);
#ifdef USE_2_IN_ENDPOINT
void HIDTxReport2(byte *buffer, byte len);
#endif
byte HIDRxReport(byte *buffer, byte len);
#ifdef USE_2_OUT_ENDPOINT
byte HIDRxReport2(byte *buffer, byte len);
#endif
void GetInputReport0(void);
void GetFeatureReport0(void);
void SetOutputReport0(void);
void SetFeatureReport0(void);
void HandleControlOutReport(void);

#endif //HID_H
