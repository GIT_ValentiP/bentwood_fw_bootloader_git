/**
  Communication keyboards File

  @Company
    K-TRONIC

  @File Name
    Communication_Keyboards.c

  @Summary
    This is the C file regarding the management of serial monitor

  @Description
    This C file provides APIs for the management of serial monitor.
    Generation Information :
        Product Revision  :  PIC18 MCUs - 1.76
        Device            :  PIC24EP512GU810
        Driver Version    :  1.01
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.00
        MPLAB 	          :  MPLAB X 5.10
*/


//#include "mcc_generated_files/mcc.h"
#include "loads.h"
#include "Driver_MV/pin_manager.h"
#include "Driver_MV/tmr1.h"
#include "Driver_MV/tmr2.h"
#include "MainDemo.h"
#include "LCD.h"
#include "driver_MV/uart1.h"
#include "swi2c.h"
#include "Communication_Keyboards.h"

#define _DEMO  

extern flagManuale;

extern unsigned short count_rst_keyb; 
extern unsigned char flag_rst_keyb;
extern unsigned short refresh_keyboard_read; 
extern UART_OBJECT uart1_obj ;
extern short int cap1;
extern short int cap_pre;
extern short cap_direzione;
unsigned char Comm_State=0;
unsigned char Flag_EnableComm=0;
unsigned char val =  'F';
unsigned char old_val = 'E';
unsigned short Vread = 0;
BYTE test;
unsigned long int i =0 ;
extern int ultimoCaffe;
extern char BUTTON[10];
extern char BUTTON_OLD[10];
extern flg_setting;
extern int premuti;
extern BYTE KeyCollaudati;
extern BYTE start_calibrazione;

void EnableComm(unsigned char status){
    
    Flag_EnableComm = status;
}

/**/
void reset_keyboard(){
    
    LATBbits.LATB5 =0;
    TRISBbits.TRISB5=0;		//reset KEYBOARD MASTER-CLEAR OOUTPUT    
    
    __delay_ms(100);
    LATBbits.LATB5 =1;    
}


void calibrazione(void){
   
    
    UART1_Write('J');
}

void ripristina_fabbrica(){
    UART1_Write('K');
}


void mnu_collaudo_tasti_mec2(){
    BYTE iy=-1;
    switch(val){
        case '9':
            iy=1+2;    
            KeyCollaudati=KeyCollaudati | 0x01;
        break;
        case '1':    
            iy=2+2;
            KeyCollaudati=KeyCollaudati | 0x02;
        break;
        case '2':    
            iy=3+2;
            KeyCollaudati=KeyCollaudati | 0x04;
        break;
        case '3':    
            iy=4+2;
            KeyCollaudati=KeyCollaudati | 0x08;
        break;
        case '4':    
            iy=5+2;
            KeyCollaudati=KeyCollaudati | 0x10;
        break;
        case '5':    
            iy=6+2;
            KeyCollaudati=KeyCollaudati | 0x20;
        break;  
        case '6':    
            iy=7+2;
            KeyCollaudati=KeyCollaudati | 0x40;
        break;
        case '7':    
            iy=8+2;
            KeyCollaudati=KeyCollaudati | 0x80;
        break;
        
      
        
            
    }
    Disable_ALL_Led_White();
    Disable_ALL_Led_Blue();
    Enable_Led(val,'1');
    
    
    if(iy>0){
        LCDPutStringXY(10,iy,"  ");
        __delay_ms(50);   
        LCDPutStringXY(10,iy,"OK");
    }
}
void mnu_collaudo_tasti_mec(){
    BYTE iy;
    char appoggio[10];
    
    LCDCls();
    for(iy=1;iy<9;iy++){
        sprintf(appoggio,"%s%u:","TASTO_",iy);
        LCDPutStringXY(2,iy+2,appoggio);
        
    }
    KeyCollaudati=0;
}

void reset_capacitivo(void){
    __delay_ms(100);
    
    start_calibrazione='0';
    Disable_CapSense();
    LCDCls();
    reset_keyboard();
                    
    if(Comm_State==2){
    // LCDPutStringCenter(4,"RESETTO1");
       Config_LED();
       Comm_State=1;
    }else{
       //   LCDPutStringCenter(4,"RESETTO0");
            Comm_State=0;
    }
}

void CommStart(void)
{
    extern BOOL collaudo;
    extern BOOL collaudo_cap;
    extern BOOL running;
    extern BYTE capAttivo;
    extern BYTE ultimoCapAttivo;
   // int _i1;
    extern int salti;
    int c;
    //char a[3] ="00";
    //unsigned char found= 0;
    char appoggio[30];     
    
  //      val = U1RXREG;
   
        if(flag_rst_keyb && start_calibrazione != '1'){
           flag_rst_keyb=0;
           count_rst_keyb=0;
           
          // LCDPutStringCenter(4,"           ");  
                            
           reset_keyboard();                          

           if(Comm_State==2){
               //LCDPutStringCenter(4,"RESETTO1");
               //__delay_ms(500);
               Config_LED();
               Comm_State=1;
           }else{
               //LCDPutStringCenter(4,"RESETTO0");
               //__delay_ms(500);
               Comm_State=0;
           }
        }else{
                if((flag_15sec==1) && (start_calibrazione == '1')){
                    flag_15sec=0;
                    start_calibrazione='0';
                    LCDCls();
                    LCDPutStringCenter(7,"RESTART");
                    __delay_ms(500);
                    capAttivo=0;
                    reset_capacitivo();
                }
        }
         
		switch(Comm_State) {
			case 0: 
                val = UART1_Read();
				switch(val) 
                {
                    //printf("\nOK"); 
					case 'Y':        
                            Config_LED();
                            if(collaudo==FALSE){
                                for(i=0;i < sizeof(BUTTON);i++){
                                    Enable_Led((char)(i+'0'),'0');
                                    Enable_Led((char)( sizeof(BUTTON)-1-i+'0'),'1');
                                }
                            }
                            else{
                                    Config_LED();
                                    sequenzaBW();
                                    mnu_collaudo_tasti_mec();
                            }
                            Comm_State = 1;
                        
                    break;
 
                    default:
                        Comm_State = 0;
                      
				} 
            break;

			case 1:
                LATAbits.LATA2 = 1;
            //    #ifndef DEBUG_FAB
            //    UART1_Enable();
            //    #endif

                  Enable_Mec_Button();
                  
                  if(collaudo==FALSE){
                    if(capAttivo!=ultimoCapAttivo){    
                      if(capAttivo==1){
                          Enable_CapSense();
                          Disable_ALL_Led_White();
                      } 
                      else{
                          Disable_CapSense();
                          Disable_ALL_Led_Blue();
                          Enable_ALL_Led_White();
                      }
                      ultimoCapAttivo=capAttivo;
                   }else{
                        if(capAttivo==0){
                              Disable_ALL_Led_Blue();
                              Enable_ALL_Led_White();
                        }
                   }  
                 }
                //Disable_ALL_Led_White();
               // TMR1_Stop(); //reset all timer
                Comm_State = 2;

            break;
            
            case 2: // Button Reading    
             
                
                  if ((refresh_keyboard_read==1 && uart1_obj.rxStatus.s.full==TRUE)){
                    /*PER DEMO POI DA TOGLIERE */             
                      
                    #ifdef _DEMO
                        if(capAttivo!=ultimoCapAttivo){        	
                            if(capAttivo==1){
                                Enable_CapSense();
                                Enable_Mec_Button();
                                Disable_ALL_Led_White();
                            } 
                            else{
                                Disable_CapSense();
                                Disable_ALL_Led_Blue();
                                Enable_ALL_Led_White();
                            }
                            ultimoCapAttivo=capAttivo; 
                            old_val=val;
                        }
                   #endif

                    switch(buff_obj.type) 
                    {
                        case 'E':
                            LCDPutStringCenter(10,"WAIT");                        
                            
                                
                        break;
                        case '<':
                              start_calibrazione='0';
                              LCDPutStringCenter(10,"  END  ");
    
                              capAttivo=0;
                              reset_capacitivo();
                            
                             /* __delay_ms(100);
                              LCDCls();
                              Disable_CapSense();
                              ultimoCapAttivo=0;                          
                              capAttivo=0;*/
                              //reset keyboard*/
                        break;
                            
                        case 'C':
                             #ifdef _DEMO

                               
                                
                                
                                cap_pre=cap1;
                              
                              
                                sprintf(appoggio, "%u", buff_obj.button);
                                cap1=atoi(appoggio);
                                
                                BUTTON[2]='1';
                                BUTTON[6]='1';
                                
                                
                                
                                if(cap1==cap_pre){ 
                                    //sprintf(appoggio,"%c" ,cap_direzione);
                                    //LCDPutStringXY(4,8,appoggio);
                                    //proseguo l'andamento
                                    if(cap_direzione=='+')
                                        BUTTON[6]='0';
                                    else{ 
                                           if(cap_direzione=='-')
                                                BUTTON[2]='0';
                                    }
                                } else{
                                if(cap1>cap_pre){
                                    
                                    if(cap1==2048 && cap_pre==1){
                                       // LCDPutStringXY(4,8, "decr  ");
                                        BUTTON[2]='0';
                                        cap_direzione='-';
                                    }else{
                                       // LCDPutStringXY(4,8, "incr1  ");
                                        BUTTON[6]='0';
                                        cap_direzione='+';
                                    }                                    
                                }else
                                    if(cap_pre==2048 && cap1==1){                                        
                                        //LCDPutStringXY(4,8, "incr2  ");
                                        BUTTON[6]='0';     
                                        cap_direzione='+';
                                    }
                                    else{
                                        //LCDPutStringXY(4,8, "decr  ");
                                        BUTTON[2]='0';
                                        cap_direzione='-';                                        
                                    }                                    
                                }
                                
                          /*      if(ultimoCaffe==2){
                                   Enable_Led('4','1');
                                   
                                   
                                   
                                   
                                }else{
                                        if(ultimoCaffe==1){
                                           
                                           Enable_Led('5','1');
                                        }
                                }*/
                                
                              //  salti=abs(log10f(cap1)/log10f(2)-log10f(cap_pre)/log10f(2));
                                salti=1;
                      
                            #endif   
                        break;        
                        case 'W': //means mec button
                         
                          /*  if(start_calibrazione=='1'){
                                 start_calibrazione='0';
                             
                              LCDCls();
                              reset_capacitivo();
                              return;
                            }*/
                            
                            if(buff_obj.button & 0x1){
                                BUTTON[0]='0';
                                premuti++;
                                old_val=val;
                                val='0';
                            }
                            else
                                BUTTON[0]='1';

                            if(buff_obj.button & 0x2){
                                BUTTON[1]='0';
                                premuti++;
                                old_val=val;
                                val='1';
                            }
                            else
                                BUTTON[1]='1';

                            if(buff_obj.button & 0x4){
                                BUTTON[2]='0';
                                premuti++;
                                old_val=val;
                                val='2';            
                            }
                            else
                                BUTTON[2]='1';

                            if(buff_obj.button & 0x8){
                                BUTTON[3]='0';
                                premuti++;
                                old_val=val;
                                val='3';
                            }
                            else
                                BUTTON[3]='1';
                               
                            if(buff_obj.button & 0x10){
                                BUTTON[4]='0';
                                premuti++;  
                                old_val=val;
                                val='4';
                            }
                            else
                                BUTTON[4]='1';

                            if(buff_obj.button & 0x20){
                                BUTTON[5]='0';
                                premuti++;
                                old_val=val;
                                val='5';
                                
                            }
                            else
                                BUTTON[5]='1';


                            if(buff_obj.button & 0x40){
                                BUTTON[6]='0';
                                premuti++;
                                old_val=val;
                                val='6';
                            }
                            else
                                BUTTON[6]='1';


                            if(buff_obj.button & 0x80){
                                BUTTON[7]='0';
                                old_val=val;
                                premuti++;
                                val='7'; 
                            }
                            else
                                BUTTON[7]='1';

                            if(buff_obj.button & 0x100){
                                BUTTON[8]='0';
                                premuti++;
                                old_val=val;
                                val='8';
                            }
                            else
                                BUTTON[8]='1';    

                            if(buff_obj.button & 0x200){
                                BUTTON[9]='0';
                                premuti++;
                                old_val=val;
                                val='9';
                            }
                            else
                                BUTTON[9]='1';     
                       
                            if(buff_obj.button==0){
                                premuti=0;
                                val='-';
                            }

                        
                           // buff_obj.button = 0x0000;
                            //buff_obj.chKsum = 0;    

                        break;   
                        case 'F':
                        break;
                    }
                    uart1_obj.rxStatus.s.full = false;
                    refresh_keyboard_read=0;
                }
                
                
                if(collaudo==TRUE)  {
                    
                    if((KeyCollaudati != 0xFF)&& (collaudo_cap== FALSE))
                        mnu_collaudo_tasti_mec2();
                    else if (collaudo_cap== FALSE) {
                            collaudo_cap=TRUE;
                            LCDPutStringXY(2,13,"TASTI MECC COLLAUDATI");
                            Disable_ALL_Led_White();
                            Enable_CapSense(); 
                        }
                        
                        
                    gestioneMenu();
                    
                    
                    
                }
                else{
                        #ifdef _DEMO  
                            //sprintf(appoggio,"val:%c  cap:%u",val,capAttivo);
                            //LCDPutStringXY(3,9,appoggio);

                            if((val=='1') && (capAttivo==0)){
                                ultimoCapAttivo=capAttivo;
                                //capAttivo=(capAttivo+1)%2;
                                capAttivo=1;

                                LCDCls();
                            }else{  
                                    if((val=='7') && (capAttivo==1)){  
                                        ultimoCapAttivo=capAttivo;                                
                                        capAttivo=0;
                                        reset_azione();
                                    }
                                    else{ 
                                        
                                         if((BUTTON[2]=='0') && (BUTTON[6]=='0')&& (capAttivo==1) && buff_obj.type=='W'){
                                             //RIPRISTINO FABBRICA
                                             LCDCls();
                                             LCDPutStringCenter(4,"RESTORE ");
                                             ripristina_fabbrica();
                                             capAttivo=0;
                                             Delay_mS(100);
                                             LCDCls();
                                             BUTTON[2]=='1';
                                             BUTTON[6]=='1';
                                             reset_capacitivo();
                                             return;
                                         }
                                         else{
                                                if((start_calibrazione!='1') &&(BUTTON[2]=='0') && (BUTTON[1]=='0')&& (capAttivo==1) && buff_obj.type=='W'){
                                                   
                                                    Enable_ALL_Led_Blue();
                                                    LCDCls();
                                                    LCDPutStringCenter(4,"CALIBRATION ");
                                                    LCDPutStringCenter(5,"RUNNING");
                                                    LCDPutStringCenter(7,"DO NOT TOUCH");
                                                    LCDPutStringCenter(8,"THE KEYBOARD");
                                                    __delay_ms(250);
                                                    start_calibrazione='1';
                                                    flag_15sec=0;
                                                    count15=0;
                                                    ultimoCaffe=0;
                                                    Enable_ALL_Led_Blue();
                                                    calibrazione();
                                                    BUTTON[2]=BUTTON[1]='1';
                                                    BUTTON_OLD[2]=BUTTON_OLD[1]=0; 
                                                    
                                                    return;
                                                }else{
                                                       if(flg_setting==FALSE)
                                                           gestioneMenu();
                                                       else{ //setup

                                                           loadImpostazioni();  
                                                       }
                                                }
                                         }
                                         
                                        
                    
                                    }                         
                                 } 
                        #else
                            if(flg_setting==FALSE)
                               gestioneMenu();
                            else //setup
                               loadImpostazioni();
                        #endif                    
                    
                }
                    

                  
            break;
        }
      
        
}

void Enable_Led(unsigned char index, unsigned char type){
    
    //type='0' = blue
    //type='1' = white
    
    UART1_Write('S');
    UART1_Write(index);
    UART1_Write('D');
    UART1_Write('D');
    UART1_Write('D');
    UART1_Write('D');
    UART1_Write('o');
    UART1_Write(type);
    UART1_Write('H');
}

void Disable_Led(unsigned char index, unsigned char type){
    
    //type='0' = blue
    //type='1' = white
    
    UART1_Write('S');
    UART1_Write(index);
    UART1_Write('0');
    UART1_Write('0');
    UART1_Write('0');
    UART1_Write('0');
    UART1_Write('o');
    UART1_Write(type);
    UART1_Write('H');    
}

void Config_LED(void){
    //white
    UART1_Write('G');
    UART1_Write('2');
    UART1_Write('1');
    UART1_Write('i');
    UART1_Write('1');
    UART1_Write('H');
    
    //Blue
    UART1_Write('G');
    UART1_Write('2');
    UART1_Write('1');
    UART1_Write('i');
    UART1_Write('0');
    UART1_Write('H');
    
}

void Enable_ALL_Led_White(void){
    UART1_Write('N');
    UART1_Write('F');
    UART1_Write('F');
    UART1_Write('F');
    UART1_Write('F');
    UART1_Write('p');
    UART1_Write('1');
    UART1_Write('H');
}

void Enable_ALL_Led_Blue(void){
    UART1_Write('N');
    UART1_Write('F');
    UART1_Write('F');
    UART1_Write('F');
    UART1_Write('F');
    UART1_Write('p');
    UART1_Write('0');
    UART1_Write('H');
}

void Disable_ALL_Led_White(void){
    
    UART1_Write('N');
    UART1_Write('0');
    UART1_Write('0');
    UART1_Write('0');
    UART1_Write('0');
    UART1_Write('p');
    UART1_Write('1');
    UART1_Write('H');
    
}

void Disable_ALL_Led_Blue(void){
    
    UART1_Write('N');
    UART1_Write('0');
    UART1_Write('0');
    UART1_Write('0');
    UART1_Write('0');
    UART1_Write('p');
    UART1_Write('0');
    UART1_Write('H');
}

void Hys_CapSense(unsigned char val){
    char hex[2];
    sprintf(hex, "%x", val);
    
    UART1_Write('W');
    UART1_Write(hex[0]);
    UART1_Write(hex[1]);
    UART1_Write('H');
}

void Disable_CapSense(void){
    UART1_Write('C');
    UART1_Write('0');
}

void Enable_CapSense(void){
    UART1_Write('C');
    UART1_Write('1');
}

void Enable_Mec_Button(void){
    UART1_Write('M');
    UART1_Write('a');
}

void Disable_Mec_Button(void){
  UART1_Write('M');
  UART1_Write('b');
}

void Enable_Only_Mec_ButtonX(char tasto){
    UART1_Write('M');
    UART1_Write(tasto);
}