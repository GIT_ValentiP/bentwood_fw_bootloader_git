// FileName:        delay_func.c
// Dependencies:    delay.h
// Processor:       PIC24FJ128GA010 
// Hardware:        Explorer 16 demo board
// Complier:        MPLAB C30 v3.30b
/***********************************************************************
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the �Company�) for its dsPIC30F Microcontroller is intended 
 * and supplied to you, the Company�s customer, for use solely and
 * exclusively on Microchip's dsPIC30F Microcontroller products. 
 * The software is owned by the Company and/or its supplier, and is
 * protected under applicable copyright laws. All rights are reserved.
 * Any use in violation of the foregoing restrictions may subject the
 * user to criminal sanctions under applicable laws, as well as to
 * civil liability for the breach of the terms and conditions of this
 * license.
 *
 * THIS SOFTWARE IS PROVIDED IN AN �AS IS� CONDITION. NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
 * TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
 * IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
 * CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * Author               Date           Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Eugen Ionescu        10/12/2011     Initial release for LCD & push-buttons suport
 *
 **********************************************************************************/
#include "delay.h"
unsigned int temp_count;         // general delay counter 
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// 									Basic Delay functions
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void delayus(unsigned int del_us){        // delay expressed in microseconds
temp_count=del_us;                        // initialize the delay counter 
asm volatile ("start: dec _temp_count");
asm volatile ("repeat #11")     ;         // repeat next instruction for((Fosc/2)-5):5 asm instructions*1Ck=5Ck
asm volatile ("nop")            ;         // Fosc = 32 MHz (PLL = ON); 1 us = 32 * (1/32 MHz)
asm volatile ("cp0 _temp_count");
asm volatile ("bra nz,start")   ;
asm volatile ("end:");          
}

void delayms(unsigned int del_ms){        // delay expressed in miliseconds 
temp_count=del_ms;                        // initialize the delay counter 
asm volatile ("start1: dec _temp_count");
asm volatile ("repeat #16000")          ; // Fosc = 8*4=32 MHz (PLL = ON) 
asm volatile ("nop")                    ;                     
asm volatile ("cp0 _temp_count")        ;
asm volatile ("bra nz,start1")          ;
asm volatile ("end1:");                 }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// 								High level delay funcions
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void delay200us(void){  // 200 usec delay   
delayus(200);        }

void delay1_5ms(void){  // 1.5 msec delay 
delayus(1500);       }

void delay5ms(void)  {  // 5.0 msec delay 
delayms(5);          }

void delay15ms(void) {  // 15  msec delay 
delayms(15);         }

void delay1s(void)   {  // 1 second delay 
delayms(1000);       }

