/**
  @Generated PIC24 / dsPIC33 / PIC32MM MCUs Source File

  @Company:
    Microchip Technology Inc.

  @File Name:
    clock.c

  @Summary:
    This is the clock.c file generated using PIC24 / dsPIC33 / PIC32MM MCUs

  @Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC24 / dsPIC33 / PIC32MM MCUs - 1.95-b-SNAPSHOT
        Device            :  PIC24EP128GP206
    The generated drivers are tested against the following:
        Compiler          :  XC16 v1.36
        MPLAB             :  MPLAB X v5.10
*/

/*
    (c) 2016 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

#include <stdint.h>
#include "../HardwareProfile.h"
#include "clock.h"

void CLOCK_Initialize(void)
{
    OSCTUN=0;	//(da USB)
    /* 
     * [Lavoro a 100MHz per ora (v .H anche, v. anche il blocco per dsPIC sopra) ]
     * [(215*7.37)/(2*8*2) = 100~]
     * (280*8)/(8*2) = 140
     * forse meglio cos�? 
     */
    PLLFBD = 68; // M = PLLFBD + 2 = 70
    CLKDIVbits.PLLPRE = 0; // N1 = 2
    CLKDIVbits.PLLPOST = 0; // N2 = 2
            
    //OSCCON = 0x3300;    // Enable secondary oscillator TOLTO (x prova) lo 0x0002; uso PWM per dargli input!
    CLKDIV = 0x0000;    // Set PLL prescaler (1:1)
    //    __builtin_write_OSCCONH((unsigned char) (0x02));
    //    __builtin_write_OSCCONL((unsigned char) (0x00));
    //    CLKDIV = 0x3140;

	/*
     * Wait for the Primary PLL to lock and then
     * configure the auxilliary PLL to provide 48MHz needed for USB
     * Operation.
     */
    while(OSCCONbits.LOCK != 1) ClrWdt();

    /* per USB
     * Configuring the auxiliary PLL, since the primary oscillator provides 
     * the source clock to the auxiliary PLL, the auxiliary oscillator is 
     * disabled. Note that the AUX PLL is enabled. 
     * The input 8MHz clock is divided by 2, multiplied by 24 and then 
     * divided by 2. Wait till the AUX PLL locks.
     */

    ACLKCON3 = 0x24c1 /*0x24C1*/;		// 8MHz input, diviso 2 e diviso 2
    ACLKDIV3 = 0x7;						// x24, ossia in tutto 48MHz!


    ACLKCON3bits.ENAPLL = 1;
    while(ACLKCON3bits.APLLCK != 1) ClrWdt();
}
