/******************************************************************************
 * FileName:        MainDemo.c
 * Dependencies:    see included files below
 * Processor:       PIC24F, PIC24H, dsPIC, PIC32
 * Compiler:        C30 v3.25/C32 v0.00.18
 * Company:         Microchip Technology, Inc.
 * Software License Agreement
 *
 * Copyright (c) 2011 Microchip Technology Inc.  All rights reserved.
 * Microchip licenses to you the right to use, modify, copy and distribute
 * Software only when embedded on a Microchip microcontroller or digital
 * signal controller, which is integrated into your product or third party
 * product (pursuant to the sublicense terms in the accompanying license
 * agreement).  
 *C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h/p24Exxxx.h
 * You should refer to the license agreement accompanying this Software
 * for additional information regarding your rights and obligations.
 *
 *******************************************************************************/
/************************************************************
 * Includes
 ************************************************************/
#include <ctype.h>
#include <stdlib.h>
#include "MainDemo.h"

#include "Graphics/SSD1963.h"
// http://www.techtoys.com.hk/Displays/SSD1963EvalRev3B/SSD1963%20Eval%20Board%20Rev3B.htm


#include "ppsnew.h"
#include "Graphics\gfxpmp.h"
#include "sd-spi.h"
#include "swi2c.h"
#include <math.h>
#include <libpic30.h>

#include "usb_config.h"
#include "usb.h"
#include "usb_common.h"
#include "usb_host_msd.h"
#include "usb_host_msd_scsi.h"
#include "FSIO.h"
#include "USBFSIO.h"
#include "EEFSIO.h"

#include "boot_config.h"
#include "driver_MV/uart1.h"
#include "driver_MV/clock.h"
#include "driver_MV/pin_manager.h"
#include "driver_MV/tmr1.h"
#include "driver_MV/tmr2.h"
#include "Communication_Keyboards.h"
#include "LCD.h"
#define __DEBUG_PROJECT
/************************************************************
 * Configuration Bits
 ************************************************************/
_FGS( GWRP_OFF & GSS_OFF & GSSK_OFF ) 
_FOSCSEL(FNOSC_PRIPLL & IESO_OFF)			//FNOSC_FRCPLL 
_FOSC( POSCMD_HS & OSCIOFNC_ON & IOL1WAY_ON & FCKSM_CSDCMD )
#ifdef __DEBUG_PROJECT
   _FWDT( WDTPOST_PS32768 & WDTPRE_PR32 & PLLKEN_ON & WINDIS_OFF & FWDTEN_OFF )
#else        
  _FWDT( WDTPOST_PS32768 & WDTPRE_PR32 & PLLKEN_ON & WINDIS_OFF & FWDTEN_ON )
#endif        
//_FWDT(
//    WDTPOST_PS32768 &    // Watchdog Timer Postscaler bits (1:32,768)
//    WDTPRE_PR32 &        // Watchdog Timer Prescaler bit (1:32)
//    PLLKEN_ON &          // PLL Lock Wait Enable bit (Clock switch to PLL source will wait until the PLL lock signal is valid.)
//    WINDIS_OFF &         // Watchdog Timer Window Enable bit (Watchdog Timer in Non-Window mode)
//    FWDTEN_ON            // Watchdog Timer Enable bit (Watchdog timer always enabled)
_FPOR( FPWRT_PWR8 & BOREN_ON & ALTI2C1_OFF & ALTI2C2_ON )
_FICD( ICS_PGD3 & RSTPRI_PF & JTAGEN_OFF )
_FAS( AWRP_OFF & APL_OFF & APLK_OFF)
//_FUID0(x) simpa..

void SendLedState(char *);

char dimCarattere;
char Index;  
char BUTTON[10]= "1111111111";
char BUTTON_OLD[10] = "1111111111";
char LED_CMD[10] = "2222222222";
char Completed;
char StateRx;
char step;
char FirstPacket;

/*FABRIZIO*/
unsigned short DelayAfter;
float CycleTime, Cycle_1Tazza = 35.5,Cycle_2Tazza = 70.5,unita_Tazza=1;
unsigned char flag_nok_sd = 0, flag_ok_press = 0;
unsigned short _sottoMenuAndata,_sottoMenuRitorno,_sottoMenu=0;
unsigned short __anno,__mese,__giorno,__cursore,__ore,__minuti,__secondi,__ix=0;
BYTE start_calibrazione='0';
int _cursore=0,_cursorePrec=-1;
int premuti=0;
bool flg_setting = FALSE;
int salti=1;
int __massimo=4000;
int __minimo=2019;
int periodoPostVibro=POSTVIBRO;
int flgInterrotto=-1;
int ultimoPremuto=-1;
int ultimoCaffe=0;
unsigned short count_down = 0;
unsigned short count_temp = 0; 
unsigned short checkMan = 0;
unsigned short ok_macin_manual = 0;
unsigned short ok_macin_auto = 0;
unsigned short int FiltroCheck = 0;
unsigned short start_timer_macin_manual = 0;
BOOL refreshed=TRUE;
BOOL running=FALSE;
BOOL stopVibroPost=FALSE;
BOOL flagManuale = FALSE;
BOOL flagAutomatico = FALSE;
char returnMenu[20];
extern UART_OBJECT uart1_obj ;
short int cap1;
short int cap_pre;
short cap_direzione;
/*FINE FABRIZIO*/


//******************** REMEDIA ***************************************

#define BMP_USB_TO_SD   "0.BMP" //"lingue.csv"//"0.BMP"

FSFILE * myFile_usb;
FSFILE * myFile_sd;
unsigned char step_transfer=0;
unsigned char count_timeout_transfer=0;
int character;
unsigned int l=0;
unsigned char check_update_sd(void);
//*********************************************************************   



const char CopyrString[]= {'O','A',' ','(','X','X','X','X','X','X','X','X','X',' ','(','X','X','X','X',')',')',' ','-',' ','K','-','T','r','o','n','i','c',' ','-',' ','B','X','X','X','w','X','X','X',' '
	,
#ifdef USA_BOOTLOADER 
	' ','B','L',
#endif
#ifdef USA_ETHERNET
	' ','E','T','H',
#endif
	' ','v',VERNUMH+'0','.',VERNUML/10+'0',(VERNUML % 10)+'0',' ',
	'1','4','/','0','3','/','1','9', 0 };


/************************************************************
 * Defines
 ************************************************************/
#define	PutBmp8BPPExt(a,b,c,d) 	PutBmp8BPPExtFast(a,b,c,d)
#define VIBRO_PWM (95)
#define WAIT_UNTIL_FINISH(x)    while(!x)
#define DEMODELAY				1000
#define KBclear() {oldkbKeys[0]=kbKeys[0]=0;}
#define TRIGK 1

#define PWM_SPEED 50
#define PWM_PERIOD    (40000)    /* equivale a 1.7KHz */

#define RTCC_INTERRUPT_REGISTER IEC3
#define RTCC_INTERRUPT_VALUE    0x2000



/************************************************************
 * Variables
 ************************************************************/
WORD curPosX=0,curPosY=0;
BYTE LCDX=0,LCDY=0,savedX=0,savedY=0;
BYTE kbKeys[MAX_TASTI_CONTEMPORANEI],oldkbKeys[MAX_TASTI_CONTEMPORANEI];
BYTE rowState[32];		//5x5 matrice; ora 10 tasti meccanici + 12 tasti touch letti da SPI
WORD FLAGS=0;		//b0=TRIGK, b7=inCtrl, b6=inShift, b5=cursorState, b8=inSetup
BYTE /*inSetup=FALSE,*/SDcardOK=FALSE;
BYTE menuLevel=0,currSchermata=SCHERMATA_SPLASH,currSchermItem=0,enterData=0,extendedMenu=0;
BYTE deviceAttached;
struct CONFIG_PARMS configParms;

char kbTempBuffer[4];

unsigned char DC = 0; // Dario
unsigned char RX_CH = 'T'; //0x55;

const char *string_disable="DISABLE",*string_enable="ENABLE";
const char *MENU_LINGUE="lingue.csv",*MENU_INI="menu.ini",*SETTINGS_INI="settings.ini",*SETTINGS2_INI="settings2.ini",*FW_UPDATE=BOOT_FILE_NAME,*MENU_UPDATE="NEWMENUS";
const BYTE *myFont=NULL;
BYTE backLight=0;

WORD textColors[16]={BLACK,BRIGHTRED,BRIGHTGREEN,BRIGHTYELLOW,BRIGHTBLUE,BRIGHTMAGENTA,BRIGHTCYAN,WHITE,DARKGRAY,RED,GREEN,YELLOW,BLUE,MAGENTA,CYAN,LIGHTGRAY};
enum COLORI_TESTO { _TESTO_NERO,_TESTO_ROSSO,_TESTO_VERDE,_TESTO_GIALLO,_TESTO_BLU,_TESTO_MAGENTA,_TESTO_CIANO,_TESTO_BIANCO,_TESTOGRIGIO,_TESTO_ROSSOSCURO,_TESTO_VERDESCURO,_TESTO_GIALLOSCURO,_TESTO_BLUSCURO,_TESTO_MAGENTASCURO,_TESTO_CIANOSCURO,_TESTO_GRIGIOSCURO};
WORD fontColor,bkColor,defaultFontColor=BLACK,defaultBackColor=WHITE;			// sotto i default vengono riassegnati
BYTE _backcolor;	

BYTE State=0;			// enum 
/*FABRIZIO*/
Menu mnu;
BYTE currMenu=0;
BYTE statoPWM[2];
WORD tempoOperazione=0;
BYTE tipoOperazione=0;
BYTE capAttivo=0;
BYTE ultimoCapAttivo=1;
BOOL collaudo=FALSE;
BOOL collaudo_cap=FALSE;
BYTE KeyCollaudati=0;

WORD myOutChar(XCHAR ch) {		// (NO non-weak) per aggiungere mirror in RAM! e per gestire font ridefiniti
	int i,x;
// con questa siamo a 1.6mSec per char circa - 13/10/14



	SetArea(GetX(),GetY(),GetX()+getCharSizeX()-1,GetY()+getCharSizeY()-1);

//fare check, e cancellare il "sotto" solo se � cambiato??
	x=getSizeX();
	i=LCDX+LCDY*x;

	if(!(FLAGS & (1 << 8))) {		// per gestire sovrapposizione setup..

	}

//  SetColor((lcdAttrib[i] & 1) ? BLACK : WHITE);

	return subOutChar(ch);

	}

WORD subOutChar(XCHAR ch) {
  int i;
  WORD  temp;
  WORD  mask, restoremask;
  SHORT xCnt, yCnt, xFontSize, yFontSize;
  WORD *theFont;

  restoremask = 0x8000 ; // & 128
    
	i=getSizeX();
	i=LCDX+LCDY*i;

	if(myFont==font32) {
		yFontSize=16;
		xFontSize=16;
		theFont=(WORD *)&font32;
		theFont+=((ch-' ')*32);
		}
	else {
		yFontSize=16;
		xFontSize=12;
		theFont=(WORD *)&font16;
		theFont+=((ch-' ')*16);
		}

	WriteCommand(CMD_WR_MEMSTART);
	DisplayEnable();
	DisplaySetData(); 

  for(yCnt=0; yCnt<yFontSize; yCnt++) 
  {
      mask = 0;

      for(xCnt=0; xCnt<xFontSize; xCnt++) 
      {
        if(!mask) 
        {		// 
          temp = *theFont++;
				  temp=(LOBYTE(temp) << 8) | HIBYTE(temp);
          mask = restoremask;
        }
			//REVERSE NERO 
// provo con PutPixel ottimizzata, spostando setarea all'inizio

// Dario
        if(temp & mask) 
			     SetColor(WHITE); //SetColor(WHITE);
			  else
			     SetColor(BLACK); //SetColor(BLACK);

				WriteColor(_color);
              
        mask >>= 1;
      }
  }

		DisplayDisable();


  return 1;
	}

WORD subOutCharTransparent(XCHAR ch) {
  int i,x,y;
  WORD  temp;
  WORD  mask, restoremask;
  SHORT xCnt, yCnt, xFontSize, yFontSize;
	WORD *theFont;


 
  restoremask = 0x8000; // 
    
	i=getSizeX();
	i=LCDX+LCDY*i;

	if(myFont==font32) {
		yFontSize=32;
		xFontSize=16;
		theFont=(WORD *)&font32;
		theFont+=((ch-' ')*32);
		}
	else {
		yFontSize=16;
		xFontSize=12;
		theFont=(WORD *)&font16;
		theFont+=((ch-' ')*16);
		}

  y = GetY();
  for(yCnt=0; yCnt<yFontSize; yCnt++) {
    mask = 0;
    x = GetX();

    for(xCnt=0; xCnt<xFontSize; xCnt++) {
      if(!mask) {		// 
        temp = *theFont++;
				temp=(LOBYTE(temp) << 8) | HIBYTE(temp);
        mask = restoremask;
        }
			
// provo con PutPixel ottimizzata, spostando setarea all'inizio
      if(temp & mask) 
			  PutPixel(x,y);
              
      mask >>= 1;
			x++;
      }
		y++;
    }

  return 1;
	}

WORD OutCharRenderRAM(XCHAR ch, BYTE *theFont,BYTE reverse) {
  BYTE        temp;
  BYTE        mask;
  BYTE  restoremask;
  SHORT xCnt, yCnt, xFontSize, yFontSize;
    
  restoremask = 0x80; // POI PASSARE a word ?

// STRIMINZISCO per mancanza RAM! poi rimettere a posto (anche redef char)    
		yFontSize=8 /*16*/;
		xFontSize=6 /*12*/;

  if(IsDeviceBusy())
    return (0);

	WriteCommand(CMD_WR_MEMSTART);
	DisplayEnable();
	DisplaySetData(); 

  for(yCnt=0; yCnt<yFontSize; yCnt++) {
    mask = 0;

    for(xCnt=0; xCnt<xFontSize; xCnt++) {
      if(!mask) {		// 
        temp = *theFont;
        mask = restoremask;
        }

			if(reverse) {
// o si potrebbe plottare sia neri che bianchi, evitando di sbianchettare prima... per� poi salta il superimpose con la grafica
	      if(temp & mask) 
				  SetColor(BLACK);
				else
				  SetColor(WHITE);

					WriteColor(_color);
//2x in orizzontale...
					WriteColor(_color);
				}
			else {
	      if(temp & mask) 
				  SetColor(WHITE);
				else
				  SetColor(BLACK);

					WriteColor(_color);
//2x in orizzontale...
					WriteColor(_color);
        }
            
      mask >>= 1;
      }

//... e 2x in verticale...
    mask = 0;

    for(xCnt=0; xCnt<xFontSize; xCnt++) {
      if(!mask) {		// 
        temp = *theFont++;
        mask = restoremask;
        }

			if(reverse) {
// o si potrebbe plottare sia neri che bianchi, evitando di sbianchettare prima... per� poi salta il superimpose con la grafica
	      if(temp & mask) 
				  SetColor(BLACK);
				else
				  SetColor(WHITE);

					WriteColor(_color);
//2x in orizzontale...
					WriteColor(_color);
				}
			else {
	      if(temp & mask) 
				  SetColor(WHITE);
				else
				  SetColor(BLACK);

					WriteColor(_color);
//2x in orizzontale...
					WriteColor(_color);
        }
            
      mask >>= 1;
      }

    }


  // move cursor
//  _cursorX = x - (!configParms.screenSize ? 8 : 6) /*boh...*/;
				DisplayDisable();

  return 1;
	}

void OutCharCursor(BYTE ch) {
  SHORT xCnt, yCnt, x, y, xFontSize, yFontSize;

int i;

	ch+=155;


	{
  WORD temp;
  WORD mask,restoremask;
	const WORD *theFont;

//fondamentalmente copiata da outchar e outcharrenderRAM, ma duplicata per i cursori
	if(myFont==font32) {
		yFontSize=32;
		xFontSize=16;
		theFont=(WORD *)&font32;
		theFont+=((ch-' ')*32);
		}
	else {
		yFontSize=16;
		xFontSize=12;
		theFont=(WORD *)&font16;
		theFont+=((ch-' ')*16);
		}

  restoremask = 0x8000;

  y = curPosY;
  for(yCnt=0; yCnt<yFontSize; yCnt++) {
    x = curPosX;
    mask = 0;

    for(xCnt=0; xCnt<xFontSize; xCnt++) {
      if(!mask) {		// 
        temp = *theFont++;
				temp=(LOBYTE(temp) << 8) | HIBYTE(temp);
        mask = restoremask;
        }
			
      if(temp & mask) {
        PutPixel(x, y);
        }
              
      mask >>= 1;
      x++;
      }
    y++;
    }
	}
	}

void LCDPutChar(BYTE ch) {

  MoveTo(curPosX,curPosY);
	if(FLAGS & (1 << 4))
		drawCursor(0);
  switch(ch) {
		case CR:
			curPosX=GetRealOrgX(); LCDX=0;
			break;
		case LF:
			curPosY+=getCharSizeY();	LCDY++;
			// questo � SEMPRE 
			if(LCDY>=getSizeY()) {
//				LCDscrollY();
				LCDXY(LCDX,getSizeY()-1);
				}
			break;
		default:
			if(ch>=32 && ch<127) {
//			  SetColor(attribGrafici.reverse ? BLACK : WHITE);
//			  SetFont(!configParms.screenSize ? (void *)&Font15 : (void *)&Font30);
				myOutChar(ch);
printed:
				curPosX+=getCharSizeX();
				LCDX++;
				if(LCDX>=getSizeX()) {
					curPosX=GetRealOrgX();
					LCDX=0;
					curPosY+=getCharSizeY();
					LCDY++;
					if(LCDY>=getSizeY()) {
//							LCDscrollY();
						LCDXY(LCDX,getSizeY()-1);
						}
					}		//LCDX
				else if(LCDY==getSizeY() && LCDX==1) {		
//					LCDscrollY();
					LCDXY(0,getSizeY()-1);
					myOutChar(ch);
					LCDXY(1,getSizeY()-1);
					}
				}
			else if(ch>=128) {
//			  SetColor(attribGrafici.reverse ? BLACK : LIGHTGRAY);
//			  SetFont(!configParms.screenSize ? (void *)&Font15 : (void *)&Font30);
				myOutChar(ch);
				goto printed;
				}

			break;
		}
	}

void LCDPutString(const char *s) {

	if(FLAGS & (1 << 4))
		drawCursor(0);
	while(*s) {
	  MoveTo(curPosX,curPosY);
	  switch(*s) {
			case CR:
				curPosX=GetRealOrgX(); LCDX=0;
				break;
			case LF:
				curPosY+=getCharSizeY();	LCDY++;
				// questo � SEMPRE 
				if(LCDY>=getSizeY()) {
//					LCDscrollY();
					LCDXY(LCDX,getSizeY()-1);
					}
				break;
			default:
				if(((BYTE)*s)>=32 && ((BYTE)*s)<127) {
//				  SetColor(attribGrafici.reverse ? BLACK : WHITE);
//				  SetFont(!configParms.screenSize ? (void *)&Font15 : (void *)&Font30);
			  	myOutChar(*s);
printed:
					curPosX+=getCharSizeX();
					LCDX++;
					if(LCDX>=(getSizeX())) {
						curPosX=GetRealOrgX();
						LCDX=0;
						curPosY+=getCharSizeY();
						LCDY++;
						if(LCDY>=getSizeY()) {
//								LCDscrollY();
							LCDXY(LCDX,getSizeY()-1);
							}
						}		//LCDX
					else if(LCDY==getSizeY() && LCDX==1) {		
//						LCDscrollY();
						LCDXY(0,getSizeY()-1);
						myOutChar(*s);
						LCDXY(1,getSizeY()-1);
						}
					}
				else if(((BYTE)*s)>=128) {
//				  SetColor(attribGrafici.reverse ? BLACK : LIGHTGRAY);
//				  SetFont(!configParms.screenSize ? (void *)&Font15 : (void *)&Font30);
					myOutChar((BYTE)*s);									// 
					goto printed;
					}

				break;
			}
		s++;
		}
	}

void LCDCls(void) {
    SetColor(BLACK);
	ClearDevice();
	LCDHome();
	}

void LCDHome(void) {
	curPosX=GetRealOrgX();
	curPosY=GetRealOrgY();
	LCDX=LCDY=0;
	}

void LCDXY(BYTE x,BYTE y) {

	if(x>=getSizeX())
		x=getSizeX()-1;
	if(y>=getSizeY())
		y=getSizeY()-1;
	curPosX=GetRealOrgX()+(x*getCharSizeX());
	curPosY=GetRealOrgY()+(y*getCharSizeY());
  MoveTo(curPosX,curPosY);
	LCDX=x; LCDY=y;
	}

void LCDXYGraph(WORD x,WORD y) {			// da usare solo per label semplice

	if(x>=GetMaxX())
		x=GetMaxX()-1;
	if(y>=GetMaxY())
		y=GetMaxY()-1;
	curPosX=GetRealOrgX()+x;
	curPosY=GetRealOrgY()+y;
     MoveTo(curPosX,curPosY);
	LCDX=x/getCharSizeX(); LCDY=y/getCharSizeY();			// tanto per rimanere allineati!
	}

void LCDPutStringXY(BYTE x, BYTE y, const char *s) {
	
	LCDXY(x,y);
	LCDPutString(s);
	}

void LCDPutStringCenter(BYTE y,const char *text) {
  SHORT  width;
  char separatore='_';
  int xFontSize;
  int pos;
  char * p;
  char *buf1;
  
  
  if(myFont==font32) 
	xFontSize=16;
  else 
	xFontSize=12;
		
  
    p = strchr(text, separatore);
                
    pos=0;
    while(p!=NULL){
         
                
        pos = (int)(p - text);
        buf1=(char*)malloc(sizeof(char)* (pos + 1));
        strncpy(buf1,text,pos);
        buf1[pos]=NULL;
        LCDPutStringXY(((DISP_HOR_RESOLUTION/xFontSize) - pos) / 2, y, buf1);
        text=text+pos+1;
        
        p = strchr(text, separatore);
        pos++;
        free(buf1);
        *buf1=0;
        y=y+1;
    }
//	SetFont((void *)font);
//  width = GetTextWidth(text, (void *)font);
   width = strlen(text);

  LCDPutStringXY(((DISP_HOR_RESOLUTION/xFontSize) - width) / 2, y, text);
}

void drawCursor(BYTE m) {
	BYTE n;

	if(FLAGS & (1<<6) && FLAGS & (1<<7)) {
		n=4;
		}
	else if(FLAGS & (1<<6)) {
		n=5;
		}
	else if(FLAGS & (1<<7)) {
		n=3;
		}	
	else {
		n=2;
		}	
	if(m) {
	  SetColor(WHITE);
		OutCharCursor(n);	    
		FLAGS |= (1 << 4);
		}
	else {
		int i=LCDX+LCDY*getSizeX();
		clearChar();

		FLAGS &= ~(1 << 4);
		}

	}

void clearChar(void) {

    SetColor(BLACK);
	Bar(curPosX,curPosY,curPosX+getCharSizeX(),curPosY+getCharSizeY());
	}


void resetSettings(void) {
	FSFILE *f;
	
	configParms.backLight=1;
	configParms.splash=0;
	configParms.test=0;
	configParms.tVibro1[0]=5;
	configParms.tVibro1[1]=5;
	configParms.tVibro2[0]=10;
	configParms.tVibro2[1]=10;
    configParms.pressino=(BYTE)1;
	EEFSCreateMBR(1,254);

/*{
int i;
	I2CRead16Seq(0,16);
	sprintf(buf,"%02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X \r\n",
	I2CBuffer[0],I2CBuffer[1],I2CBuffer[2],I2CBuffer[3],I2CBuffer[4],I2CBuffer[5],I2CBuffer[6],I2CBuffer[7],
		I2CBuffer[8],I2CBuffer[9],I2CBuffer[10],I2CBuffer[11]);
	LCDPutString(buf);
	for(i=54; i<512; i+=12) {
		I2CRead16Seq(i,16);
		sprintf(buf,"%u:%02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X\r\n",i,
		I2CBuffer[0],I2CBuffer[1],I2CBuffer[2],I2CBuffer[3],I2CBuffer[4],I2CBuffer[5],I2CBuffer[6],I2CBuffer[7],
			I2CBuffer[8],I2CBuffer[9],I2CBuffer[10],I2CBuffer[11]);
		LCDPutString(buf);
	}	
}
__delay_ms(4000);*/


//__delaywdt_ms(100);
    ClrWdt();
    __delay_ms(100);

	EEFSformat(1,0x4448,"bwEE");

	/*	{
int i;
char buf[64];
	for(i=512; i<700; i+=12) {
		I2CRead16Seq(i,16);
		sprintf(buf,"%u:%02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X\r\n",i,
		I2CBuffer[0],I2CBuffer[1],I2CBuffer[2],I2CBuffer[3],I2CBuffer[4],I2CBuffer[5],I2CBuffer[6],I2CBuffer[7],
			I2CBuffer[8],I2CBuffer[9],I2CBuffer[10],I2CBuffer[11]);
		LCDPutString(buf);
	}	
}*/

	EEFSInit();			// forza il Mount di nuovo
	__delay_ms(50);

//sprintf(buf,"M,err:%u ",EEFSerror());
//	LCDPutString(buf);
/*	f=EEFSfopen("settings.ini",FS_APPEND);
sprintf(buf,"F=%u,err:%u ",f,EEFSerror());
	LCDPutString(buf);
	if(f) {
	LCDPutString("creo file");
		EEFSfwrite("dario=1\r\n",9,1,f);
		EEFSfclose(f);
		}*/
//flushData();		// sembra che serva... v. sopra: aprire il file, o scorrere DIR... la prima non va la seconda s�

//__delay_ms(1000);

	}

void saveSettings(void) {
	FSFILE *f;
    char buf[64];

    SetColor(BRIGHTRED);
	LCDPutStringCenter(6,"SAVING DATA..");

	f=EEFSfopen(SETTINGS_INI,FS_WRITE);
    
    sprintf(buf,"F=%u,err:%u ",f,EEFSerror());
//	LCDPutString(buf);
	if(f) {
		EEFSfwrite("[opzioni]\r\n",11,1,f);
		EEFSwriteParmN(f,"backlight",configParms.backLight);
		EEFSwriteParmN(f,"splash",configParms.splash);
		EEFSwriteParmN(f,"test",configParms.test);
        EEFSwriteParmN(f,"pressino",configParms.pressino);
		EEFSfwrite("[parametri]\r\n",13,1,f);
		EEFSwriteParmN(f,"TVibro1",configParms.tVibro1[0]);
		EEFSwriteParmN(f,"TVibro2",configParms.tVibro2[0]);
        EEFSwriteParmN(f,"lingua_default",configParms.lingua_default);
        
		EEFSfclose(f);
		}

//  SetColor(attribGrafici.reverse ? WHITE : BLACK);
//	ClearDevice();
	}

/* FABRIZIO lettura dei parametri di setting dal file setting ini presente nella sd card
   � stata aggiunta nella struct configParms presente in Maindemo.h BYTE lingua_default; */
void loadSettings(void) {
	char buf[64];
    char *chiave;
    char *profilo;    
              
	if(!SDcardOK) {
        configParms.backLight=1;
        configParms.splash=0;
        configParms.test=0;
        
		resetSettings();
		saveSettings();
	}
    else{              
            profilo="opzioni";
            
            *buf=0;
            chiave="backlight";
            GetProfileString(SETTINGS_INI,profilo,chiave,buf);
            configParms.backLight=atoi(buf);

            *buf=0;
            chiave="splash";
            GetProfileString(SETTINGS_INI,profilo,chiave,buf);
            configParms.splash=atoi(buf);

            *buf=0;
            chiave="test";
            GetProfileString(SETTINGS_INI,profilo,chiave,buf);
            configParms.test=atoi(buf);
                    
            *buf=0;
            chiave="pressino";
            GetProfileString(SETTINGS_INI,profilo,chiave,buf);
            configParms.pressino=atoi(buf);
            
            profilo="parametri";
            
            *buf=0;
            chiave="lingua_default";           
            GetProfileString(SETTINGS_INI,profilo,chiave,buf);
            configParms.lingua_default=atoi(buf);
                    
                    
            *buf=0;
            chiave="TVibro1";           
            GetProfileString(SETTINGS_INI,profilo,chiave,buf);
            Cycle_1Tazza=atof(buf);
            
           
            
            *buf=0;
            chiave="TVibro2";           
            GetProfileString(SETTINGS_INI,profilo,chiave,buf);
            Cycle_2Tazza=atof(buf);         
    }
}

void exportSettingsSD(void) {		// copia INI da EEprom a SD
	int ch;
	FSFILE *f1,*f2;

	f1=EEFSfopen(SETTINGS_INI,FS_READ);
	if(f1) {
		f2=FSfopen(SETTINGS_INI,FS_WRITE);
		if(f2){
            while((ch=EEFSgetc(f1)) != EOF) {
                FSputc(ch,f2);
            }	
            FSfclose(f2);
        }
        EEFSfclose(f1);
	}
}

int exportSettingsUSB(void) {		// copia INI da EEprom a USB
	int ch;
	FSFILE *f1,*f2;

	f1=EEFSfopen(SETTINGS_INI,FS_READ);
	if(f1) {
		f2=USBFSfopen(SETTINGS_INI,FS_WRITE);
		if(f2) {
			while((ch=EEFSgetc(f1)) != EOF) {
				USBFSputc(ch,f2);
				}	
			USBFSfclose(f2);
			}
		EEFSfclose(f1);
		}

//  SetColor(attribGrafici.reverse ? WHITE : BLACK);
//	ClearDevice();
	return f1 && f2;
	}

int importSettingsUSB(void) {		// copia INI da USB a EEprom 
	int ch;
	FSFILE *f1,*f2;

	f1=USBFSfopen(SETTINGS_INI,FS_READ);
	if(f1) {
		f2=EEFSfopen(SETTINGS_INI,FS_WRITE);
		if(f2) {
			while((ch=USBFSgetc(f1)) != EOF) {
				EEFSputc(ch,f2);
				}	
			EEFSfclose(f2);
			}
		USBFSfclose(f1);
		}

//  SetColor(attribGrafici.reverse ? WHITE : BLACK);
//	ClearDevice();
	return f1 && f2;
	}

DWORD copyFileUSBToSD(const char *source,const char *dest) {
	int ch;
	DWORD l=0;
	FSFILE *f1,*f2;

	f1=USBFSfopen(source,FS_READ);
	if(f1) {
		f2=FSfopen(dest,FS_WRITE);
		if(f2) {
			while((ch=USBFSgetc(f1)) != EOF) {
				FSputc(ch,f2);
        		l++;
			}	
			FSfclose(f2);
		}
		USBFSfclose(f1);
	}

//  SetColor(attribGrafici.reverse ? WHITE : BLACK);
//	ClearDevice();
	return l;
	}

int FSReadLine(FSFILE *f,char *buf) {
	int i,j=0;
	char ch;

  do {
        ClrWdt();   
		i=FSfread(&ch,1,1,f);
		if(i>0) {
			switch(ch) {
				case 13:		//Linux??
					break;
				case 10:
					i=0;
					break;
				default:
					buf[j++]=ch;
					break;
				}
			}
  } while(i>0);
  buf[j]=0;
  return j;
}

int getParmN(const char *buf,const char *parm,int *val) {
	char *p;

	if((p=strchr(buf,'='))) {
		*p=0;
		if(!stricmp(buf,parm)) {
			if(val)
				*val=atoi(p+1);
			*p='=';
			return 1;
			}
		else			
			*p='=';
		}
	return 0;
	}

int getParmS(const char *buf,const char *parm,char *val) {
	char *p;

	if((p=strchr(buf,'='))) {
		*p=0;
		if(!stricmp(buf,parm)) {
			if(val)
				strcpy(val,p+1);
			*p='=';
			return 1;
			}
		else			
			*p='=';
		}
	return 0;
	}

int FSwriteParmN(FSFILE *f,const char *parm,int val) {

	FSfprintf(f,"%s=%d\r\n",parm,val);
	}

int FSwriteParmS(FSFILE *f,const char *parm,const char *val) {

	FSfprintf(f,"%s=%s\r\n",parm,val);
	}

int EEFSwriteParmN(FSFILE *f,const char *parm,int val) {

	EEFSfprintf(f,"%s=%d\r\n",parm,val);
	}

int EEFSwriteParmS(FSFILE *f,const char *parm,const char *val) {

	EEFSfprintf(f,"%s=%s\r\n",parm,val);
	}

/* FABRIZIO tolta if(value), altrimenti partendo con variabile vuota non svolge la sua funzione
 * dopo if(strncmp(buf,chiave,n)==0) { */
BYTE GetProfileInt(const char * file,const char *gruppo,const char *chiave,int *value) {
	FSFILE *f;
	char buf[64];
	int n;
    
    // LCDPutStringCenter(1,"PROFILO INT");
	if(f=FSfopen(file,FS_READ)) {
     //   LCDPutStringCenter(2,"APERTO FILE ");
		if(cercaGruppo(f,gruppo)) {
           //  LCDPutStringCenter(3,"TROVO GRUPPO ");
			n=strlen(chiave);
			while(FSReadLine(f,buf)>0) {
           //   LCDPutStringCenter(4,"RIGA ");
           //   LCDPutStringCenter(5,buf);
				if(strncmp(buf,chiave,n)==0) {
					*value=atoi(buf+n+1);
					FSfclose(f);
					return 1;
				}	
			}	
		}
		FSfclose(f);
	}	
	return 0;
}

/* FABRIZIO tolta if(value), altrimenti partendo con variabile vuota non svolge la sua funzione
 * dopo if(strncmp(buf,chiave,n)==0) { */
BYTE GetProfileString(const char * file,const char *gruppo,const char *chiave,char *value) {
	FSFILE *f;
	char buf[64];
	int n;

	if(f=FSfopen(file,FS_READ)) {
		if(cercaGruppo(f,gruppo)) {
			n=strlen(chiave);
			while(FSReadLine(f,buf)>0) {
				if(strncmp(buf,chiave,n)==0) {               
                    strcpy(value,buf+n+1);
                    FSfclose(f);
					return 1;
				}	
			}	
		}
            /*else  { // SOLO X DEBUG
                LCDPutStringCenter(11,"non gruppo ");
            } */
        FSfclose(f);
        /* SOLO X DEBUG*/
           //LCDPutStringCenter(12,"non trov ");
	}	/* //SOLO X DEBUG
    else{ 
          LCDPutStringCenter(13,"non open ");
    }        
    LCDPutStringCenter(14,chiave);
    */
	return 0;
}

BYTE WriteProfileInt(const char *gruppo,const char *chiave,int value) {
	FSFILE *f;

	if(f=FSfopen(MENU_INI,FS_READWRITE)) {
		if(cercaGruppo(f,gruppo)) {
			FSwriteParmN(f,chiave,value);
		}
		FSfclose(f);
	}	

	return 0;
}


BYTE updateSDCardFileSetting(char *file){
    FSFILE *f;
    char appoggio[20]="";
    
    if(f=FSfopen(file,FS_WRITE)) {
        FSfprintf(f,"%s","[opzioni]\r\n");
        FSfprintf(f,"%s","backlight=1\r\n");
        FSfprintf(f,"%s","splash=1\r\n");
        sprintf(appoggio,"%s%u%s","pressino=",(BYTE)configParms.pressino,"\r\n");
        
        LCDPutStringXY(1,3,appoggio);
        FSfprintf(f,appoggio);
        
        
        FSfprintf(f,"%s","[parametri]\r\n");
        
        sprintf(appoggio,"%s%u%s","LANGUAGE=",(BYTE)configParms.lingua_default,"\r\n");
        LCDPutStringXY(1,4,appoggio);
        sprintf(appoggio,"%s%u%s","lingua_default=",(BYTE)configParms.lingua_default,"\r\n");
        
        FSfprintf(f,appoggio);
        
        sprintf(appoggio,"%s%0.1f%s","TVibro1=",Cycle_1Tazza,"\r\n");
        FSfprintf(f,appoggio);
        LCDPutStringXY(1,5,appoggio);
        
        sprintf(appoggio,"%s%.1f%s","TVibro2=",Cycle_2Tazza,"\r\n");
        FSfprintf(f,appoggio);
        LCDPutStringXY(1,6,appoggio);
        
        
       
        FSfclose(f);
        return 1;
    }
    return 0;
}

BYTE WriteProfileString(const char *file,const char *gruppo,const char *chiave,char *value) {
	FSFILE *f;

	if(f=FSfopen(file,FS_READWRITE)) {
		if(cercaGruppo(f,gruppo)) {
			FSwriteParmS(f,chiave,value);
		}
		FSfclose(f);
        return 1;
	}	

	return 0;
}

/* FABRIZIO MODIFICATA ORIGINALE BACATA E UTILIZZO LA strncmp funzione di libreria originale*/
BYTE cercaGruppo(FSFILE *f,const char *gruppo) {
	char buf[64];
    //char str[20];
	int n;

	FSfseek(f,0,SEEK_SET);
 
	while(FSReadLine(f,buf)>0) {
  
		if(buf[0]=='[') {
			n=strlen(gruppo);
            
        //  LCDPutStringCenter(5,gruppo);
        //  sprintf(str,"comp %d",strncmp(buf+1,gruppo,n)) ;
        //  LCDPutStringCenter(6,str);
        //  __delay_ms(1000);
			if(strncmp(buf+1,gruppo,n)==0) {
				if(buf[n+1]==']') {
					return 1;
				}	
			}	
		}        
	}
}

signed char stricmp(const char *s,const char *d) {
/*
	while(*s && *d) {
		if(tolower(*s) != tolower(*d))
			return tolower(*s) - tolower(*d);
		s++;
		d++;
		ClrWdt();
		}
	return 0;
*/

  while(tolower(*s) == tolower(*d++)) {
		if(*s++ == '\0') {
			return 0;
			}
		}

	return *s - *d;
	}

signed char strnicmp(const char *s,const char *d,int n) {

  while(tolower(*s) == tolower(*d++) && n--) {
		if(*s++ == '\0') {
			return 0;
			}
		}

	return *s - *d;
	}



/****************************************************************************
  Function:
    DWORD   PIC24RTCCGetDate( void )

  Description:
    This routine reads the date from the RTCC module.

  Precondition:
    The RTCC module has been initialized.


  Parameters:
    None

  Returns:
    DWORD in the format <xx><YY><MM><DD>

  Remarks:
    To catch roll-over, we do two reads.  If the readings match, we return
    that value.  If the two do not match, we read again until we get two
    matching values.

    For the PIC32MX, we use library routines, which return the date in the
    PIC32MX format.
  ***************************************************************************/
DWORD PIC24RTCCGetDate( void ) {
  DWORD_VAL   date1;
  DWORD_VAL   date2;

  do{
        while (RCFGCALbits.RTCSYNC);

        RCFGCALbits.RTCPTR0 = 1;
        RCFGCALbits.RTCPTR1 = 1;
        date1.w[1] = RTCVAL;
        date1.w[0] = RTCVAL;

        RCFGCALbits.RTCPTR0 = 1;
        RCFGCALbits.RTCPTR1 = 1;
        date2.w[1] = RTCVAL;
        date2.w[0] = RTCVAL;

  } while (date1.Val != date2.Val);

  return date1.Val;
}

/****************************************************************************
  Function:
    DWORD   PIC24RTCCGetTime( void )

  Description:
    This routine reads the time from the RTCC module.

  Precondition:
    The RTCC module has been initialized.

  Parameters:
    None

  Returns:
    DWORD in the format <xx><HH><MM><SS>

  Remarks:
    To catch roll-over, we do two reads.  If the readings match, we return
    that value.  If the two do not match, we read again until we get two
    matching values.

    For the PIC32MX, we use library routines, which return the time in the
    PIC32MX format.
  ***************************************************************************/
DWORD PIC24RTCCGetTime( void ) {
  DWORD_VAL   time1;
  DWORD_VAL   time2;

  do {
    while (RCFGCALbits.RTCSYNC);

    RCFGCALbits.RTCPTR0 = 1;
    RCFGCALbits.RTCPTR1 = 0;
    time1.w[1] = RTCVAL;
    time1.w[0] = RTCVAL;

    RCFGCALbits.RTCPTR0 = 1;
    RCFGCALbits.RTCPTR1 = 0;
    time2.w[1] = RTCVAL;
    time2.w[0] = RTCVAL;

    } while (time1.Val != time2.Val);

    return time1.Val;
	}

/****************************************************************************
  Function:
    void PIC24RTCCSetDate( WORD xx_year, WORD month_day )

  Description:
    This routine sets the RTCC date to the specified value.


  Precondition:
    The RTCC module has been initialized.

  Parameters:
    WORD xx_year    - BCD year in the lower byte
    WORD month_day  - BCD month in the upper byte, BCD day in the lower byte

  Returns:
    None

  Remarks:
    For the PIC32MX, we use library routines.
  ***************************************************************************/
void PIC24RTCCSetDate( WORD xx_year, WORD month_day ) {
  
	UnlockRTCC();
  RCFGCALbits.RTCPTR0 = 1;
  RCFGCALbits.RTCPTR1 = 1;
  RTCVAL = xx_year;
  RTCVAL = month_day;
	}

/****************************************************************************
  Function:
    void PIC24RTCCSetTime( WORD weekDay_hours, WORD minutes_seconds )

  Description:
    This routine sets the RTCC time to the specified value.

  Precondition:
    The RTCC module has been initialized.

  Parameters:
    WORD weekDay_hours      - BCD weekday in the upper byte, BCD hours in the
                                lower byte
    WORD minutes_seconds    - BCD minutes in the upper byte, BCD seconds in
                                the lower byte

  Returns:
    None

  Remarks:
    For the PIC32MX, we use library routines.
  ***************************************************************************/
void PIC24RTCCSetTime( WORD weekDay_hours, WORD minutes_seconds) {

  UnlockRTCC();
  RCFGCALbits.RTCPTR0 = 1;
  RCFGCALbits.RTCPTR1 = 0;
  RTCVAL = weekDay_hours;
  RTCVAL = minutes_seconds;
	}

/****************************************************************************
  Function:
    void UnlockRTCC( void )

  Description:
    This function unlocks the RTCC so we can write a value to it.

  Precondition:
    None

  Parameters:
    None

  Return Values:
    None

  Remarks:
    For the PIC32MX, we use library routines.
  ***************************************************************************/

void UnlockRTCC( void ) {
  BOOL interruptsWereOn;

  interruptsWereOn = FALSE;
  if((RTCC_INTERRUPT_REGISTER & RTCC_INTERRUPT_VALUE) == RTCC_INTERRUPT_VALUE) {
    interruptsWereOn = TRUE;
    RTCC_INTERRUPT_REGISTER &= ~RTCC_INTERRUPT_VALUE;
    }

  // Unlock the RTCC module
  __asm__ ("mov #NVMKEY,W0");
  __asm__ ("mov #0x55,W1");
  __asm__ ("mov #0xAA,W2");
  __asm__ ("mov W1,[W0]");
  __asm__ ("nop");
  __asm__ ("mov W2,[W0]");
  __asm__ ("bset RCFGCAL,#13");
  __asm__ ("nop");
  __asm__ ("nop");

  if(interruptsWereOn) {
    RTCC_INTERRUPT_REGISTER |= RTCC_INTERRUPT_VALUE;
    }
	}


BYTE to_bcd(BYTE n) {
	
	return (n % 10) | ((n / 10) << 4);
	}

BYTE from_bcd(BYTE n) {
	
	return (n & 15) + (10*(n >> 4));
	}
// ----------------------------------------------------------------------

unsigned char WriteSPI2M(unsigned char data_out) {

#ifdef __PIC32MX__
    BYTE   clear;
    putcSPI((BYTE)data_out);
    clear = getcSPI();
    return ( 0 );                // return non-negative#
#else
    BYTE   clear;
    SPI2BUF = data_out;          // write byte to SSP1BUF register
    while(!SPI2STAT_RBF); // wait until bus cycle complete
    clear = SPI2BUF;
    return 0;                // return non-negative#
#endif
	}

BYTE ReadSPI2M(void) {

#ifdef __C32__
    putcSPI((BYTE)0xFF);
    return (BYTE)getcSPI();
#else
    SPIBUF = 0xFF;                              //Data Out - Logic ones
    while(!SPISTAT_RBF);                     //Wait until cycle complete
    return(SPIBUF);                             //Return with byte read
#endif
	}

void scanKBD(void) {
	BYTE *p,n;
	BYTE i;
	BYTE *rowStatePtr;
	BYTE kbtemp;
	BYTE kbScanY;
	BYTE kbScanCode;		// il num. progressivo del tasto scandito


//return;  //FINIRE!

	ClrWdt();
//	memcpy(oldkbKeys,kbKeys,MAX_TASTI_CONTEMPORANEI);		meglio sopra...
	memset(kbKeys,0,MAX_TASTI_CONTEMPORANEI);


  SPI2CON1 = 0x0000;              // power on state
  SPI2CON1 |= MASTER_ENABLE_ON | PRI_PRESCAL_1_1 | SEC_PRESCAL_1_1;          // select serial mode 
  SPI2CON1bits.CKP = 1;
  SPI2CON1bits.CKE = 0;
  SPI2CLOCK = 0;
  SPI2OUT = 0;                  // define SDO1 as output (master or slave)
  SPI2IN = 1;                  // define SDI1 as input (master or slave)
  SPI2ENABLE = 1;             // enable synchronous serial port


	p=(BYTE *)rowState;

	SPI2_CS=0;
	WriteSPI2M(1);
	for(i=0; i<32; i++) {
		*p++=ReadSPI2M();
		}
	SPI2_CS=1;

	kbScanCode=1;			// parto da 1!! (se no lo zero=non valido taglierebbe via ROW0:COL0)
	for(i=0; i<10; i++) {					// analizzo i tasti meccanici
		ClrWdt();

		if(rowState[i])
			checkKey(kbScanCode);

		kbScanCode++;
		}			// for(rowStatePtr...

	if(memcmp(kbKeys,oldkbKeys,MAX_TASTI_CONTEMPORANEI))
		FLAGS |= (1 << TRIGK);

	return;


	TRISBbits.TRISB4=1;

	TRISEbits.TRISE9=1;
	TRISEbits.TRISE8=1;
	TRISAbits.TRISA0=1;

	LATBbits.LATB3=0;		// prima, riscrivo output...
	LATBbits.LATB2=0;
	LATBbits.LATB1=0;
	LATBbits.LATB0=0;
	LATGbits.LATG13=0;

	TRISBbits.TRISB3=1;
	TRISBbits.TRISB2=1;
	TRISBbits.TRISB1=1;
	TRISBbits.TRISB0=1;
	TRISGbits.TRISG13=1;

	kbScanY=0b00011110;
	kbScanCode=1;			// parto da 1!! (se no lo zero=non valido taglierebbe via ROW0:COL0)
	

// faccio la scansione, scrivendo nelle colonne e leggendo dalle righe;
//		poi salvo i valori nell'array da 8 byte rowState

	do {

		switch(kbScanY) {
			case 0b00011110:
				TRISBbits.TRISB3=0;
				break;
			case 0b00011101:
				TRISBbits.TRISB2=0;
				break;
			case 0b00011011:
				TRISBbits.TRISB1=0;
				break;
			case 0b00010111:
				TRISBbits.TRISB0=0;
				break;
			case 0b00001111:
				TRISGbits.TRISG13=0;
				break;
			}

		__delay_us(2);									// ritardino...
		ClrWdt();

		n=0;
		n |= PORTBbits.RB4;
		n |= (PORTBbits.RB5 << 1);
		n |= (PORTEbits.RE9 << 2);
		n |= (PORTEbits.RE8 << 3);
		n |= (PORTAbits.RA0 << 4);

		*p++=n;

		kbScanY <<= 1;
		kbScanY |= 1;								// entra 1
		kbScanY &= 0b00011111;

		TRISBbits.TRISB3=1;
		TRISBbits.TRISB2=1;
		TRISBbits.TRISB1=1;
		TRISBbits.TRISB0=1;
		TRISGbits.TRISG13=1;

		} while(kbScanY != 0b00011111);

// risistemiamo la situaz. uscite
	TRISBbits.TRISB3=1;
	TRISBbits.TRISB2=1;
	TRISBbits.TRISB1=1;
	TRISBbits.TRISB0=1;
	TRISGbits.TRISG13=1;




// ora vado a controllare i tasti premuti, facendo anche attenzione ai tasti ripetuti:
//		non devono essere piu' di 2 nella colonna 7 (modifier)
//		 e in generale non devono esserci tasti fantasma (quelli "a quadrato")

/*SetColor(BLACK);
LCDCls();
LCDPutString(":");*/
	for(rowStatePtr=rowState; rowStatePtr < (rowState+5); rowStatePtr++) {	// puntatore alle letture fatte prima


/*{char mybuf[8];
sprintf(mybuf,"%02X ",*rowStatePtr);
LCDPutString(mybuf);
}*/
		for(kbtemp=1; kbtemp!=0b00100000; kbtemp <<= 1 /* entra 0 */) {					// analizzo le righe...
			ClrWdt();

			if(!(kbtemp & *rowStatePtr))
				checkKey(kbScanCode);

			kbScanCode++;
			} // 5 righe

		}			// for(rowStatePtr...


/*{char mybuf[8];
int i;
LCDPutString("; ");
for(i=0; i<MAX_TASTI_CONTEMPORANEI; i++) {
sprintf(mybuf,"%u ",kbKeys[i]);
LCDPutString(mybuf);
}
}
LCDPutString("\n");*/

	if(memcmp(kbKeys,oldkbKeys,MAX_TASTI_CONTEMPORANEI))
		FLAGS |= (1 << TRIGK);

	}

BYTE checkKey(BYTE kbScanCode) {
	BYTE i;

	for(i=0; i<MAX_TASTI_CONTEMPORANEI; i++) {
		ClrWdt();
		if(!kbKeys[i]) {				//	cerco un posto vuoto x metterci lo scan code
			kbKeys[i]=kbScanCode;
			return 1;
			}
		}

	return 0;
	}

void setKBDLed(void) {
	BYTE *p,n;
	BYTE i;

	ClrWdt();

  SPI2CON1 = 0x0000;              // power on state
  SPI2CON1 |= MASTER_ENABLE_ON | PRI_PRESCAL_1_1 | SEC_PRESCAL_1_1;          // select serial mode 
  SPI2CON1bits.CKP = 1;
  SPI2CON1bits.CKE = 0;
  SPI2CLOCK = 0;
  SPI2OUT = 0;                  // define SDO1 as output (master or slave)
  SPI2IN = 1;                  // define SDI1 as input (master or slave)
  SPI2ENABLE = 1;             // enable synchronous serial port



	SPI2_CS=0;
	WriteSPI2M(1);
	for(i=0; i<32; i++) {
		WriteSPI2M(0);
		}
	SPI2_CS=1;
	}

WORD getChksum16(BYTE *p,BYTE len) {		//CRC16
	WORD c;
	int i,j;

	c=0xffff;
	while(len--) {
		c ^= *p;
		for(i=0; i<8; i++) {
			j=c & 1;
			c >>= 1;
			if(j) {
				c ^= 0xa001;
				}
			}

		p++;
		}

	return c;
	}

WORD getChksumASCII(BYTE *p,int len) {		//LRC, non usiamo qua
	WORD c;

	c=0;
	while(len--) {
		c += *p;
		p++;
		}

	return c;
	}


static FSFILE *f;
WORD ExternalMemoryCallback(GFX_EXTDATA* memory, LONG offset, WORD nCount, void* buffer) {
	WORD n=0;

//	LED2_IO = 1;
	if(memory) {
		if(!f)
			f=FSfopen((char *)memory,FS_READ);			// provo a cache-are e lasciare aperto...
		if(f) {
//			if(f->seek  != offset)		// ottimizzo NO V. SOTTO
				FSfseek(f,offset,SEEK_SET);
			n=FSfread(buffer,1,nCount,f);
			}
		}
	else {
		FSfclose(f);
		f=NULL;
		}
//	LED2_IO = 0;
	return n;
	}

WORD ExternalMemoryCallbackCont(WORD nCount, void* buffer) {

//	LED2_IO ^= 1;
	if(f) {
		return FSfread(buffer,1,nCount,f);
		}
	return 0;
	}

/****************************************************************************
  Function:
    BOOL USB_ApplicationEventHandler( BYTE address, USB_EVENT event,
                void *data, DWORD size )

  Summary:
    This is the application event handler.  It is called when the stack has
    an event that needs to be handled by the application layer rather than
    by the client driver.

  Description:
    This is the application event handler.  It is called when the stack has
    an event that needs to be handled by the application layer rather than
    by the client driver.  If the application is able to handle the event, it
    returns TRUE.  Otherwise, it returns FALSE.

  Precondition:
    None

  Parameters:
    BYTE address    - Address of device where event occurred
    USB_EVENT event - Identifies the event that occured
    void *data      - Pointer to event-specific data
    DWORD size      - Size of the event-specific data

  Return Values:
    TRUE    - The event was handled
    FALSE   - The event was not handled

  Remarks:
    The application may also implement an event handling routine if it
    requires knowledge of events.  To do so, it must implement a routine that
    matches this function signature and define the USB_HOST_APP_EVENT_HANDLER
    macro as the name of that function.
  ***************************************************************************/
BOOL USB_ApplicationEventHandler( BYTE address, USB_EVENT event, void *data, DWORD size ) {
  
	switch(event) {
        case EVENT_VBUS_REQUEST_POWER:
            // The data pointer points to a byte that represents the amount of power
            // requested in mA, divided by two.  If the device wants too much power,
            // we reject it.
            return TRUE;

        case EVENT_VBUS_RELEASE_POWER:
            // Turn off Vbus power.
            // The PIC24F with the Explorer 16 cannot turn off Vbus through software.

            //This means that the device was removed
            deviceAttached = FALSE;
            return TRUE;
            break;

        case EVENT_HUB_ATTACH:
            return TRUE;
            break;

        case EVENT_UNSUPPORTED_DEVICE:
            return TRUE;
            break;

        case EVENT_CANNOT_ENUMERATE:
            //UART2PrintString( "\r\n***** USB Error - cannot enumerate device *****\r\n" );
            return TRUE;
            break;

        case EVENT_CLIENT_INIT_ERROR:
            //UART2PrintString( "\r\n***** USB Error - client driver initialization error *****\r\n" );
            return TRUE;
            break;

        case EVENT_OUT_OF_MEMORY:
            //UART2PrintString( "\r\n***** USB Error - out of heap memory *****\r\n" );
            return TRUE;
            break;

        case EVENT_UNSPECIFIED_ERROR:   // This should never be generated.
            //UART2PrintString( "\r\n***** USB Error - unspecified *****\r\n" );
            return TRUE;
            break;

        default:
            break;
    }

  return FALSE;
	}

const struct LCD_ITEM lcdItems[ULTIMA_SCHERMATA_PROGRAMMAZIONE-PRIMA_SCHERMATA_PROGRAMMAZIONE] = 
{
//	BYTE x,y;	BYTE size;	WORD valMin,valMax;	WORD *var;	BYTE varSize;	char *name;
//varsize=0 e val min/max 0..1 indicano ON/OFF!
	0,0,11,0,50,(WORD *)&configParms.tVibro1[0],1,"Vibro1/pre:","d.s",		// v. sopra
	0,1,11,0,50,(WORD *)&configParms.tVibro1[1],1,"Vibro1/post:","d.s",
	0,2,11,0,50,(WORD *)&configParms.tVibro2[0],1,"Vibro2/pre:","d.s",
	0,3,11,0,50,(WORD *)&configParms.tVibro2[1],1,"Vibro2/post:","d.s",
	0,4,11,0,1,(WORD *)&configParms.mVibro1[0],0, "Vibro1\r\nristretto:",NULL,
	0,5,11,0,1,(WORD *)&configParms.mVibro1[1],0, "Vibro1\r\n1 tazza:",NULL,
	0,6,11,0,1,(WORD *)&configParms.mVibro1[2],0, "Vibro1\r\n2 tazze:",NULL,
	0,7,11,0,1,(WORD *)&configParms.mVibro1[3],0, "Vibro1\r\n1/4 sacco:",NULL,
	0,8,11,0,1,(WORD *)&configParms.mVibro1[4],0, "Vibro1\r\n1/2 sacco:",NULL,
	0,9,11,0,1,(WORD *)&configParms.mVibro1[5],0, "Vibro1\r\n1 sacco:",NULL,
	0,10,11,0,1,(WORD *)&configParms.mVibro2[0],0,"Vibro2\r\nristretto:",NULL,
	0,11,11,0,1,(WORD *)&configParms.mVibro2[1],0,"Vibro2\r\n1 tazza:",NULL,
	0,12,11,0,1,(WORD *)&configParms.mVibro2[2],0,"Vibro2\r\n2 tazze:",NULL,
	0,13,11,0,1,(WORD *)&configParms.mVibro2[3],0,"Vibro2\r\n1/4 sacco:",NULL,
	0,14,11,0,1,(WORD *)&configParms.mVibro2[4],0,"Vibro2\r\n1/2 sacco:",NULL,
	0,15,11,0,1,(WORD *)&configParms.mVibro2[5],0,"Vibro2\r\n1 sacco:",NULL,
//questi passano a centesimi/sec
	0,16,14,0,30000,(WORD *)&configParms.tMacinatura[0],2,"Tempo macinaristretto:","c.s",		// 12 char per riga
	0,17,14,0,30000,(WORD *)&configParms.tMacinatura[1],2,"Tempo macina1 tazza:","c.s",
	0,18,14,0,30000,(WORD *)&configParms.tMacinatura[2],2,"Tempo macina2 tazze:","c.s",
	0,19,14,0,30000,(WORD *)&configParms.tMacinatura[3],2,"Tempo macina1/4 sacco:","c.s",
	0,20,14,0,30000,(WORD *)&configParms.tMacinatura[4],2,"Tempo macina1/2 sacco:","c.s",
	0,21,14,0,30000,(WORD *)&configParms.tMacinatura[5],2,"Tempo macina1 sacco:","c.s",

	0,22,13,300,1500,(WORD *)&configParms.vMacinatura,2,"Vel.macina:","rpm",
	0,23,7,0,100,(WORD *)&configParms.vVibro[0],1,"Vel.vibro1:","%",
	0,24,7,0,100,(WORD *)&configParms.vVibro[1],1,"Vel.vibro2:","%",
	0,25,6,0,1,(WORD *)&configParms.needPortafiltro,0,"Filtro:",NULL,	
	0,26,8,0,1,(WORD *)&configParms.needSacchetto,0,"Sacchetto:",NULL,
// inserire ioni negativi
	0,27,10,0,1,(WORD *)&configParms.test,0,"SELF TEST:",NULL,
	0,28,10,0,2,(WORD *)&configParms.backLight,1,"Backlight:",NULL,
	0,29,10,0,2,(WORD *)&configParms.backLight,1,"Contrasto:",NULL,
	0,30,14,0,1,(WORD *)&configParms.setupEnabled,0,"SETUP ENABLED:",NULL,
	0,31,14,0,1,(WORD *)&configParms.splash,0,"Splash screen:",NULL
	};
  
void myInit_PWM(void)
{
    TRISDbits.TRISD3 = 0;               /* output   */
    RPOR1bits.RP67R = 0x10;                 /* OC1 010000 RPn tied to Output Compare 1 output */  

    OC1CON1 = 0;                        /* It is a good practice to clear off the control bits initially */
    OC1CON2 = 0;
    OC1CON1bits.OCTSEL = 0x07;          /* This selects the peripheral clock as the clock input to the OC module */
    OC1R = 00000;                        /* This is just a typical number, user must calculate based on the waveform requirements and the system clock */
    OC1RS = PWM_PERIOD;                       /* Determines the Period */
    
    OC1CON2bits.SYNCSEL = 0x1F;         /* This selects the synchronization source as itself */
    OC1CON1bits.OCM = 6;                /* This selects and starts the Edge Aligned PWM mode*/

}

void mySetPwm(unsigned char perc)
{
unsigned long duty;

    duty = ((PWM_PERIOD * perc) / 100);
    OC1R = duty;                        /* This is just a typical number, user must calculate based on the waveform requirements and the system clock */

}



void testLCD_SD_CARD(int SDcardOK){
    char buffer[64];
    char ByteRec;
    int i;
    
     // test caratteri
    myFont=&font16;
    SetFont((void *)&font16);
    SetFontOrientation(0);		//horiz
    SetColor(GREEN);
    LED0_IO ^= 1;
    
	//	LCDPutStringCenter(5,"..SELF TEST..");   //matteo2019
	if(SDcardOK) {
		FSFILE *logFile=NULL;
		 // test caratteri
		myFont=&font16;
		SetFont((void *)&font16);
		SetFontOrientation(0);		//horiz
		SetColor(GREEN);
		LED0_IO ^= 1;
    
		LCDPutStringCenter(5,"..TEST.. ");
		LCDPutStringCenter(5,SDcardOK);
 
//		SetClockVars(((WORD)(currentDate.year))+2000, currentDate.mon, currentDate.mday, currentTime.hour, currentTime.min, currentTime.sec);
//		qua usiamo RTC
        logFile = FSfopen("bw.txt",FS_APPEND);
		if(logFile) {
			sprintf((char*)&buffer[0],"%u,%u\r\n",VERNUMH,VERNUML);
			FSfwrite((const void*)&buffer[0],1,strlen(buffer),logFile);
			FSfclose(logFile);
			logFile = NULL;
		}
	}

		
	char ch;
	LCDHome();
	for(ch=' '; ch<127; ch++)
		LCDPutChar(ch);
		

	sprintf(buffer,"VIEW ANGLE SETTING=%u",configParms.viewingAngle);
   	DelayMs(1*DEMODELAY);
	LED0_IO ^= 1;
	

	LCDCls();
	scanKBD(); 
	
	SetColor(BRIGHTGREEN);

	if(SDcardOK){ 
		LCDPutStringXY(0,6,"EXT.MEM.TEST..PASSED");     
        LCDCls();
    }
	else{
		LCDCls();
		LCDPutStringXY(0,6,"EXT.MEM.TEST..FAILED");      
    }

	// Initialize USB layers
	deviceAttached = FALSE;
	//Initialize the stack
	ByteRec=USBInitialize(0);
	LCDPutStringXY(0,7,ByteRec ? "USBHOST TEST..PASSED" : "USBHOST TEST..FAILED");     
	i=EEFSInit();
//	EEFSformat(1,0x4447,"bw");

//	LCDPutStringXY(0,9,"ID=");
	strncpy(buffer,CopyrString,28);
	buffer[28]=0;
//	LCDPutStringXY(4,9,buffer);
	strncpy(buffer,CopyrString+28,28);
	buffer[28]=0;
//	LCDPutStringXY(4,10,buffer);
	strncpy(buffer,CopyrString+56,28);
	buffer[28]=0;
//	LCDPutStringXY(4,11,buffer);
	
    PutBmp8BPPExtFast(GetRealOrgX(), GetRealOrgY(), (void *) "0.bmp" , IMAGE_NORMAL);
    
   	#ifdef USA_BUZZER_PWM 		
   		OC2CON1 |= 0x0006;   // on
        __delay_ms(100);
		ClrWdt();
		OC2CON1 &= ~0x0006;   // off
		__delay_ms(100);
		ClrWdt();
		OC2CON1 |= 0x0006;   // on
		__delay_ms(100);
		ClrWdt();
		OC2CON1 &= ~0x0006;   // off
		__delay_ms(100);
		ClrWdt();
		OC2CON1 |= 0x0006;   // on
		__delay_ms(100);
		ClrWdt();
		OC2CON1 &= ~0x0006;   // off
		//	__delay_ms(100);
		ClrWdt();
	#else
		LATGbits.LATG15=1;   // on
		__delay_ms(100);
        ClrWdt();
        LATGbits.LATG15=0;   // off
		__delay_ms(100);
		ClrWdt();
		LATGbits.LATG15=1;   // on
		__delay_ms(100);
		ClrWdt();
		LATGbits.LATG15=0;   // off
		__delay_ms(100);
		ClrWdt();
		LATGbits.LATG15=1;   // on
	    __delay_ms(100);
		ClrWdt();
		LATGbits.LATG15=0;   // off
	//	__delay_ms(100);
        ClrWdt();
	#endif
	DelayMs(1*DEMODELAY);

}


/*FABRIZIO */
BYTE cercaRigaLingua(int riga){
    FSFILE *fl=NULL;
    char bufl[512];
    int contaRiga=0;
    BYTE trovato=0;
    int c=0;
    char separatore=';';
    int pos;
    char *p;
    int occorrenze;
  
    
    
    if(fl=FSfopen(MENU_LINGUE,FS_READ)) {
        while(FSReadLine(fl,bufl)>0) {
            
            if(contaRiga==riga){
                break;
            }
            contaRiga++;
        }
         if(contaRiga==riga){     
             
             trovato= 1; // riga trovata
              /* indice lingue 0=Italiano,1=English;2=Deutsch;3=Francais;4=Espanol
               * essendo nel file lingue la struttura Indice;Italiano;English;Deutsch;Francais;Espanol
               * si ha che la posizione coincide con indice lingua+1
              */
             
            occorrenze =((int)configParms.lingua_default)+1;
            
         
            
            /*Devo prelevare la voce della lingua corrente che si trover� fra 2 punti e virgola  */          
            while(c<occorrenze){
                //puntatore all'occorennza di separatore
                p = strchr(bufl, separatore);
                
                if(p==NULL)
                    break; //se capita vuol dire che non ho trovato la colonna della lingua
                
                pos = (int)(p - bufl);
                strcpy(bufl,bufl+pos+1);
                c++;                
            }    
            
            if(c==occorrenze){
                p=strchr(bufl,separatore);//se ho un altro ; devo troncare perch� inizia una nuova lingua
                if(p!=NULL){
                    pos = (int)(p - bufl);
                    
                }
                else
                   pos=strlen(bufl) +1;  
            }else pos=strlen(bufl)+1; 
            
          
            //mnu.text = realloc(mnu.text, sizeof(char)* strlen(pos) + 1);
            free(mnu.text);
            *mnu.text=0;
            
            mnu.text=(char*)malloc(sizeof(char)* (strlen(bufl) + 1));
            
            strncpy(mnu.text,bufl,pos);
            mnu.text[pos]=NULL;
           
             
         }
      FSfclose(fl);
        
    }
    return trovato;
}

//Corrispondenza con comandi
BYTE loadMenu(const char * file,const char *chiave){
    FSFILE *f;    
    int c,n,indice=0;
    char buf[64];
    char risultato[64];
    char appoggio[30];
    int ix;
    int rigaFile=0;
    char *chiavi[9]={"text","backimg","show1","action","a_s","x","y","align","timeout"};
    
    // uso nchiavi e non sizeof perch� a volte voglio lavorare con meno elementi
    int nChiavi=9;
    
    if(f=FSfopen(file,FS_READ)) {
    
		if(cercaGruppo(f,chiave)) {    
            if(mnu.text[0]!=NULL)
                free(mnu.text);
            *mnu.text=NULL;
            *mnu.backimg=NULL;
            *mnu.show1=NULL;
            *mnu.action=NULL;
            
            mnu.x=-1;
            mnu.y=-1;
            sprintf(mnu.align,"");
            
            while(FSReadLine(f,buf)>0) {
                if(buf[0]=='['){
                    
                    break;
                }    
                
              //  sprintf(appoggio,"dimChiavi %u",sizeof(chiavi));
              //  LCDPutStringCenter(1,appoggio);
              //  DIMENSIONE=sizeof(chiavi);
                
                for( c=0;c<nChiavi;c++){
                    n=strlen(chiavi[c]);
                    
                    if(strncmp(buf,chiavi[c],n)==0) {
                       
                        sprintf(risultato,"%s",buf+n+1);
                        switch ((comandi)c) {
                            case text:
                                sprintf(appoggio,"%s","$variabile");
                                
                                if(strncmp(risultato,appoggio,strlen(appoggio))==0){
                                    rigaFile=atoi(risultato+strlen(appoggio));
                                    //sprintf(risultato,"buon%u",rigaFile);
                                    //se rigaFile � maggiore di 0 ho trovato il numero di rigaFile della variabile inserita
                                    //nel file lingue a cui aggiungere 1 x rigaFile di intestazione
                                 
                                }
                                
                                mnu.text=(char*)malloc(sizeof(char)* (strlen(risultato) + 1));
                                
                                
                                sprintf(mnu.text,"%s",risultato);                          
                            break;
                            case x:
                                mnu.x=atoi(risultato);
                            break;
                            case y:
                                mnu.y=atoi(risultato);
                            break;                            
                            case align:
                                sprintf(mnu.align,"%s",risultato);
                            break;
                            case backimg:
                                sprintf(mnu.backimg,"%s.bmp",risultato);                             
                            break;
                            case show1:
                                sprintf(mnu.show1,"%s",risultato);                                
                            break;
                            case action: 
                                sprintf(mnu.action,"%s",risultato);
                            break;
                            case a_s: 
                               
                                ix=atoi(buf+n);
                                                              
                                sprintf(appoggio,"%s",risultato+1);
                                mnu.ixbutton[indice]=ix;
                                
                                if(mnu.btn[indice]!=NULL)
                                    free(mnu.btn[indice]);
                                mnu.btn[indice]=(char*)malloc(sizeof(char)* (strlen(appoggio) + 1));
                                strcpy(mnu.btn[indice], appoggio);
                                
                                                               
                                indice++;
                            //    sprintf(mnu.button,"%s%s;",mnu.button,risultato+1);
                                
                            break;
                            
                        } //end switch
                    }  // end if
                }//end for                
            } // end while
        } // end if cercaGruppo
        FSfclose(f);
        
       
        if(rigaFile>0){
            cercaRigaLingua(rigaFile);
        }
        
        
        return 1;
	}//end if open file
    
    
    return 0;
}



/*FABRIZIO */  


void caffe_1(){
    ultimoCaffe=1;
    flagAutomatico=TRUE;
    tempoOperazione=Cycle_1Tazza*50;
    CycleTime = Cycle_1Tazza;
    Disable_ALL_Led_White();
    Enable_Led(val,'0');
    Enable_RelayMacina();
    EnableFan();
    EnablePWMVibr(1);
    stopVibroPost=TRUE;
    periodoPostVibro=POSTVIBRO;
    PWMVibr();
    Disable_Mec_Button();                         
    LED8_SetHigh();
    if (flag_nok_sd){
        LCDCls();
        
        LCDPutStringXY(5,5, "MACINAZIONE 1 TAZZA");
    }
    else{
        //per debug
        //    sprintf(buffer,"1 tazza %u",tempoOperazione);
        //    LCDPutStringXY(3,3, buffer);
    }
        
        
    FiltroCheck = 0;
    count_down = 0;
    
        


//#ifndef DEBUG_FAB
//    UART1_Disable();
//#endif
    
    
    
    //TMR1_Stop();
    Comm_State = 2;
   /* Timer1_Counter(PR1TICK20MILLI);
   // TMR1_Start();*/
    
}

void caffe_2(){
    flagAutomatico=TRUE;
    tempoOperazione=Cycle_2Tazza*50;
    // tempoOperazione=300;
    ultimoCaffe=2;
    CycleTime = Cycle_2Tazza;
    Disable_ALL_Led_White();                
    Enable_Led(val,'0');
    Enable_RelayMacina();
    EnableFan();
    EnablePWMVibr(1);
    stopVibroPost=TRUE;
    periodoPostVibro=POSTVIBRO;
    PWMVibr();
    Disable_Mec_Button();                         
    LED8_SetHigh();
  
    if (flag_nok_sd){
        LCDCls();
        LCDPutStringXY(5,5, "MACINAZIONE 2 TAZZE");
    }
    else{
        // per debug
        //    sprintf(buffer,"2 tazze %u",tempoOperazione);
        //    LCDPutStringXY(3,3, buffer);
    }

    FiltroCheck = 0;
    count_down = 0;
 //   #ifndef DEBUG_FAB
 //       UART1_Disable();
 //   #endif
   // TMR1_Stop();
    Comm_State = 2;
   // Timer1_Counter(PR1TICK20MILLI);
   // TMR1_Start();
    
}

void sequenzaBW(){
    BYTE i;
    
    Disable_ALL_Led_Blue();
    Disable_ALL_Led_White();
    
    for(i=0;i < 10;i++){
        Enable_Led((char)(i+'0'),'0');
        
        if(i>0)
            Disable_Led((char)((i-1)+'0'),'0');
      __delay_ms(250);
        
        
    }
    Disable_Led((char)((i-1)+'0'),'0');
    
    for(i=0;i < 10;i++){
        Enable_Led((char)(i+'0'),'1');
        if(i>0)
            Disable_Led((char)((i-1)+'0'),'1');
        
        __delay_ms(250);
    }
    Disable_Led((char)((i-1)+'0'),'1');
}

void manuale(){
  //  char tastoPremuto;
  //  tastoPremuto=ultimoPremuto +  '0';
    tempoOperazione=0;
    ultimoCaffe=0;
    flagManuale=TRUE;
    Disable_ALL_Led_White();
    Enable_Led(val,'0');
    Enable_RelayMacina();
    EnableFan();
    EnablePWMVibr(1);
    PWMVibr();
    stopVibroPost=TRUE;
    periodoPostVibro=POSTVIBRO;
    Enable_Only_Mec_ButtonX(val);
    LED8_SetHigh();
        
    if (flag_nok_sd){
        LCDCls();
        LCDPutStringXY(5,5, "MACINAZIONE MANUALE");
    }
    FiltroCheck = 0;
    start_timer_macin_manual = 1;    
    Comm_State = 2;
 //   TMR1_Start();    
}

void portaFiltro(int tipo){
    char appoggio[50];
    
    //running=FALSE;
    sprintf(appoggio,"%s",mnu.text);
    cercaRigaLingua(67); 

 
    if (!flag_nok_sd){
        PutBmp8BPPExt(40, 0, (void *) "portaF.bmp" , IMAGE_NORMAL);
    }
  
  
    LCDPutStringCenter(3, mnu.text);
    flgInterrotto=tipo;
    
    free(mnu.text);
    *mnu.text=0;            
    mnu.text=(char*)malloc(sizeof(char)* (strlen(appoggio) + 1));

    sprintf(mnu.text,"%s",appoggio);
   
    Disable_RelayMacina();
    DisableFan();
    
    EnablePWMVibr(0);
    
    ultimoCaffe=tipo;
   
    
    tempoOperazione=150;
    Disable_ALL_Led_White();
    Enable_Led(val,'0');
    refreshed=TRUE;
    running=TRUE;
 
}

void plotta_videata(){
   char x=-1,y=-1;
 
   
   ClrWdt();   

   
  // LCDCls();
   if(mnu.backimg[0]!=NULL)
       PutBmp8BPPExt(40, GetRealOrgY(), (void *) mnu.backimg , IMAGE_NORMAL);                
   if(mnu.text[0]!=NULL){
       if((mnu.y & 0xFF )> 0 ) 
         y=mnu.y;

       if( (mnu.x & 0xFF) <getSizeX()){ 
           x=mnu.x;

          LCDPutStringXY(x,y,mnu.text);                 
       }
       else{  
               if(strncmp(mnu.align,"center",6)==0){
                   //x=(int)((26-strlen(mnu.text))/2);

                   LCDPutStringCenter(y,mnu.text);
                   //LCDPutStringXY(x,y+1,mnu.text);                 

               }
               else 
                   LCDPutStringXY(1,y,mnu.text);
       }    
   }   
   
}

void gestioneMenu(void){
    int i,n,ix;
	
	static signed char currMenu=0,currLang /* default, italiano ;) */;
    char profilo[50];
    char x=-1,y=-1;
  
    char chiave[50];
    char appoggio[50];
    char *returnMenu=NULL;  
    BOOL returnHome=FALSE;
   // LCDPutStringXY(1,1,mnu.button);
    BYTE t1,t2,t3;
 
    //LCDPutStringXY()
    
    
    
    
    
    
    
    
    if(stopVibroPost==TRUE && running==FALSE){
        // Stop VIBRO posticipato dopo fine azione di macinazione- 1,2 caff� 
         if(flag_20msec){
            if(periodoPostVibro>0){
                periodoPostVibro--;
            }
            flag_20msec=0;
         }
     
         if(periodoPostVibro<=0){
             stopVibroPost=FALSE;
             EnablePWMVibr(0);
         }
    
        
         return;
    }
    
    
    if(collaudo==FALSE){
        
         if(running==FALSE){
            
             
             refresh_orario();
             
             sprintf(appoggio,"%04u/%02u/%02u_%02u:%02u",__anno,__mese,__giorno,__ore,__minuti);

             //   sprintf(appoggio,"%u",periodoPostVibro);
             LCDPutStringCenter(2,appoggio);
         }


         if(capAttivo==1){
               if(ultimoCaffe==2){
                 Disable_Led('5','1');
                 Enable_Led('4','1');

                  sprintf(appoggio," 2 SHOT:%0.1f",Cycle_2Tazza);
                  LCDPutStringCenter(7,appoggio);
                  if(unita_Tazza<1.0) 
                     LCDPutStringXY(15,9,"   -");
                  else
                     LCDPutStringXY(15,9,"--");            
              }else{
                      if(ultimoCaffe==1){
                         Disable_Led('4','1');
                         Enable_Led('5','1');
                         sprintf(appoggio," 1 SHOT:%0.1f",Cycle_1Tazza);
                         LCDPutStringCenter(7,appoggio);
                         if(unita_Tazza<1.0) 
                             LCDPutStringXY(15,9,"   -");
                         else
                             LCDPutStringXY(15,9,"--");
                      }
              }


         }

         if ((refreshed==TRUE)&& (running==FALSE)){
             refreshed=FALSE;
             //debug
             //LCDCls();

             if(mnu.action[0]!=NULL){
                 //LCDPutStringXY(1,10,mnu.action);
                 // Se il menu caricato prevede un'azione la eseguo subito
                 chiave[0]=NULL;
                 sprintf(chiave,"%s","1coffe");
                 n=strlen(chiave);
                 if(strncmp(mnu.action,chiave,n)==0){
                     if(capAttivo==0){
                         if (flag_ok_press){    
                             caffe_1();
                             running=TRUE;
                         }else{
                             portaFiltro(1);                         
                             return;
                         }
                     }
                     else{
                         ultimoCaffe=1;
                         return;
                     }



                 }
                 else {
                         chiave[0]=NULL;
                         sprintf(chiave,"%s","2coffe");
                         n=strlen(chiave);
                         if(strncmp(mnu.action,chiave,n)==0){   
                             if(capAttivo==0){
                                 if (flag_ok_press){    
                                     caffe_2();
                                     running=TRUE;
                                 }else{
                                     portaFiltro(1);                         
                                     return;
                                 }
                             }
                             else{
                                    ultimoCaffe=2;
                                    return;
                                 }           
                         }
                         else{

                                 chiave[0]=NULL;
                                 sprintf(chiave,"%s","manual");
                                 n=strlen(chiave);
                                 if(strncmp(mnu.action,chiave,n)==0){ 
                                    if(capAttivo==0){
                                        /*PER MACINAZIONE MANUALE NON SI RICHIEDE PRESENZA PORTA FILTRO
                                         
                                         if (flag_ok_press){                                
                                             manuale();                                    
                                             running=TRUE;
                                         }
                                         else{//provvisorio
                                             portaFiltro(0);
                                             return;
                                         }*/
                                         manuale();                                    
                                         running=TRUE;
                                         
                                    }else
                                        return;
                                 }
                                 else{
                                         chiave[0]=NULL;
                                         sprintf(chiave,"%s","increment");
                                         n=strlen(chiave);
                                         
                                         
                                           salti=salti | 1;
                                         //  sprintf(appoggio,"salto %i",salti); 
                                         //  LCDPutStringCenter(9,appoggio); 
                                         
                                         if(strncmp(mnu.action,chiave,n)==0){
                                             Disable_ALL_Led_White();
                                             Enable_Led(val,'0');

                                             if(capAttivo==0){
                                                 Enable_Only_Mec_ButtonX(val);
                                             }

                                             switch(ultimoCaffe){
                                                 case 1:

                                                      tempoOperazione=20;
                                                      if(Cycle_1Tazza <500){
                                                             Cycle_1Tazza+=(unita_Tazza*salti);
                                                      }
                                                      salti=1;
                                                      sprintf(appoggio," 1 SHOT:%0.1f",Cycle_1Tazza);
                                                      LCDPutStringCenter(7,appoggio); 
                                                 break;
                                                 case  2:

                                                      tempoOperazione=20;

                                                      if(Cycle_2Tazza <500){
                                                             Cycle_2Tazza+=(unita_Tazza*salti);
                                                      }
                                                      sprintf(appoggio," 2 SHOT:%0.1f",(Cycle_2Tazza*salti));
                                                      salti=1;
                                                      LCDPutStringCenter(7,appoggio); 
                                                 break;    
                                                 default:                                             
                                                      tempoOperazione=0;
                                                      break;
                                             }       
                                             //reset_azione();
                                             if(capAttivo==0){                                            
                                                 returnHome=TRUE;     
                                                 running=TRUE;
                                                 return;
                                             }
                                             return;
                                         }else{
                                             chiave[0]=NULL;

                                             sprintf(chiave,"%s","decrement");
                                             n=strlen(chiave);
                                             if(strncmp(mnu.action,chiave,n)==0){
                                                 Disable_ALL_Led_White();
                                                 Enable_Led(val,'0');
                                                 if(capAttivo==0){

                                                     Enable_Only_Mec_ButtonX(val);
                                                 }

                                                 switch(ultimoCaffe){
                                                     case 1:
                                                          tempoOperazione=20;
                                                          if(Cycle_1Tazza >2){
                                                             Cycle_1Tazza-=(unita_Tazza*salti);
                                                          }
                                                          salti=1;
                                                          sprintf(appoggio," 1 SHOT:%0.1f",Cycle_1Tazza);
                                                          LCDPutStringCenter(7,appoggio); 
                                                     break;
                                                     case  2:
                                                          tempoOperazione=20;
                                                          if(Cycle_2Tazza >2){
                                                             Cycle_2Tazza-=(unita_Tazza*salti);
                                                          }
                                                          salti=1;
                                                          sprintf(appoggio," 2 SHOT:%0.1f",Cycle_2Tazza); 
                                                          LCDPutStringCenter(7,appoggio); 
                                                     break;    
                                                     default:                                             
                                                          tempoOperazione=0;
                                                          break;
                                                 }       
                                                 //reset_azione();
                                                 if(capAttivo==0){
                                                     returnHome=TRUE;     
                                                     running=TRUE;
                                                     return;
                                                 }
                                                 return;
                                             }else{
                                                     chiave[0]=NULL;
                                                     sprintf(chiave,"%s","settings");
                                                     n=strlen(chiave);
                                                     if(strncmp(mnu.action,chiave,n)==0){
                                                         if(capAttivo==0)
                                                             initSetting();    
                                                         else{
                                                             unita_Tazza=0.1;

                                                         }
                                                         return;
                                                     }
                 
                                                 }

                                         }
                                 }
                         }
                 }             
             }  
             
             
           plotta_videata(); 

        }

        if(running==TRUE){ 
          //RUNINNG 
            if(flag_ok_press){//porta filtro presente
                if(flgInterrotto!=-1){ //Macinazione interrotta dopo essere stata avviata
                     //returnHome=TRUE;
                     running=FALSE;
                     refreshed=TRUE;
                     tempoOperazione=0;

                     switch( flgInterrotto){
                         case 1: //se viene messo porta filtro riparte caffe 1
                             flgInterrotto=-1;
                             plotta_videata();
                             caffe_1();
                             running=TRUE;
                         break;
                         case 2: //se viene messo porta filtro riparte caffe 1
                             flgInterrotto=-1;
                             plotta_videata();
                             caffe_2();
                             running=TRUE;
                         break;
                         default:                      
                             reset_azione();

                     }           
                }
            }
            if(flagManuale==TRUE){//e' Attiva MACINAZIONE MANUALE
               /* if(!flag_ok_press){
                     reset_azione();
                     return;
                }*/


                if(flag_20msec){
                     tempoOperazione++;
                     flag_20msec=0;
                }
            }else
            {//Qualsiasi altro tipo d'azione  1CAFFE,2CAFFE,INCR,DECR,CONFERMA
                     if(flag_20msec){
                         if(tempoOperazione >0){
                           tempoOperazione--;
                         }
                         flag_20msec=0;
                         if((!flag_ok_press) && (flgInterrotto==-1)){//porta filtro NON presente AND Macinazione non precedente interrotta
                             //si � interrotta una macinazione in corso
                             tempoOperazione=0;//azzero timer countdown 
                         }
                     }
                     if (tempoOperazione==0){
                             //State= STATE_FINITO;

                             if ((flagAutomatico==TRUE)||(!flag_ok_press)){//interrompo macinazione in corso
                                 if((flagAutomatico==TRUE && flag_ok_press)){
                                     sprintf(appoggio,"t: %02u.%02u  ",0,0);
                                     LCDPutStringXY(8,11,appoggio);
                                 }                      

                                 reset_azione();                       

                             }else{ //interrompo timer no macinazione
                                 running=FALSE;
                                 refreshed=TRUE;
                                 Enable_ALL_Led_White();
                                 Disable_ALL_Led_Blue();
                             }
                             returnHome=TRUE;

                     }
             }
             t1=(tempoOperazione *2) /100;
             t2=(tempoOperazione*2) % 100;
             sprintf(appoggio,"t: %02u.%02u  ",t1,t2);

             if(flgInterrotto!=-1){//MACINAZIONE INTERROTTA
                 LCDPutStringXY(8,7,appoggio);
             }else if(running){
                     // reset_azione puo' portarlo al false
                     if ((flagManuale==TRUE)||(flagAutomatico==TRUE)) //Non mostro il timer su incremento e decremento
                         LCDPutStringXY(8,11,appoggio);
             }
         } 
    }
    

    if((running==FALSE) || (flagManuale==TRUE)){
        // ciclo su tutti i tasti, se presenti, che hanno un'azione nel menu 
        for(i=0;i<sizeof(mnu.btn)/sizeof(mnu.btn[0]);i++){
           ix=mnu.ixbutton[i];
           // vado avanti fino a valorizzazione effettiva dei tasti
           if(!ix){
               break;
           }           
           //Se un tasto � premuto. Recupero le informazioni legate alla pressione
           //e quali azioni intraprendere
           if (BUTTON[ix] == '0'){
               if(running==FALSE){
                    BUTTON_OLD[ix]='0';
                    ultimoPremuto=ix;
                    //non metto pi� configParms.lingua_default carico sempre il menu della configurazione italiana
                    //e uso configParms.lingua_default solo per leggere le variabili in lingua
                    // lascio la struttura perche cos� si possono definire versioni diverse per lingua tradotte
                    sprintf(profilo,"%s_%u",mnu.btn[i],(BYTE)0);
                    
                 
                    
                    loadMenu(MENU_INI,profilo);
                    currMenu=mnu.btn[i]+strlen("menu");
                    refreshed=TRUE;
               }
           }else{ 
               
                    
               
                    if((flagManuale==TRUE) && (ix==ultimoPremuto+1) && (BUTTON_OLD[ultimoPremuto]=='0') && (BUTTON[ultimoPremuto]=='1')){
                        BUTTON[ultimoPremuto]='1';
                        BUTTON_OLD[ultimoPremuto]=BUTTON[ultimoPremuto];
                        
                     //PROVVISORIO  
                     //   sprintf(appoggio,"indice%u val:%c",ultimoPremuto,val);
                        
                     //   LCDPutStringXY(2,6,"RESET AZIONE 1");
                      //  LCDPutStringXY(2,7,appoggio);
                      //  __delay_ms(1000);
                        
                        reset_azione();
                        returnHome=TRUE;
                        //Comm_State=11;
           //             FileLog("m",PIC24RTCCGetDate(),PIC24RTCCGetTime(),tempoOperazione);
                    }
           }
       }
    }
    if (returnHome==TRUE){//Ritorno al menu specificato in menu.action
            //ricerco su action il menu di ritorno se specificato         

        
        strcpy(profilo,strstr(mnu.action, "menu"));          
            
            
            
            if (strncmp(profilo,"menu",4)==0){//trovato menu di ritorno
                returnMenu=(char*)malloc(sizeof(char)* (strlen(profilo) + 1));               
                strcpy(returnMenu,profilo);
                n=strlen(returnMenu);
                if(strcmp(returnMenu+n-1,";")==0){
                    returnMenu[n-1]=0;
                }
                //non metto pi� configParms.lingua_default caricao sempre il menu della configurazione italiana
                //e uso configParms.lingua_default solo per leggere le variabili in lingua
                // lascio la struttura perche cos� si possono definire versioni diverse per lingua tradotte
                sprintf(returnMenu,"%s_%u",returnMenu,(BYTE)0);
            }else{ //altrimenti setto menu0
                    returnMenu=(char*)malloc(sizeof(char)* (strlen(mnu.btn[i])+strlen(configParms.lingua_default) + 2));
                    sprintf(returnMenu,"%s_%u",mnu.btn[i],(BYTE)0);
            }    
   //     reset_azione();
        //     sprintf(profilo,"menu%u_%u",0,configParms.lingua_default);
        returnHome=FALSE;
        loadMenu(MENU_INI,returnMenu);
        if(returnMenu[0]!=NULL)
            free(returnMenu);
        *returnMenu=0;   
    }
    //return;
}


void initSetting(){
    flg_setting=TRUE;
    refreshed=FALSE;
    running=FALSE;


    Enable_Led('2','0');
    Enable_Led('6','0');
    Enable_Led('7','0');
    Enable_Led('9','0');
    Disable_ALL_Led_White();
    LCDCls();

    
    _cursore=0;
    _cursorePrec=-1;
    _sottoMenu=0;
}


BOOL setDate(){
    int c;
    char appoggio[22];
    BOOL terminato=FALSE;

    LCDPutStringXY(2,2,"Date YYYY/mm/dd hh:mm:ss");
   
    switch(__ix){
        case 0:
            __anno=__cursore;
            LCDPutStringXY(2,5,"____               ");
            
        break;
        case 1:
            __mese=__cursore;
            LCDPutStringXY(2,5,"     __   ");
          
        break;
        case 2:
            __giorno=__cursore;
            LCDPutStringXY(2,5,"        __");
        break;
        case 3:
            __ore=__cursore;
            LCDPutStringXY(10,5,"   __       ");
        break;
        case 4:
            __minuti=__cursore;
            LCDPutStringXY(13,5,"   __    ");
        break;    
        case 5:
            __secondi=__cursore;
            LCDPutStringXY(13,5,"      __ ");
        break;    
        

    }
          
    for(c=0;c<sizeof(BUTTON);c++){


     
        sprintf(appoggio,"%04u/%02u/%02u %2u:%2u",__anno,__mese,__giorno,__ore,__minuti);
        LCDPutStringXY(2,4,appoggio);
        
        if (BUTTON[c] == '0'){
            BUTTON_OLD[c]='0';

            switch(c){
                case 2:
                   //freccia gi�   
                   if(__cursore > __minimo)
                        __cursore=(__cursore-1);
                   else
                       __cursore=__massimo;
                               
                   //__delay_ms(150);
                   BUTTON[c]='1';
                break;

                case 6: //freccia su
                  if(__cursore<__massimo)
                        __cursore=(__cursore+1);
                  else
                      __cursore=__minimo;
                  
                  //__delay_ms(150);
                  BUTTON[c]='1';                   
                break;
                case 9: //ricomincio settaggio
                    refresh_orario();
                    
                    __cursore=__anno;
                    __ix=0;
                    __massimo=4000;
                    __minimo=2019;
                    //__delay_ms(150);
                    BUTTON[c]='1';   
                break;
                        
                case 7:// conferma
                    
                    
                    
                    switch(__ix){
                        case 0: //conferma su anno
                            __anno=__cursore;
                           
                            // mi sposto su mese
                            __ix++;
                            __cursore=__mese;
                            __massimo=12;
                            __minimo=1;
                            
                        break;
                        case 1://conferma su mese
                            __mese=__cursore;
                            
                            
                            //mi sposto su giorno
                            __ix++;
                            __cursore=__giorno;
                            if((__mese==11) || (__mese==4) || (__mese==6) || (__mese==9))
                                __massimo=30;
                            else{
                                    if(__mese==2){
                                        if (bisestile(__anno))
                                            __massimo=29;
                                        else
                                            __massimo=28;
                                    }
                                    else 
                                        __massimo=31;
                            }  
                            __minimo=1;
                        break;    
                        case 2:
                            // conferma su giorno
                            __giorno=__cursore;
                            __ix++;
                            //mi sposto su ore
                            __cursore=__ore;
                            __minimo=0;
                            __massimo=23;
                           
                        break;    
                        case 3:
                            // conferma su ore
                            __ore=__cursore;
                            __ix++;
                       
                            //mi sposto su minuti
                            __cursore=__minuti;
                            __minimo=0;
                            __massimo=59;
                        break;    
                        case 4:
                            // conferma su minuti
                            __minuti=__cursore;
                            
                            
                            //mi sposto su secondi
                            //__ix++;
                            //__cursore=__secondi;
                            //__minimo=0;
                            //__massimo=59;
                            __massimo=4000;
                            __minimo=2019;
                                                                        
                            
                            
                            rtc_time_to_set.year=-2000+__anno;
                            rtc_time_to_set.month=__mese;
                            rtc_time_to_set.date=__giorno;
                            rtc_time_to_set.hour=__ore;               
                            rtc_time_to_set.mins=__minuti; 
                            
                            
                            rtc_set_new_data();
                            
                            
                            terminato=TRUE;
                            __ix=0;

                        break;    
                        
                    }  // end  switch(__ix)                 
                    //__delay_ms(150);
                    BUTTON[c]='1'; 
                break;
                
            }
        } 
    }
    return terminato;
}

  
void loadImpostazioni(void){
    int c;
    int ix=3;
    int iy=7;
    int max;
    char voci[6][16];
    char titolo[30];
  
    
      switch(_sottoMenu){
        case 0:
            strcpy(voci[0],"LANGUAGE    ");
            strcpy(voci[1],"DATE        ");
            strcpy(voci[2],"PORTAF. ");
            strcpy(voci[3],"SAVE        ");
            strcpy(voci[4],"EXIT        ");
            strcpy(titolo,"SETTINGS     ");
            max=5;
           
            
        break;
        case 1:
         
            strcpy(voci[0],"ITALIANO    ");
            strcpy(voci[1],"ENGLISH     ");
            strcpy(voci[2],"DEUTSCH     ");
            strcpy(voci[3],"FRANCAIS    ");
            strcpy(voci[4],"ESPANOL     ");
            strcpy(voci[5],"EXIT        ");
            strcpy(titolo,"LANGUAGES    ");
            _sottoMenuRitorno=0;
            max=6;
            
        break;
        case 2:
            
            if(setDate()==TRUE){
                _sottoMenu=_sottoMenuRitorno;
                _cursore=0;
                _cursorePrec=-1;
                LCDCls();
            }
            return;
        break;
        case 3:
                strcpy(voci[0],"ON          ");
                strcpy(voci[1],"OFF         ");
                strcpy(voci[2],"EXIT        ");
                strcpy(titolo,"PORTAFILTER  ");
                _sottoMenuRitorno=0;
                max=3;
                
        break;  
        
        
//        case 3:
//                LCDCls();
//                if(updateSDCardFileSetting(SETTINGS_INI)==1)
//                    LCDPutStringCenter(5,"SAVE:OK");
//                else
//                    LCDPutStringCenter(5,"SAVE:KO");
//                __delay_ms(500);
//                LCDCls();
//                _sottoMenuRitorno=0;
//                _cursore=0;
//                _cursorePrec=-1;
//                return;
//        break;
    }
    
   
      
    if(_cursore!=_cursorePrec ){
       
        
        LCDPutStringCenter(mnu.y,titolo);
        for(c=0;c<max;c++){
            if(c==_cursore){
                LCDPutStringXY(10,iy+c,voci[c]);
                
                if(_cursorePrec!=-1)
                    PutBmp8BPPExt((ix+strlen(voci[0]))*16, 4+(iy+_cursorePrec)*16, (void *) "cursoreD.bmp" , IMAGE_NORMAL);
                    PutBmp8BPPExt((ix+strlen(voci[0]))*16, 4+(iy+c)*16, (void *) "cursore.bmp" , IMAGE_NORMAL);
                    _cursorePrec=_cursore;
            }
            else
                LCDPutStringXY(10,iy+c,voci[c]);
        }
        
   
    }
    
    for(c=0;c<sizeof(BUTTON);c++){
       if (BUTTON[c] == '0'){
           BUTTON_OLD[c]='0';
           ultimoPremuto=c;
           
           
           switch(c){
           case 2:
                   //freccia gi�                  
                   _cursore=(_cursore+1  ) % max;
                   //__delay_ms(150);
                   BUTTON[c]='1';
                   return;
            break;
            
            case 6: //freccia su
                  if(_cursore==0)
                      _cursore=max-1;
                  else
                   _cursore=(_cursore-1  ) % max;
                                     
                   //__delay_ms(150);
                   BUTTON[c]='1';
                   return;
               break;
            case 7:
                 // conferma
                __delay_ms(50);
                //conferma su exit
                if(strncmp(voci[_cursore],"EXIT",4)==0){
                    if(_sottoMenu==0){ //exit prima videata settaggi
                                                
                        
                        /*;
                        reset_azione();

                        Disable_ALL_Led_Blue();
                        Enable_ALL_Led_White();
                        
                        */
                        
                       
                        *mnu.action=NULL;
                      
                        reset_azione();
                        BUTTON[c]='1';
                            
                        flg_setting=FALSE;
                                           
                        sprintf(returnMenu,"menu%u_%u",0,0);
                        loadMenu(MENU_INI,returnMenu);
                        
                        return;
                    }
                    else{ 
                            //ritorno alla videata di ritorno
                            LCDCls();
                            _sottoMenu=_sottoMenuRitorno;
                            _cursorePrec=-1;
                            _cursore=0;
                    }
                    __delay_ms(50);
                    BUTTON[c]='1';
                    return;
                }  // end conferma exit
                
               //conferma diversa da "Exit
                
                 
                switch(_sottoMenu){
                    case 0:
                            // conferma prima videata
                            switch(_cursore){
                                case 0://Language
                                       
                                      // __delay_ms(150);
                                       BUTTON[c]='1';
                                       _sottoMenuAndata=1;
                                       _sottoMenu=_sottoMenuAndata;
                                       _cursorePrec=-1;
                                       _cursore=0;
                                       LCDCls();
                                       return;
                                break;
                                case 1:// Date
                                       
                                        refresh_orario();
                                        
                                        __cursore=__anno;
                                        __ix=0;
                                        
                                        __massimo=4000;
                                        __minimo=2019;
                                        _sottoMenuRitorno=0;                                   
                                        _sottoMenuAndata=2;
                                   //     __cursore=__anno;
                                        _sottoMenu=2;
                                        LCDCls();
                                        __delay_ms(100);
                                        return;
                                
                                break;    
                                case 2: //PORTAFILTRO
                                       BUTTON[c]='1';
                                       _sottoMenuRitorno=0;
                                       _sottoMenu=3;
                                       _sottoMenuAndata=3;
                                       _cursorePrec=-1;
                                       _cursore=0;
                                       LCDCls();
                                       return;
                                break;
                         
                                case 3: //SAVE
                                        BUTTON[c]='1';
                                        LCDCls();
                                        if(updateSDCardFileSetting(SETTINGS_INI)==1)
                                            LCDPutStringCenter(5,"SAVE:OK");
                                        else
                                            LCDPutStringCenter(5,"SAVE:KO");
                                        __delay_ms(1000);
                                        
                                        *mnu.action=NULL;
                      
                                        reset_azione();
                                        BUTTON[c]='1';

                                        flg_setting=FALSE;

                                        sprintf(returnMenu,"menu%u_%u",0,0);
                                        loadMenu(MENU_INI,returnMenu);

                                        return;
                                        
                                break;



                            }
                    case 1: // conferma seconda videata lingua
                               
                                // __delay_ms(150);
                                BUTTON[c]='1';
                                 LCDCls();
                                configParms.lingua_default=(BYTE)(_cursore);
                              
                                _cursorePrec=-1; 
                                _sottoMenu=_sottoMenuRitorno;                            
                                _cursore=0;
                             
                                return;      
                    case 3: //conferma seconda videata portafiltro
                                if (_cursore==0)
                                    configParms.pressino=(BYTE)1;
                                else
                                    configParms.pressino=(BYTE)0;
                                          
                                LCDPutStringXY(15,iy+_cursore,"OK");
                                                       
                                return;    
                                
                    break;
                
                }
                break;    
            }
    
       }
    }
      
     
}


void reset_azione(){
    BYTE c;
    running=FALSE;
    refreshed=TRUE;
    if(flagManuale==TRUE){
        flagManuale=FALSE;
        start_timer_macin_manual = 0;
    }else{
        if(flagAutomatico==TRUE){
            flagAutomatico=FALSE;
        }
    }
    flgInterrotto=-1;
    Comm_State = 1;
    count_down = 0;
    unita_Tazza=1;
    val='\0';
    old_val = '\0';
    FiltroCheck = 0;
    ok_macin_manual = 0;
    ok_macin_auto = 0;
    start_timer_macin_manual = 0;
    
    
               
    for(c=0;c< sizeof(BUTTON_OLD);c++){
       BUTTON_OLD[c]='1';
    }
    
    Disable_RelayMacina();
    DisableFan();
    if(stopVibroPost==FALSE) 
        EnablePWMVibr(0);
    else
        periodoPostVibro=POSTVIBRO;
      
        
   // Enable_Mec_Button();
    //TMR1_Stop();
  //  #ifndef DEBUG_FAB
  //      UART1_Disable();
  //      UART1_Enable();  
  //  #endif
}


void refresh_orario(void){
    __anno=2000 + (10*((rtc_time.year & 0xF0)>>4))+ (rtc_time.year & 0xF);
    __mese=(10*((rtc_time.month & 0xF0)>>4))+ (rtc_time.month & 0xF);
    __giorno=(10*((rtc_time.date & 0xF0)>>4))+ (rtc_time.date & 0xF);

    __ore=(10*((rtc_time.hour & 0xF0)>>4))+ (rtc_time.hour & 0xF);
    __minuti=(10*((rtc_time.mins & 0xF0)>>4))+ (rtc_time.mins & 0xF);
    __secondi=(10*((rtc_time.sec & 0xF0)>>4))+ (rtc_time.sec & 0xF);
}



int main(void)
{
    
    BYTE blinkCnt=0;
	int i;
  	static signed char oldMenu=-1,currMenu=0 /* default, italiano ;) */;
    unsigned long int period_to_refresh_rtc; 
    char profilo[20]; 

    //init(); //substitute CLOCK Initialize  
    CLOCK_Initialize();
    PIN_MANAGER_Initialize();
    CLOCK_Initialize();
    
    INTERRUPT_Initialize();
    TMR2_Initialize();
    TMR1_Initialize();
    UART1_Initialize();  

    reset_keyboard();//RESET KEYBOARD

    ini_i2c_rtcc();//initialize I2C1 for communication to RTC MCP79400 

    OUT_SNS_Pres_SetDigitalOutput();
    IN_SNS_Pres_SetDigitalInput();
    
    OUT_SNS_Pres_SetHigh();

	curPosX=GetRealOrgX(); curPosY=GetRealOrgY();

    InitGraph();     // Graphics
	ClrWdt();

	if(MDD_MediaDetect()){//Ricerca Sd Card    
        SDcardOK=FSInit();  // File system 
        flag_nok_sd = 0;
    } else {
        
                DisplayBacklightOn();		//serve per attivare display!
                backLight=1;
                SetColor(WHITE);
    
                       
                LCDPutStringXY(5,5, "INSERIRE mSD-CARD");
                //while(!MDD_MediaDetect());
                LCDCls();
                flag_nok_sd = 1;
        
    }


    // FABRIZIO
    //Legge da Sd Card i parametri di configurazione, nell'impossibilit�
    //vengono impostati dei valori di default
    loadSettings();
    
	if(configParms.backLight) {
		DisplayBacklightOn();		//serve per attivare display!
		backLight=1;
        SetColor(WHITE);
    }

     
//  Initialize the RTCC
//  Turn on the secondary oscillator
	__asm__ ("MOV #OSCCON,w1");
	__asm__ ("MOV.b #0x02, w0");
	__asm__ ("MOV #0x46, w2");
	__asm__ ("MOV #0x57, w3");
	__asm__ ("MOV.b w2, [w1]");
	__asm__ ("MOV.b w3, [w1]");
	__asm__ ("MOV.b w0, [w1]");
	
	PIC24RTCCSetDate( DEFAULT_YEARS, DEFAULT_MONTH_DAY );
	PIC24RTCCSetTime( DEFAULT_WEEKDAY_HOURS, DEFAULT_MINUTES_SECONDS );
    //	__builtin_write_OSCCONL(OSCCON | 0x0002); 		//DAL FORUM... MA CREDO CI SIA GIA'...
	RCFGCAL = 0x8000;

	//scanKBD(); 

 	SetColor(BLACK);
	ClearDevice();
    
     
//    configParms.test=1;
    /* FABRIZIO LO LEGGE DAL SETTINGS SD_CARD*/
    configParms.splash=1;
	if(configParms.splash) {		// nostra estensione
		ClearDevice();
    	// draw border lines to show the limits of the 
        // left, right, top and bottom pixels of the screen
        // draw the topmost horizontal line
          SetColor(BRIGHTRED);
        //  SetColor(BRIGHTYELLOW); hmmm , manca il rosso...
          WAIT_UNTIL_FINISH(Line(GetRealOrgX(),GetRealOrgY(),GetRealMaxX()+GetRealOrgX(),GetRealOrgY()));
          // draw the rightmost vertical line
          SetColor(BRIGHTYELLOW);
          WAIT_UNTIL_FINISH(Line(GetRealMaxX()+GetRealOrgX()-1,GetRealOrgY(),GetRealMaxX()+GetRealOrgX()-1,GetRealMaxY()+GetRealOrgY()));
        // draw the bottom-most horizontal line
          SetColor(BRIGHTGREEN);
          WAIT_UNTIL_FINISH(Line(GetRealOrgX(),GetRealMaxY()+GetRealOrgY()-1,GetRealMaxX()+GetRealOrgX(),GetRealMaxY()+GetRealOrgY()-1));
          // draw the leftmost vertical line
          SetColor(BRIGHTBLUE);
          WAIT_UNTIL_FINISH(Line(GetRealOrgX(),GetRealOrgY(),GetRealOrgX(),GetRealMaxY()+GetRealOrgY()));

          DelayMs(DEMODELAY);
	}

    LCDCls();
    //scanKBD(); 
//
//	if(configParms.test) {	 
//        testLCD_SD_CARD(SDcardOK);
//	}

	LED0_IO ^= 1;
	LCDCls();
    SetColor(BLACK);
	LCDHome();
    LCDCls();
    
    /*FABRIZIO TEST  SCANSIONE FILE SYSTEM E PLOTTAGGIO A VIDEO  SD_CARD*/
    // Parametro di configurazione caricato da  loadSettings();
	if(configParms.test) {	 
        testLCD_SD_CARD(SDcardOK);
        
        SearchRec srec;
    	LCDPutStringXY(0,0,"DIR>");
        FindFirst("*.bmp",ATTR_MASK,&srec);

        if(configParms.test){ 
            // stampa a video elenco file in sd card
            for(i=0; i<16; i++) {
                LCDPutStringXY(1,i+1,srec.filename);
                if(FindNext(&srec))
                    break;
                }
                DelayMs(2*DEMODELAY);
                LCDCls();
        }
	}
    
    
    /* FABRIZIO SOSTITUITO CON BLOCCO LETTO DA FILE CONFIGURAZIONE 
     * if (!flag_nok_sd)PutBmp8BPPExt(GetRealOrgX(), GetRealOrgY(), (void *) "home.bmp" , IMAGE_NORMAL);
     *  else{LCDPutStringXY(5,5, "HOME SCREEN");}
     */
    
    myFont=font16;
  //  PutBmp8BPPExt(40, GetRealOrgY(), (void *) "0.bmp" , IMAGE_NORMAL);
    if (!flag_nok_sd){
        
          //non metto pi� configParms.lingua_default caricao sempre il menu della configurazione italiana
          //e uso configParms.lingua_default solo per leggere le variabili in lingua
          // lascio la struttura perche cos� si possono definire versioni diverse per lingua tradotte
        sprintf(profilo,"menu%u_%u",0,(BYTE)0);
        loadMenu(MENU_INI,profilo);
        PutBmp8BPPExt(40, GetRealOrgY(), (void *) mnu.backimg , IMAGE_NORMAL);                
        
    }
    
    
         
    SetColor(WHITE);
   
    PutBmp8BPPExt(40, GetRealOrgY(), (void *) "0.bmp" , IMAGE_NORMAL);
    
    sprintf(profilo,"FW %03d.%03d",VERNUMH,VERNUML);
    LCDPutStringCenter(12, profilo);

    rtc_update();  
    refresh_orario();
  
    Timer1_Counter(PR1TICK20MILLI);                  
    TMR1_Start();   
//    Enable_Mec_Button();

    
     if(strncmp(mnu.text,"COLLAUDO",8)==0){
         collaudo=TRUE;
     }


    
    while(1) 
    {//mainloop
       ClrWdt(); // watchdog
      
        check_update_sd();
       
        if(refresh_rtc)
        { 
          refresh_rtc=0;
          if(count_timeout_transfer>0)
          {
              count_timeout_transfer--;
          }
          rtc_update();  
        }
      
        if (IN_SNS_Pres_GetValue() || configParms.pressino==0){
          flag_ok_press = 1;
            LED4_SetHigh();
        }else{
          flag_ok_press = 0;
            LED4_SetLow();
        }  
              
        CommStart();
	}	//main loop
    return (-1);
}



BOOL bisestile(int anno){
/*
 *     Un anno � bisestibile se 1) � divisibile per 4 e non divisibile per 100
 *                              2) � divisibile per 4 per 100 e per 400
 
 */
    
    if(((int)(anno/4))*4==anno){
        // Se � divisibile x 4 verifico se � anche divisibile x 100
        if(((int)(anno/100))*100==anno){
            // se � divisibile per 100 � bisestile solo se � anche divisibile per 400
            if(((int)(anno/400))*400==anno){
                //divisibile per 400
                return TRUE;
            }
            //divisibile x 100 ma non per 400
            return FALSE;
        }
        // bisestile in quanto divisibile per 4 e non divisibile per 100
        return TRUE;
    }else // non divisibile x 4 return FALSE        
        return FALSE;
}


unsigned char check_update_sd(void)                     // routine che controlla la usb per il cambio immagini o fili si SD
{
unsigned char ret=0;                                    // scandisce le fasi della funzione e restitusce il valore

    if(step_transfer==0) 
    {
        deviceAttached = FALSE;                         // Initializzazione variabile USB
        USBInitialize(0);                               // Initializzazion stack usb
        count_timeout_transfer=10;  
        step_transfer=1;
    }
    else if(step_transfer==1)
    {
        USBTasks(); 
        if(USBHostMSDSCSIMediaDetect())                 // mi indica se la USB e' collegata alla scheda
        {
            ret=1; 
            LED7_SetHigh();                             // DEBUG
            deviceAttached = TRUE;                      // Setto la variabile che mi indica USB collegata
            if(FSInit())
            {
                ret=2;
                if(USBFSInit())                         // Controllo se il dispositivo collegato e' nel formato corretto
                {
                    ret=3;
                    count_timeout_transfer=10;
                    myFile_usb=USBFSfopen(BMP_USB_TO_SD,FS_READ);
                    if(myFile_usb) 
                    {
                        ret=4;
                        myFile_sd=FSfopen(BMP_USB_TO_SD,FS_WRITE);
                        if(myFile_sd) 
                        {
                            ret=5;
                            while((character=USBFSgetc(myFile_usb)) != EOF) 
                            {
                                FSputc(character,myFile_sd);
                                LED7_Toggle();          // DEBUG
                            }	
                            FSfclose(myFile_sd);
                        }
                        ret=6;
                        USBFSfclose(myFile_usb);
                        step_transfer=2;
                        LED7_SetLow();                  // DEBUG
                    }
                } 
            }
        }

        if(count_timeout_transfer==0)                   // timeout scaduto - nn faccio piu' niente
        {
            step_transfer=1;
        }
    }
    else
    {
        ret=1;
    } 

    return ret;
}
