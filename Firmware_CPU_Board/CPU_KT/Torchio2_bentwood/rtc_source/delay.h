// FileName:        delay.h
// Dependencies:    none
// Processor:       PIC24FJ128GA010 
// Hardware:        Explorer 16 demo board
// Compiler:        MPLAB� C30 v3.30b
/***********************************************************************
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the �Company�) for its dsPIC30F Microcontroller is intended 
 * and supplied to you, the Company�s customer, for use solely and
 * exclusively on Microchip's dsPIC30F Microcontroller products. 
 * The software is owned by the Company and/or its supplier, and is
 * protected under applicable copyright laws. All rights are reserved.
 * Any use in violation of the foregoing restrictions may subject the
 * user to criminal sanctions under applicable laws, as well as to
 * civil liability for the breach of the terms and conditions of this
 * license.
 *
 * THIS SOFTWARE IS PROVIDED IN AN �AS IS� CONDITION. NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
 * TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
 * IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
 * CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Eugen Ionescu        10/12/2011  Initial release for LCD & push-buttons suport
 *
 *******************************************************************************/
void delayus(unsigned int del_us);   // delay expressed in microseconds 
void delayms(unsigned int del_ms);   // delay expressed in miliseconds
void delay200us(void)            ;   // 200 usec delay 
void delay1_5ms(void)            ;   // 1.5 msec delay  
void delay5ms(void)              ;   // 5.0 msec delay 
void delay15ms(void)             ;   // 15  msec delay 
void delay1s(void)               ;   // 1 second delay  


