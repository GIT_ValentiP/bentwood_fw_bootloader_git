/******************************************************************************
 * FileName:        MainDemo.c
 * Dependencies:    see included files below
 * Processor:       PIC24F, PIC24H, dsPIC, PIC32
 * Compiler:        C30 v3.25/C32 v0.00.18
 * Company:         Microchip Technology, Inc.
 * Software License Agreement
 *
 * Copyright (c) 2011 Microchip Technology Inc.  All rights reserved.
 * Microchip licenses to you the right to use, modify, copy and distribute
 * Software only when embedded on a Microchip microcontroller or digital
 * signal controller, which is integrated into your product or third party
 * product (pursuant to the sublicense terms in the accompanying license
 * agreement).  
 *
 * You should refer to the license agreement accompanying this Software
 * for additional information regarding your rights and obligations.
 *
 *******************************************************************************/
/************************************************************
 * Includes
 ************************************************************/
#include <ctype.h>
#include <stdlib.h>
#include "MainDemo.h"

#include "Graphics/SSD1963.h"
// http://www.techtoys.com.hk/Displays/SSD1963EvalRev3B/SSD1963%20Eval%20Board%20Rev3B.htm


#include "ppsnew.h"

#include "Graphics\gfxpmp.h"


#include "sd-spi.h"
#include "swi2c.h"

#ifdef USA_ETHERNET
#include "tcpipConfig.h"
#include "tcpip stack/stackTsk.h"
#include "tcpip stack/helpers.h"
#endif

#include "usb_config.h"
#include "usb.h"
#include "usb_common.h"
#include "usb_host_msd.h"
#include "usb_host_msd_scsi.h"
#include "FSIO.h"
#include "USBFSIO.h"
#include "EEFSIO.h"

#include "boot_config.h"


/************************************************************
 * Configuration Bits
 ************************************************************/
_FGS( GWRP_OFF & GSS_OFF & GSSK_OFF ) 
_FOSCSEL(FNOSC_PRIPLL & IESO_OFF)			//FNOSC_FRCPLL 
_FOSC( POSCMD_HS & OSCIOFNC_ON & IOL1WAY_ON & FCKSM_CSDCMD )
_FWDT( WDTPOST_PS32768 & WDTPRE_PR32 & PLLKEN_ON & WINDIS_OFF & FWDTEN_OFF )
_FPOR( FPWRT_PWR8 & BOREN_ON & ALTI2C1_OFF & ALTI2C2_OFF )
_FICD( ICS_PGD3 & RSTPRI_PF & JTAGEN_OFF )
_FAS( AWRP_OFF & APL_OFF & APLK_OFF)
//_FUID0(x) simpa..



#ifdef USA_BOOTLOADER 
//#define BL_ENTRY_BUTTON PORTEbits.RE0 //button 1...

//If defined, the reset vector pointer and boot mode entry delay
// value will be stored in the device's vector space at addresses 0x100 and 0x102
#define USE_VECTOR_SPACE
//****************************************

//Bootloader Vectors *********************
#ifdef USE_VECTOR_SPACE
	/*
		Store delay timeout value & user reset vector at 0x100 
		(can't be used with bootloader's vector protect mode).
		
		Value of userReset should match reset vector as defined in linker script.
		BLreset space must be defined in linker script.
	*/
	unsigned char timeout  __attribute__ ((space(prog),section(".BLreset"))) = 0x0A;
	unsigned int userReset  __attribute__ ((space(prog),section(".BLreset"))) = 0xC00; 
//cambiato ordine o le metteva a caso... 
#else
	/*
		Store delay timeout value & user reset vector at start of user space
	
		Value of userReset should be the start of actual program code since 
		these variables will be stored in the same area.
	*/
	unsigned int userReset  __attribute__ ((space(prog),section(".init"))) = 0xC04 ;
	unsigned char timeout  __attribute__ ((space(prog),section(".init"))) = 5 ;
#endif
#endif



const char CopyrString[]= {'O','A',' ','(','X','X','X','X','X','X','X','X','X',' ','(','X','X','X','X',')',')',' ','-',' ','K','-','T','r','o','n','i','c',' ','-',' ','B','X','X','X','w','X','X','X',' '
	,
#ifdef USA_BOOTLOADER 
	' ','B','L',
#endif
#ifdef USA_ETHERNET
	' ','E','T','H',
#endif
	' ','v',VERNUMH+'0','.',VERNUML/10+'0',(VERNUML % 10)+'0',' ',
	'1','4','/','0','3','/','1','9', 0 };
// fuck microchip, die cindy & george pauley & nickderosa & dariokubler

/************************************************************
 * Defines
 ************************************************************/
#define	PutBmp8BPPExt(a,b,c,d) 	PutBmp8BPPExtFast(a,b,c,d)

/************************************************************
 * Enumeration Structure
 ************************************************************/
typedef enum {
  DEMO_FILE_TYPE_JPEG,
  DEMO_FILE_TYPE_RGB,
  DEMO_FILE_TYPE_OTHER
	} DEMO_FILE_TYPE;

/////////////////////////////////////////////////////////////////////////////
//                            FONTS
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//                            PICTURES
/////////////////////////////////////////////////////////////////////////////


/************************************************************
 * Externs: Function Prototypes
 ************************************************************/

#define WAIT_UNTIL_FINISH(x)    while(!x)
#define DEMODELAY				1000

/************************************************************
 * Function Prototypes
 ************************************************************/

int MostraSiNo(const char *);
int showAParm(BYTE ,char *);
int handleBIOS(BYTE);
int showBIOS(void);
int handleTastiera(void);
void refreshScreen(void);

int FSReadLine(FSFILE *,char *);
int EEFSReadLine(FSFILE *,char *);
int getParmN(const char *,const char *,int *);
int getParmS(const char *,const char *,char *);
int FSwriteParmN(FSFILE *,const char *,int );
int FSwriteParmS(FSFILE *,const char *,const char *);
int EEFSwriteParmN(FSFILE *,const char *,int );
int EEFSwriteParmS(FSFILE *,const char *,const char *);
BYTE GetProfileInt(const char *gruppo,const char *chiave,int *value);
BYTE GetProfileString(const char *gruppo,const char *chiave,char *value);
BYTE WriteProfileInt(const char *gruppo,const char *chiave,int value);
BYTE WriteProfileString(const char *gruppo,const char *chiave,char *value);
BYTE cercaGruppo(FSFILE *,const char *);
signed char stricmp(const char *s,const char *d);
signed char strnicmp(const char *s,const char *d,int n);
void exportSettingsSD(void);
int exportSettingsUSB(void);
int importSettingsUSB(void);
DWORD copyFileUSBToSD(const char *,const char *);

void scanKBD(void);
BYTE checkKey(BYTE);
void checkKeys_doError(void);
#define KBclear() {oldkbKeys[0]=kbKeys[0]=0;}

/************************************************************
 * Variables
 ************************************************************/
WORD curPosX=0,curPosY=0;
BYTE LCDX=0,LCDY=0,savedX=0,savedY=0;
BYTE kbKeys[MAX_TASTI_CONTEMPORANEI],oldkbKeys[MAX_TASTI_CONTEMPORANEI];
BYTE rowState[32];		//5x5 matrice; ora 10 tasti meccanici + 12 tasti touch letti da SPI
WORD FLAGS=0;		//b0=TRIGK, b7=inCtrl, b6=inShift, b5=cursorState, b8=inSetup
BYTE /*inSetup=FALSE,*/SDcardOK=FALSE;
BYTE menuLevel=0,currSchermata=SCHERMATA_SPLASH,currSchermItem=0,enterData=0,extendedMenu=0;
BYTE deviceAttached;
struct CONFIG_PARMS configParms;

char kbTempBuffer[4];


//Flags
#define TRIGK 1

const char *string_disable="DISABLE",*string_enable="ENABLE";
const char *MENU_INI="menu.ini",*SETTINGS_INI="settings.ini",*FW_UPDATE=BOOT_FILE_NAME,*MENU_UPDATE="NEWMENUS";
const BYTE *myFont=NULL;
BYTE backLight=0;

void InitializeTimer(void);


WORD textColors[16]={BLACK,BRIGHTRED,BRIGHTGREEN,BRIGHTYELLOW,BRIGHTBLUE,BRIGHTMAGENTA,BRIGHTCYAN,WHITE,
	DARKGRAY,RED,GREEN,YELLOW,BLUE,MAGENTA,CYAN,LIGHTGRAY};
enum COLORI_TESTO { _TESTO_NERO,_TESTO_ROSSO,_TESTO_VERDE,_TESTO_GIALLO,_TESTO_BLU,_TESTO_MAGENTA,_TESTO_CIANO,_TESTO_BIANCO,
	_TESTOGRIGIO,_TESTO_ROSSOSCURO,_TESTO_VERDESCURO,_TESTO_GIALLOSCURO,_TESTO_BLUSCURO,_TESTO_MAGENTASCURO,_TESTO_CIANOSCURO,_TESTO_GRIGIOSCURO};
WORD fontColor,bkColor,defaultFontColor=WHITE,defaultBackColor=BLACK;			// sotto i default vengono riassegnati
BYTE _backcolor;		// ora cos�

#define PWM_SPEED 50

BYTE State=0;			// enum 
BYTE currMenu=0;
BYTE statoPWM[2];
WORD tempoOperazione=0;
BYTE tipoOperazione=0;

void ModBusSetRele(int);
signed char ModBusSetMotore(unsigned int);
int ModBusReadMotoreState(void);
int ModBusReadMotoreTemperature(void);

long BilanciaLeggiPeso(void);
long BilanciaTara(void);

void ModBusSetRele(int);
signed char ModBusSetMotore(unsigned int);
int ModBusReadMotoreState(void);
int ModBusReadMotoreTemperature(void);

long BilanciaLeggiPeso(void);
long BilanciaTara(void);
void serialPause(void);



#ifdef USA_ETHERNET
APP_CONFIG AppConfig;
static unsigned short wOriginalAppConfigChecksum;	// Checksum of the ROM defaults for AppConfig
static void InitAppConfig(void);
static void InitializeBoard(void);
void SaveAppConfig(const APP_CONFIG *);
#endif

/************************************************************
 * int main (void)
 ************************************************************/
int main(void) {
	char ByteRec;
	char buffer[64];
	BYTE cursorCnt=0,blinkCnt=0;
	int i;
	long n2;
	signed char imgCnt=0;
	static signed char oldMenu=-1,currMenu=0,currLang=0 /* default, italiano ;) */;
#ifdef USA_ETHERNET
	static DWORD dwLastIP = 0;
#endif


//	scanKBD(); 




	ANSELA=0x0000;
	ANSELB=0x0000;
	ANSELC=0x0000;
	ANSELD=0x0000;
	ANSELE=0x0000;
	ANSELG=0x0000;



//DriverInterfaceInit();	// per DEBUGGARE/SIMULARE tempi LCD...
//	__delay_ms(100);
//return;

//	_PLLDIV=278 /*205*/;						// M = _PLLDIV+2, 2..513
//	_PLLPRE=6;						// N1 = _PLLPRE+2, 2..33
//	_PLLPOST=0;						// N2 = _PLLPOST, 2 (0), 4 (1), 8 (3)
	OSCTUN=0;			//(da USB)
// [Lavoro a 100MHz per ora (v .H anche, v. anche il blocco per dsPIC sopra) ]
// [(215*7.37)/(2*8*2) = 100~]
// (280*8)/(8*2) = 140

			// forse meglio cos�?
  PLLFBD = 68; // M = PLLFBD + 2 = 70
  CLKDIVbits.PLLPRE = 0; // N1 = 2
  CLKDIVbits.PLLPOST = 0; // N2 = 2

/*	
  Fosc= Fin*M/(N1*N2)
	0.8MHz<(Fin/N1)<8MHz
	100MHz<(Fin*M/N1)<340MHz
	M=2,3,4,...513
	N1=2...33
	N2=2,4,8
	
	PLLFBD    = M -2;
	CLKDIVbits.PLLPRE = N1 -2;
	CLKDIVbits.PLLPOST = 0b00; //N2={2,4,R,8} */

//OSCCONbits.CLKLOCK=1;OSCCONbits.NOSC=1;OSCCONbits.OSWEN=1;
//  while (OSCCONbits.COSC != 0x7)LATB ^= 1;; 
//	while(OSCCONbits.LOCK!=1)
//		ClrWdt();


	//Wait for the Primary PLL to lock and then
       // configure the auxilliary PLL to provide 48MHz needed for USB
       // Operation.
  while(OSCCONbits.LOCK != 1) ClrWdt();			// boh?

// per USB
            // Configuring the auxiliary PLL, since the primary
            // oscillator provides the source clock to the auxiliary
            // PLL, the auxiliary oscillator is disabled. Note that
            // the AUX PLL is enabled. The input 8MHz clock is divided
            // by 2, multiplied by 24 and then divided by 2. Wait till
            // the AUX PLL locks.

            ACLKCON3 = 0x24c1 /*0x24C1*/;		// 8MHz input, diviso 2 e diviso 2
            ACLKDIV3 = 0x7;									// x24, ossia in tutto 48MHz!

#ifndef __DEBUG
            ACLKCON3bits.ENAPLL = 1;
            while(ACLKCON3bits.APLLCK != 1) ClrWdt();
#endif



#warning AGGIUNTE TRIS GLOBALI, VERIFICARE 10.17
	TRISA = 0b0000000000000000;			// 
	TRISB = 0b0000000000000000;			// 
	TRISC = 0b0000000000011100;			// 
	TRISD = 0b0000000000000000;			// 
	TRISE = 0b0000000000000000;			// 
	TRISF = 0b0000000000000000;			// 
	TRISG = 0b0100000001000000;			//  22.9.18 ; RG14 DEVE essere 1??! cos'�?



	LED0_TRIS=0; 	LED1_TRIS=0; LED2_TRIS=BUTTON2_TRIS=1 /*dip per reset*/; // 
	kLED0_TRIS=0; 	kLED1_TRIS=0; kLED2_TRIS=0; kLED3_TRIS=0; kLED4_TRIS=0; 	kLED5_TRIS=0; kLED6_TRIS=0; kLED7_TRIS=0;
//	BUTTON0_TRIS=1; BUTTON1_TRIS=1; BUTTON2_TRIS=1; per debug no..
	TRISGbits.TRISG15=0;		//buzzer
	TRISDbits.TRISD15=0;		//SPI
	TRISBbits.TRISB14=0;		//SPI
	TRISBbits.TRISB15=1;		//SPI
	TRISDbits.TRISD14=0;		//SPI SD
	TRISFbits.TRISF12=0;		//ENC
	TRISDbits.TRISD1=0;		//retro pwm
	TRISBbits.TRISB7=0;		//TX-EN
	TRISBbits.TRISB8=0;		//RX-EN
	LATBbits.LATB7=1;
	LATBbits.LATB8=0;
	LATFbits.LATF12=1;		// CS ENC disattivo
	LATDbits.LATD14=1;		// CS SD disattivo

#ifdef USA_ETHERNET
// non li usiamo
	TRISBbits.TRISB12=1;		//INT; v. anche ENC100_INT_TRIS
	TRISBbits.TRISB13=1;		//PSPCFG0
#endif



	LED0_IO = 0;
	LED1_IO = 1;
	// LED2_IO = 0;		qua lo usiamo come input per reset eeprom

//	while(1) {	/*LED1_IO ^= 1;*/ ClrWdt(); 		// test clock 210
//		__builtin_btg((unsigned int *)&LATC,13); }		// 15/11/14: dovrebbe andare a 8MHz (sono 8 cicli v. asm ossia 4 + 4); ok

	loadSettings();

	OpenTimer2(T2_ON & T2_IDLE_CON & T2_GATE_OFF & T2_32BIT_MODE_OFF & T2_PS_1_64 & T2_SOURCE_INT,
		TMR2BASE);		//30Hz per timing			//(il timer va a 16uSec su GB, 4 su GU)

	ConfigIntTimer2(T2_INT_PRIOR_3 & T2_INT_ON);
//	EnableIntT2;


//#ifndef USA_BOOTLOADER mah e invece s� s� s�

// Unlock Registers
	PPSUnLock;


// non le trova... manco in XC... froci urfidi...@$%&
// http://www.microchip.com/forums/m801504.aspx  MBedder !
#define PPSIn(fn,pin)    iPPSInput(IN_FN_PPS##fn,IN_PIN_PPS##pin)
#define PPSOut(fn,pin)    iPPSOutput(OUT_PIN_PPS##pin,OUT_FN_PPS##fn)

#ifdef USA_232_ANZICHE_485		// se si vogliono usare entrambe, cambiare..
	PPSOut(_U1TX, _RP100);      // TXD su Explorer16 e RS232 (1) su KTronic
	PPSIn(_U1RX, _RPI38);      // RXD su Explorer16
//	  PPSInput(PPS_U1CTS,PPS_RP15);      // CTS 
//	  PPSOutput(PPS_RP30,PPS_U1RTS);      // RTS 
#else
  PPSOut(_U1TX, _RP101);      // TXD su 485
  PPSIn(_U1RX, _RPI41);      // RXD su 485
//	  PPSInput(PPS_U2CTS,PPS_RP7);      // TX-EN ovviamente no PPS!
//	  PPSOutput(PPS_RP8,PPS_U2RTS);      // RX-EN
#endif


  PPSIn(_SDI1, _RPI47);      // SDI 
	// Outputs
  PPSOut(_SCK1, _RP79);      // 
  PPSOut(_SDO1, _RP104);      // 

#ifdef USA_BUZZER_PWM 		// se no � on-off
	PPSOut(_OC2, _RP16);      // buzzer 4KHz , qua rimappabile volendo
#endif

#ifdef DEBUG_TESTREFCLK
// test REFCLK
	PPSOut(_REFCLKO, _RP99);      // RefClk su pin 1 (RG15, buzzer)
	REFOCONbits.ROSSLP=1;
	REFOCONbits.ROSEL=1;
	REFOCONbits.RODIV=0;
	REFOCONbits.ROON=1;
	TRISFbits.TRISF3=1;
#endif

	// lock Registers
	PPSLock;


//buzzer
#ifdef USA_BUZZER_PWM 		// se no � on-off
#if defined(__PIC24FJ256GB210__) || defined(__PIC24FJ256GB110__)
  /* Reset PWM */
  OC2CON1 = 0x0000;
  OC2CON2 = 0x0000;
  
  /* set PWM duty cycle to 50% */
  OC2R    = 2048 /* basato su SysClock => 64KHz (16MHz / 256) */; //PWM_PERIOD >> 1; /* set the duty cycle tp 50% */
  OC2RS   = 4096 /* se uso Timer come Src SEMBRA NON FARE NULLA... qua boh! */;  //PWM_PERIOD - 1;  /* set the period */
  
  /* configure PWM */
  OC2CON2 = 0x001f;   /* 0x001F = Sync with This OC module                               */
  OC2CON1 = 0x1c00 /* 0x0400 => src=Timer3 */;  /* 0x1C08 = Clock source Fcyc, trigger mode 1, Mode 0 (disable OC1) */
  
  /* enable the PWM */
//      OC2CON1 |= 0x0006;   /* Mode 6, Edge-aligned PWM Mode */ SPENTO!
#endif
#endif



	curPosX=GetRealOrgX(); curPosY=GetRealOrgY();

#ifndef __DEBUG
  InitGraph();                // Graphics
#endif
	ClrWdt();

#ifndef __DEBUG
	if(MDD_MediaDetect()) 		// mmm altrove non serviva, qui se no si pianta se manca SD... colpa della micro? boh
  	SDcardOK=FSInit();          // File system
// in Modelleria avevamo dovuto rimettere MACInit() anche dopo una FSInit...
#endif
//	LATGbits.LATG14=1;		//enable display... boh
	//LATGbits.LATG12=1;		//backlight, boh
	if(configParms.backLight) {
		DisplayBacklightOn();		//serve per attivare display!
		backLight=1;
		}

   #define DEFAULT_YEARS               0x0018
   #define DEFAULT_MONTH_DAY           0x0922
   #define DEFAULT_WEEKDAY_HOURS       0x0521
   #define DEFAULT_MINUTES_SECONDS     0x3900

   #define INDEX_HOURS                 2
   #define INDEX_MINUTES               1
   #define INDEX_SECONDS               0
   #define INDEX_YEAR                  2
   #define INDEX_MONTH                 1
   #define INDEX_DAY                   0

   OSCCON = 0x3300;    // Enable secondary oscillator TOLTO (x prova) lo 0x0002; uso PWM per dargli input!
   CLKDIV = 0x0000;    // Set PLL prescaler (1:1)
       // Initialize the RTCC
       // Turn on the secondary oscillator
	__asm__ ("MOV #OSCCON,w1");
	__asm__ ("MOV.b #0x02, w0");
	__asm__ ("MOV #0x46, w2");
	__asm__ ("MOV #0x57, w3");
	__asm__ ("MOV.b w2, [w1]");
	__asm__ ("MOV.b w3, [w1]");
	__asm__ ("MOV.b w0, [w1]");
	
	PIC24RTCCSetDate( DEFAULT_YEARS, DEFAULT_MONTH_DAY );
	PIC24RTCCSetTime( DEFAULT_WEEKDAY_HOURS, DEFAULT_MINUTES_SECONDS );
//	__builtin_write_OSCCONL(OSCCON | 0x0002); 		//DAL FORUM... MA CREDO CI SIA GIA'...
	RCFGCAL = 0x8000;




#ifndef __DEBUG
	scanKBD(); 

/*	if(!BUTTON2_IO) {
		resetSettings();
		saveSettings();
//		LCDPutStringXY(0,0,"Ripristino default!");			// SPOSTATO SOTTO perche' qua non ci sono i font
//	  DelayMs(2*DEMODELAY);
ClrWdt();
		}*/

 	SetColor(BLUE);
	ClearDevice();


	if(configParms.splash) {		// nostra estensione
		ClearDevice();
	// draw border lines to show the limits of the 
	// left, right, top and bottom pixels of the screen
	// draw the topmost horizontal line
	  SetColor(BRIGHTRED);
	//  SetColor(BRIGHTYELLOW); hmmm , manca il rosso...
	  WAIT_UNTIL_FINISH(Line(GetRealOrgX(),GetRealOrgY(),GetRealMaxX()+GetRealOrgX(),GetRealOrgY()));
	  // draw the rightmost vertical line
	  SetColor(BRIGHTYELLOW);
	  WAIT_UNTIL_FINISH(Line(GetRealMaxX()+GetRealOrgX()-1,GetRealOrgY(),GetRealMaxX()+GetRealOrgX()-1,GetRealMaxY()+GetRealOrgY()));
	// draw the bottom-most horizontal line
	  SetColor(BRIGHTGREEN);
	  WAIT_UNTIL_FINISH(Line(GetRealOrgX(),GetRealMaxY()+GetRealOrgY()-1,GetRealMaxX()+GetRealOrgX(),GetRealMaxY()+GetRealOrgY()-1));
	  // draw the leftmost vertical line
	  SetColor(BRIGHTBLUE);
	  WAIT_UNTIL_FINISH(Line(GetRealOrgX(),GetRealOrgY(),GetRealOrgX(),GetRealMaxY()+GetRealOrgY()));
	
//	  WAIT_UNTIL_FINISH(PutImage(48+GetRealOrgX(), 7+GetRealOrgY(), (void *) &logoKtronic , IMAGE_X2));
//		PutImage8BPPExt(GetRealOrgX(), GetRealOrgY(), (void *) "logokt2.bmp" , IMAGE_NORMAL);
		PutBmp8BPPExt(GetRealOrgX(), GetRealOrgY(), (void *) "logokt2.bmp" , IMAGE_NORMAL);
//		PutBmp8BPPExtFast(GetRealOrgX(), GetRealOrgY(), (void *) "logokt2.bmp" , IMAGE_NORMAL);
//		PutImageRLE8BPPExt(GetRealOrgX(), GetRealOrgY(), (void *) "logokt2r.bmp" , IMAGE_NORMAL);

//		PutJPEGFile(GetRealOrgX(), GetRealOrgY(), "logokt.jpg", NULL, NULL,	24,0,0);

	  DelayMs(1*DEMODELAY);
		}

	LCDCls();
	scanKBD(); 

// test caratteri
	myFont=&font16;
  SetFont((void *)&font16);
	SetFontOrientation(0);		//horiz
  SetColor(WHITE);
	LED0_IO ^= 1;

	if(configParms.test) {	// mm forse questo agisce solo sulla stampa dell'alfabeto US ASCII (pag. 24 spec)
		LCDPutStringCenter(5,"..SELF TEST..");
		if(SDcardOK) {
			FSFILE *logFile=NULL;

//			SetClockVars(((WORD)(currentDate.year))+2000, currentDate.mon, currentDate.mday, currentTime.hour, currentTime.min, currentTime.sec);
// qua usiamo RTC
		  logFile = FSfopen("bw.txt",FS_APPEND);
		  if(logFile) {
				sprintf((char*)&buffer[0],"%u,%u\r\n",VERNUMH,VERNUML);
				FSfwrite((const void*)&buffer[0],1,strlen(buffer),logFile);
				FSfclose(logFile);
				logFile = NULL;
				}
			}

		{
		char ch;
		LCDHome();
		for(ch=' '; ch<127; ch++)
			LCDPutChar(ch);
		}

		sprintf(buffer,"VIEW ANGLE SETTING=%u",configParms.viewingAngle);
		LCDPutStringCenter(6,buffer);
	  DelayMs(1*DEMODELAY);
		LED0_IO ^= 1;
	

		LCDCls();
		scanKBD(); 
	
	  SetColor(BRIGHTGREEN);
		LCDPutStringCenter(2,"..SELF TEST..");
		LCDPutStringXY(0,4,"DISPLAY.......PASSED");
		LCDPutStringXY(0,5,"MEMORY TEST...PASSED");
		if(SDcardOK) 
			LCDPutStringXY(0,6,"EXT.MEM.TEST..PASSED");
		else
			LCDPutStringXY(0,6,"EXT.MEM.TEST..FAILED");
#ifdef USA_ETHERNET
// if (MacInit()) // in effetti se non va, si ferma in loop l�!
			LCDPutStringXY(0,7,"ETHERNET TEST..PASSED");
#endif

  // Initialize USB layers
    deviceAttached = FALSE;
    //Initialize the stack
    ByteRec=USBInitialize(0);
		LCDPutStringXY(0,7,ByteRec ? "USBHOST TEST..PASSED" : "USBHOST TEST..FAILED");


		i=EEFSInit();
//		EEFSformat(1,0x4447,"bw");

		LCDPutStringXY(0,9,"ID=");
		strncpy(buffer,CopyrString,28);
		buffer[28]=0;
		LCDPutStringXY(4,9,buffer);
		strncpy(buffer,CopyrString+28,28);
		buffer[28]=0;
		LCDPutStringXY(4,10,buffer);
		strncpy(buffer,CopyrString+56,28);
		buffer[28]=0;
		LCDPutStringXY(4,11,buffer);

#ifdef USA_BUZZER_PWM 		
	OC2CON1 |= 0x0006;   // on
	__delay_ms(100);
	ClrWdt();
	OC2CON1 &= ~0x0006;   // off
	__delay_ms(100);
	ClrWdt();
	OC2CON1 |= 0x0006;   // on
	__delay_ms(100);
	ClrWdt();
	OC2CON1 &= ~0x0006;   // off
	__delay_ms(100);
	ClrWdt();
	OC2CON1 |= 0x0006;   // on
	__delay_ms(100);
	ClrWdt();
	OC2CON1 &= ~0x0006;   // off
//	__delay_ms(100);
	ClrWdt();
#else
	LATGbits.LATG15=1;   // on
	__delay_ms(100);
	ClrWdt();
	LATGbits.LATG15=0;   // off
	__delay_ms(100);
	ClrWdt();
	LATGbits.LATG15=1;   // on
	__delay_ms(100);
	ClrWdt();
	LATGbits.LATG15=0;   // off
	__delay_ms(100);
	ClrWdt();
	LATGbits.LATG15=1;   // on
	__delay_ms(100);
	ClrWdt();
	LATGbits.LATG15=0;   // off
//	__delay_ms(100);
	ClrWdt();
#endif
  DelayMs(1*DEMODELAY);
	}

	LED0_IO ^= 1;

	LCDCls();
  SetColor(WHITE);
	LCDHome();
#endif

#ifdef USA_ETHERNET
  TickInit();
//  MPFSInit();
	InitAppConfig();
	// Initialize core stack layers (MAC, ARP, TCP, UDP) and application modules (HTTP, SNMP, etc.)
  StackInit();
	#if defined(STACK_USE_ICMP_SERVER)
//		if(configParms.EnablePing)		// questo � complicato da gestire... v. stacktsk
//			ICMPInit();	
	#endif
	#if defined(STACK_USE_TELNET_SERVER)
//	if(configParms.EnableTelnet)	non c'�!
//		TelnetInit();	
	#endif
	#if defined(STACK_USE_HTTP2_SERVER)
	if(configParms.EnableWWW)
		HTTPInit();
	#endif
	#if defined(STACK_USE_UART2TCP_BRIDGE) || defined(STACK_USE_UART2TCP_MIRROR)
	if(configParms.EnableUartBridge)
		UART2TCPBridgeInit();
	#endif

	ClrWdt();

	LCDPutStringXY(2,1,".ETHERNET.");
#endif




	LCDCls();
/*	SearchRec srec;
	int i;
	LCDPutStringXY(0,0,"DIR>");
	FindFirst("*.bmp",ATTR_MASK,&srec);
	for(i=0; i<16; i++) {
		LCDPutStringXY(1,i+1,srec.filename);
	  if(FindNext(&srec))
			break;
		}
  DelayMs(2*DEMODELAY);
*/
	LCDCls();
#ifdef DEBUG_MODE			// x usb
#endif





	if(!TASTO4) {			// v. sopra
		LCDPutStringXY(0,0,"Ripristino default!");
		for(i=0; i<8; i++) {
			kLED0_IO ^= 1;
			kLED1_IO ^= 1;
			__delay_ms(100);
			ClrWdt();
			}
		resetSettings();
		saveSettings();

		}

		{
FSFILE *f;
	f=EEFSfopen(SETTINGS_INI,FS_READ);
	if(f) {
		kLED7_IO ^= 1;
		EEFSfclose(f);
		}

		SearchRec srec;
		LCDPutChar('\r');
		LCDPutChar('\n');
		if(EEFindFirst("*.*",/*ATTR_NORMAL*/ ATTR_MASK,&srec)>=0)
			do {
				ClrWdt();
				LCDPutString(srec.filename);
				LCDPutChar('\r');
				LCDPutChar('\n');
			  } while(!EEFindNext(&srec));


      USBTasks();
  if(USBHostMSDSCSIMediaDetect()) {
		kLED5_IO ^= 1;
		if(USBFSInit()) {

	f=USBFSfopen(SETTINGS_INI,FS_WRITE);
	if(f) {
		kLED6_IO ^= 1;
		USBFSfclose(f);
		}
/*		if(USBFindFirst("*.*",ATTR_MASK,&srec)>=0) 
			do {
				ClrWdt();
				LCDPutString(srec.filename);
				LCDPutChar('\r');
				LCDPutChar('\n');
			  } while(!USBFindNext(&srec));*/
		}
		}

		exportSettingsSD();

		}
		



//#define BOOT_OPTIONS 1
#ifdef BOOT_OPTIONS
	scanKBD();
	if(kbKeys[0]==1 /*'A'*/) {			// aggiuntine...
		goto aspettaTasto;
		}
	if(kbKeys[0]==6 /*'B'*/) {			// aggiuntine...
		if(handleBIOS(0)) {
			}
		goto aspettaTasto;
		}
	if(kbKeys[0]==16 /*'D'*/) {			// 
		resetSettings();
		saveSettings();
		LCDPutString("EEprom ripristinata!\r\n");
		goto aspettaTasto;
		}
	if(kbKeys[0]==11 /*'C'*/) {			// aggiuntine...
		SearchRec srec;
		int i=0;
		LCDCls();
	  SetColor(BLUE);
		LCDPutString("DIR>\r\n");
	  SetColor(WHITE);
		FindFirst("*.*",ATTR_MASK,&srec);
		do {
			ClrWdt();
			LCDPutString(srec.filename);
			LCDPutChar('\r');
			LCDPutChar('\n');
i++;
			if(i>24)
				break;
		  } while(!FindNext(&srec));

aspettaTasto:
		do {
			scanKBD();
			} while(kbKeys[0]);
	  SetColor(RED);
		LCDPutString("Premere un tasto...");
		do {
			scanKBD();
			} while(!kbKeys[0]);

		LCDCls();
		}
#endif





	State=STATE_IDLE;

  DelayMs(2*DEMODELAY);
//	PutBmp8BPPExt(0, 0, (void *) "fondo001.bmp", IMAGE_NORMAL);

  while(1) {
		ClrWdt();


      USBTasks();

      //if thumbdrive is plugged in
      if(USBHostMSDSCSIMediaDetect()) {
				kLED5_IO = 1;
        if(USBFSInit()) {
					SearchRec srec; FSFILE *f;

					if(!deviceAttached) {
						if(exportSettingsUSB())
							kLED6_IO ^= 1;

						f=USBFSfopen(FW_UPDATE,FS_READ);
						if(f) {
							LCDCls();
							if(MostraSiNo("Trovato firmware aggiornato: installarlo?")) {
								LCDPutString("\r\nAggiornamento in corso...");
								USBFSrename("-entw_fw.hex",f);
								}
							USBFSfclose(f);
							}

						if(USBFindFirst("*.*",ATTR_MASK,&srec)>=0) 
							do {
								ClrWdt();
								LCDPutString(srec.filename);
								LCDPutChar('\r');
								LCDPutChar('\n');
							  } while(!USBFindNext(&srec));

							if(USBFindFirst(MENU_UPDATE,ATTR_DIRECTORY,&srec)>=0) {
								LCDCls();
								if(MostraSiNo("Trovato aggiornamento grafiche e menu: installarli?")) {
									USBFSchdir(MENU_UPDATE);
									LCDPutString("\r\Copia in corso...");
									if(USBFindFirst("*.*",ATTR_NORMAL | ATTR_READ_ONLY | ATTR_ARCHIVE,&srec)>=0) {
										do {
											ClrWdt();
											LCDPutString(srec.filename);
//											LCDPutString("."
											copyFileUSBToSD(srec.filename,srec.filename);
										  } while(!USBFindNext(&srec));
										}
									USBFSchdir("/");
									currMenu=0; currLang=0;
									}
								}
							}
						}
	
	        deviceAttached = TRUE;

        //now a device is attached
        //See if the device is attached and in the right format
				}




		if(second_30) {
			scanKBD(); 
			}
		if(second_10) {
//			scanKBD(); 
#ifndef USA_ETHERNET		// bah tanto per!
			LED0_IO ^= 1;
#endif
//					LCDPutChar('a');


			second_10=0;
			blinkCnt++;

			if(configParms.backLight==2) {		// gestire timed ? secondi
				if(backLight) {
					DisplayBacklightOff();		
					backLight=0;
					}
				}

			if(!TASTO1) {
				imgCnt++;
				imgCnt %= 10;
				goto agg_img;
				}
			if(!TASTO2) {
				imgCnt--;
				if(imgCnt<0)
					imgCnt=9;
agg_img:
				{
				sprintf(buffer,"%u.bmp",imgCnt);
				SetColor(BLACK);
				LCDCls();
				if(imgCnt<5)
					PutBmp8BPPExt(40, 0, (void *)buffer, IMAGE_NORMAL);
				else
					PutBmpRLE8BPPExt(40, 0, (void *)buffer, IMAGE_NORMAL);
				}

				}


			if((tick10 & 1) == 1) {			// 0.2 sec... bah

				if(oldMenu != currMenu) {
					
					sprintf(buffer,"image%u.bmp",currMenu);
					PutBmp8BPPExt(40, 0, (void *) buffer, IMAGE_NORMAL);
					oldMenu = currMenu;
					}


				switch(currMenu) {
					case 0:				// caso/i particolari
					  currentTime.l = PIC24RTCCGetTime();
					  currentDate.l = PIC24RTCCGetDate();
						sprintf(buffer,"%02x/%02x/20%02x",		// BCD!
					    currentDate.mday,currentDate.mon,currentDate.year);
						LCDPutStringXY(3,4,buffer);
						sprintf(buffer,"%02x:%02x:%02x",		// BCD!
					    currentTime.hour,currentTime.min,currentTime.sec);
						LCDPutStringXY(4,5,buffer);
						break;
	
					default:			// leggo da menu.ini che fare con questo tasto in questo menu
{
/* char buf2[32],buf3[32];
				sprintf(buffer,"menu%u_%u",currMenu,currLang);
// vedere quanto � lento farlo ad ogni giro!
		kLED1_IO = 1;
				i=0;
				do {
					sprintf(buf2,"show%u",i);
					*buf3=0;
					GetProfileString(buffer,buf2,buf3);
					if(!strnicmp(buf3,"orologio",8)) {
						currMenu=atoi(buf3+4);
						}	
					else if(!strnicmp(buf3,"contasecondi",12)) {
						i=atoi(buf3+1);
						}	
					else if(!strnicmp(buf3,"tmacina",7)) {
						}	
					else if(!strnicmp(buf3,"tvibro1",7)) {
						}	
					}	while(!*buf3 || i<10);
		kLED1_IO = 0;
*/
}
						break;
	
					case 9:				// caso particolare macinatura
				switch(State & 15) {
					case STATE_IDLE:				// idle
						break;
					case STATE_STARTING:			// start
						LCDCls();
						//if() 
	//					SetPWM(0,configParms.vVibro[0]);
	//					SetPWM(1,configParms.vVibro[1]);
						if(configParms.needPortafiltro) {
							if(m_MicroFiltro) {
								LCDPutStringCenter(2,"Mettere");
								LCDPutStringCenter(3,"filtro");
								while(m_MicroFiltro) {
									__delay_ms(100);
									scanKBD();
									if(FLAGS & (1 << TRIGK)) {
										BYTE ch;
										ch=GetKBchar(kbKeys[0]);
										if(ch==KB_OK)
											break;
										if(ch==KB_MENU_ESC) {
											State= (State & STATE_OPERATION_SACCHETTO) | STATE_FINITO;
											break;
											}
										}
									ClrWdt();
									}
								LCDCls();
								}
							}
						if(State & STATE_OPERATION_SACCHETTO) {
							if(configParms.needSacchetto) {
								if(m_MicroSacchetto) {
									LCDPutStringCenter(2,"Mettere");
									LCDPutStringCenter(3,"sacchetto");
									while(m_MicroSacchetto) {
										__delay_ms(100);
										scanKBD();
										if(FLAGS & (1 << TRIGK)) {
											BYTE ch;
											ch=GetKBchar(kbKeys[0]);
											if(ch==KB_OK)
												break;
											if(ch==KB_MENU_ESC) {
												State= STATE_OPERATION_SACCHETTO | STATE_FINITO;
												break;
												}
											}
										ClrWdt();
										}
									LCDCls();
	
									}
								}
							}
						LCDPutStringCenter(0,"Preparazione");
						if(State & STATE_OPERATION_SACCHETTO) {
							if(configParms.mBilancia) {
								LCDPutStringCenter(1,"taratura");
								BilanciaTara();
								__delay_ms(400);
								ClrWdt();
								}
							LCDPutStringCenter(1,tipoOperazione==0 ? "1/4 sacco" :
								(tipoOperazione==1 ? "1/2 sacco" : "1 sacco"));
							if(!(configParms.mVibro1[tipoOperazione+3]) && !(configParms.mVibro2[tipoOperazione+3])) {		// QUA ci sarebbe ancora il problema del tempo negativo
								ModBusSetMotore(1);
								State= STATE_OPERATION_SACCHETTO | STATE_MACINATURA;
								tempoOperazione=configParms.tMacinatura[tipoOperazione+3]/10;		// resto in decimi!
								}	
							else {
								State= STATE_OPERATION_SACCHETTO | STATE_VIBROPRE;
								if(configParms.mVibro1[tipoOperazione+3] && configParms.tVibro1[0]>0) {
									SetPWM(0,configParms.vVibro[0]);
									}
								if(configParms.mVibro2[tipoOperazione+3] && configParms.tVibro1[1]>0) {
									SetPWM(1,configParms.vVibro[1]);
									}
								tempoOperazione=configParms.tVibro1[0];
								}	
							}
						else {
							LCDPutStringCenter(1,tipoOperazione==0 ? "1 dose" :
								(tipoOperazione==1 ? "2 dosi" : "3 dosi"));
							if(!(configParms.mVibro1[tipoOperazione]) && !(configParms.mVibro2[tipoOperazione])) {		// QUA ci sarebbe ancora il problema del tempo negativo
								ModBusSetMotore(1);
								State= STATE_MACINATURA;
								tempoOperazione=configParms.tMacinatura[tipoOperazione]/10;
								}	
							else {
								State= STATE_VIBROPRE;
								if(configParms.mVibro1[tipoOperazione] && configParms.tVibro1[0]>0) {
									SetPWM(0,configParms.vVibro[0]);
									}
								if(configParms.mVibro2[tipoOperazione] && configParms.tVibro1[1]>0) {
									SetPWM(1,configParms.vVibro[1]);
									}
								tempoOperazione=configParms.tVibro1[0];
								}	
							}	
						break;
					case STATE_VIBROPRE:				// 
						if(State & STATE_OPERATION_SACCHETTO) {
							if(tempoOperazione)
								tempoOperazione--;
							else {
								ModBusSetMotore(1);
								if(configParms.mBilancia)
									BilanciaLeggiPeso();			// lettura dummy per ciucca
								State= STATE_OPERATION_SACCHETTO | STATE_MACINATURA;
								tempoOperazione=configParms.tMacinatura[tipoOperazione+3]/10;
								if(configParms.mVibro1[tipoOperazione+3]) {
									SetPWM(0,configParms.vVibro[0]);
									}
								if(configParms.mVibro2[tipoOperazione+3]) {
									SetPWM(1,configParms.vVibro[1]);
									}
								}	
							}
						else {
							if(tempoOperazione)
								tempoOperazione--;
							else {
								ModBusSetMotore(1);
								State= STATE_MACINATURA;
								tempoOperazione=configParms.tMacinatura[tipoOperazione]/10;
								if(configParms.mVibro1[tipoOperazione]) {
									SetPWM(0,configParms.vVibro[0]);
									}
								if(configParms.mVibro2[tipoOperazione]) {
									SetPWM(1,configParms.vVibro[1]);
									}
								}	
							}			
						break;
					case STATE_MACINATURA:				// macinatura
						if(State & STATE_OPERATION_SACCHETTO) {
							char buf[32];
							BYTE t1,t2,t3;
	
	// INCRICCAva MODBUS... 20.12.17						
							if(configParms.mBilancia) {
								static BYTE divider=0;
								divider++;
								if(!(divider & 2))
									n2=BilanciaLeggiPeso();
								}
	
							t1=tempoOperazione/600;
							t2=(tempoOperazione/10) % 60;
							t3=tempoOperazione % 10;
							sprintf(buf,"t: %u:%02u.%u0  ",t1,t2,t3);
							LCDPutStringXY(1,3,buf);
							if(configParms.mBilancia) {
								sprintf(buf,"%4ld.%02u g",n2/100,abs(n2) % 100);
								LCDPutStringXY(0,2,"            ");
								LCDPutStringXY(2,2,buf);
								}
							else {
								t1=configParms.tMacinatura[tipoOperazione+3]/6000;
								t2=(configParms.tMacinatura[tipoOperazione+3]/100) % 60;
								t3=configParms.tMacinatura[tipoOperazione+3] % 100;
								sprintf(buf,"-> %u:%02u.%02u  ",t1,t2,t3);
								LCDPutStringXY(1,2,buf);
								}
							if(tempoOperazione) {
								tempoOperazione--;
								}
							else {
								ModBusSetMotore(0);
								if(!configParms.mVibro1[tipoOperazione+3] && !configParms.mVibro2[tipoOperazione+3]) {
									State= STATE_OPERATION_SACCHETTO | STATE_FINITO;
									}
								else {
									tempoOperazione=max(configParms.tVibro1[1],configParms.tVibro2[1]);
									if(configParms.mVibro1[tipoOperazione+3])
										SetPWM(0,configParms.vVibro[0]);
									if(configParms.mVibro2[tipoOperazione+3])
										SetPWM(1,configParms.vVibro[1]);
									State= STATE_OPERATION_SACCHETTO | STATE_VIBROPOST;
									}	
								}	
							}
						else {
							char buf[16];
							BYTE t1,t2,t3;
	
							t1=tempoOperazione/600;
							t2=(tempoOperazione/10) % 60;
							t3=tempoOperazione % 10;
							sprintf(buf,"t: %u:%02u.%u0  ",t1,t2,t3);
							LCDPutStringXY(1,3,buf);
							t1=configParms.tMacinatura[tipoOperazione]/6000;
							t2=(configParms.tMacinatura[tipoOperazione]/100) % 60;
							t3=configParms.tMacinatura[tipoOperazione] % 100;
							sprintf(buf,"-> %u:%02u.%02u  ",t1,t2,t3);
							LCDPutStringXY(1,2,buf);
							if(tempoOperazione) {
								tempoOperazione--;
								}
							else {
								ModBusSetMotore(0);	
								if(!configParms.mVibro1[tipoOperazione] && !configParms.mVibro2[tipoOperazione]) {
									State= STATE_FINITO;
									}
								else {
									tempoOperazione=max(configParms.tVibro1[1],configParms.tVibro2[1]);
									if(configParms.mVibro1[tipoOperazione])
										SetPWM(0,configParms.vVibro[0]);
									if(configParms.mVibro2[tipoOperazione])
										SetPWM(1,configParms.vVibro[1]);
									State= STATE_VIBROPOST;
									}	
								}	
							}			
						break;
					case STATE_VIBROPOST:				// 
						if(State & STATE_OPERATION_SACCHETTO) {
	//GESTIRE 2 TEMPI!
							if(tempoOperazione)
								tempoOperazione--;
							else {
								ModBusSetMotore(0);		// non servirebbe...
								SetPWM(0,0);
								SetPWM(1,0);
								State= STATE_OPERATION_SACCHETTO | STATE_FINITO;
								}	
							}
						else {
							if(tempoOperazione)
								tempoOperazione--;
							else {
								ModBusSetMotore(0);		// non servirebbe...
								SetPWM(0,0);
								SetPWM(1,0);
								State= STATE_FINITO;
								}	
							}			
						break;
					case STATE_FINITO:				// finito
						if(State & STATE_OPERATION_SACCHETTO) {
							if(configParms.mBilancia) {
								char buf[32];
								BYTE i;
	
								for(i=0; i<20; i++) {			//5sec
									n2=BilanciaLeggiPeso();
									sprintf(buf,"%4ld.%02u g",n2/100,abs(n2) % 100);
									LCDPutStringXY(0,2,"            ");
									LCDPutStringXY(2,2,buf);
									__delay_ms(250);
									ClrWdt();
									}
								}
							}
	
						LCDPutStringCenter(3,"   Fatto.   ");
						SetPWM(0,0);
						SetPWM(1,0);
						State= STATE_IDLE;
						goto print_logo;
						break;
					}
					break;
				}


			if(FLAGS & (1 << TRIGK) /*kbKeys[0]*/) {
				BYTE ch;

//				ModBusSetRele(1);		// debug*****
				ch=GetKBchar(kbKeys[0]);
ch=0;

				switch(currMenu) {
//					case 0:				// caso/i particolari
//						break;
	
					default:			// leggo da menu.ini che fare con questo tasto in questo menu
{
char buf2[32],buf3[32];
				sprintf(buffer,"menu%u_%u",currMenu,currLang);
				GetProfileString(buffer,"backimg",buf2);

				sprintf(buf2,"a_s%u",ch);
				*buf3=0;
				GetProfileString(buffer,buf2,buf3);
				if(!strnicmp(buf3,"menu",4)) {
					currMenu=atoi(buf3+4);
					}	
				else if(!strnicmp(buf3,"o",1)) {
					i=atoi(buf3+1);
					}	
				else if(!strnicmp(buf3,"stop",4)) {
					}	

}
	
						switch(State & 15) {
							case STATE_IDLE:				// idle
								switch(ch) {
									case KB_MENU_ESC:
										State=STATE_SETUP;
										handleBIOS(255);		// ...
										goto print_logo;
										break;
									case KB_TAZZAPICCOLA:
										tipoOperazione=0;
										State=STATE_STARTING;
										break;
									case KB_TAZZAMEDIA:
										tipoOperazione=1;
										State=STATE_STARTING;
										break;
									case KB_TAZZAGRANDE:
										tipoOperazione=2;
										State=STATE_STARTING;
										break;
									case KB_SACCHETTOPICCOLO:
										tipoOperazione=0;
										State=STATE_STARTING | STATE_OPERATION_SACCHETTO;
										break;
									case KB_SACCHETTOMEDIO:
										tipoOperazione=1;
										State=STATE_STARTING | STATE_OPERATION_SACCHETTO;
										break;
									case KB_SACCHETTOGRANDE:
										tipoOperazione=2;
										State=STATE_STARTING | STATE_OPERATION_SACCHETTO;
										break;
									case KB_RPM:
										State=STATE_SETUP;
										handleBIOS(22);		// ... occhio VMACINATURA
										goto print_logo;
										break;
									case KB_MANUALE:
										LCDCls();
										tick10=0;
										LCDPutStringCenter(0,"Macinatura");
										ModBusSetMotore(1);
										SetPWM(0,configParms.vVibro[0]);
										SetPWM(1,configParms.vVibro[1]);
										do {
											char buf[8];
											ClrWdt();
											__delay_ms(100);
											sprintf(buf,"t: %u.%u  ",tick10/10,tick10%10);
											LCDPutStringXY(2,1,buf);
											scanKBD();
											ch=GetKBchar(kbKeys[0]);
											} while(ch==KB_MANUALE);
										ModBusSetMotore(0);
										tempoOperazione=configParms.tVibro1[1];
										while(tempoOperazione--) {
											__delay_ms(100);
											ClrWdt();
											}	
										SetPWM(0,0);
										SetPWM(1,0);
										LCDPutStringCenter(3,"Terminato.");
										goto print_logo;
										break;
									case KB_BILANCIA:
										if(configParms.mBilancia) {
											LCDCls();
											BilanciaLeggiPeso();			// lettura dummy per ciucca
											LCDPutStringCenter(1,"Peso:");
											do {
												char buf[32];
		
												kLED0_IO ^= 1;
												n2=BilanciaLeggiPeso();
												ClrWdt();
												__delay_ms(300);
												sprintf(buf,"%4ld.%02u g",n2/100,abs(n2) % 100);
		//										sprintf(buf,"%8ld g",n2);
												LCDPutStringXY(0,2,"            ");
												LCDPutStringXY(2,2,buf);
												scanKBD();
												ch=GetKBchar(kbKeys[0]);
												} while(ch != KB_BILANCIA);		// un click entra, uno esce
											__delay_ms(500);			// debounce finale
											goto print_logo;
											}	
										else {
											goto non_installato;
											}	
										break;
									case KB_TARA:
										if(configParms.mBilancia) {
											LCDCls();
											LCDPutStringCenter(1,"Attendere..");
											kLED0_IO ^= 1;
											BilanciaTara();
											__delay_ms(1000);
											LCDPutStringCenter(2,".");
											kLED0_IO ^= 1;
											ClrWdt();
											__delay_ms(1000);
											LCDPutStringCenter(2,"..");
											kLED0_IO ^= 1;
											ClrWdt();
											__delay_ms(1000);
											LCDPutStringCenter(2,"...");
											kLED0_IO ^= 1;
											ClrWdt();
											__delay_ms(1000);
											LCDPutStringCenter(2,"....");
											kLED0_IO ^= 1;
											ClrWdt();
											n2=BilanciaLeggiPeso();
											goto print_logo;
											}
										else {
											goto non_installato;
											}	
										break;
									case KB_TEMPERATURA:
		non_installato:
										LCDCls();
										LCDPutStringCenter(1,"Non");
										LCDPutStringCenter(2,"installato");
		print_logo:
										__delay_ms(2000);
										LCDCls();
		//								OLED_draw_bitmap(((GetMaxX()-GetRealOrgX())-112)/2, 0, 112,48,(void *)&logoKiki);		// in alto
										break;
									}
								break;
		
							case STATE_VIBROPRE:				// vibro1
							case STATE_MACINATURA:				// macinatura
							case STATE_VIBROPOST:				// vibro2
								switch(ch) {
									case KB_MENU_ESC:			// ESC interrompe operazione in corso!
										LCDCls();
										LCDPutStringCenter(2,"Interrotto!");
										__delay_ms(1000);
		//								LCDCls();
		//								OLED_draw_bitmap(((GetMaxX()-GetRealOrgX())-112)/2, 0, 112,48,(void *)&logoKiki);		// in alto
										ModBusSetMotore(0);
										State=STATE_FINITO;
										break;
									}
								break;
			
							case STATE_FINITO:				// 
								break;
							}
	
						break;
					}	

				}


				}				// 0.2 sec


			}			// second_10

		if(second_30) {
			handleTastiera();		// messo dopo per non fare uscire char se entro in bios ecc!
			second_30=0;
			}



#ifdef USA_ETHERNET
    // This task performs normal stack task including checking
    // for incoming packet, type of packet and calling
    // appropriate stack entity to process it.
    StackTask();
//		LED1_IO ^= 1;
    
    #if defined(WF_CS_TRIS)
		if(gRFModuleVer1209orLater)
    	WiFiTask();
    #endif

    // This tasks invokes each of the core stack application tasks
    StackApplications();

    #if defined(STACK_USE_ZEROCONF_LINK_LOCAL)
			ZeroconfLLProcess();
    #endif

    #if defined(STACK_USE_ZEROCONF_MDNS_SD)
    	mDNSProcess();
// Use this function to exercise service update function
// HTTPUpdateRecord();
    #endif

		// Process application specific tasks here.
		// For this demo app, this will include the Generic TCP 
		// client and servers, and the SNMP, Ping, and SNMP Trap
		// demos.  Following that, we will process any IO from
		// the inputs on the board itself.
		// Any custom modules or processing you need to do should
		// go here.
		#if defined(STACK_USE_GENERIC_TCP_CLIENT_EXAMPLE)
		GenericTCPClient();
		#endif
		
		#if defined(STACK_USE_GENERIC_TCP_SERVER_EXAMPLE)
		GenericTCPServer();
		#endif
		
		#if defined(STACK_USE_SMTP_CLIENT)
		SMTPDemo();
		#endif
		
		#if defined(STACK_USE_POP3_CLIENT)
		POP3Demo();
		#endif
		
		#if defined(STACK_USE_MQTT_CLIENT)
		MQTTDemo();
		#endif
		
		#if defined(STACK_USE_ICMP_CLIENT)
		PingDemo();
		#endif
		
		#if defined(STACK_USE_ICMP_SERVER)
//		if(configParms.EnablePing)		// questo � complicato da gestire... v. stacktsk
//			ICMPTask();	
		#endif
		#if defined(STACK_USE_TELNET_SERVER)
		if(configParms.EnableTelnet)
//			TelnetTask();	
		#endif
		#if defined(STACK_USE_HTTP2_SERVER)
		if(configParms.EnableWWW)
//			HTTPServer();
		#endif
		#if defined(STACK_USE_UART2TCP_BRIDGE) || defined(STACK_USE_UART2TCP_MIRROR)
		if(configParms.EnableUartBridge)
//			UART2TCPBridgeTask();
		#endif

		#if defined(STACK_USE_SNMP_SERVER) && !defined(SNMP_TRAP_DISABLED)
		//User should use one of the following SNMP demo
		// This routine demonstrates V1 or V2 trap formats with one variable binding.
		SNMPTrapDemo();
		
		#if defined(SNMP_STACK_USE_V2_TRAP) || defined(SNMP_V1_V2_TRAP_WITH_SNMPV3)
		//This routine provides V2 format notifications with multiple (3) variable bindings
		//User should modify this routine to send v2 trap format notifications with the required varbinds.
		//SNMPV2TrapDemo();
		#endif 
		if(gSendTrapFlag)
			SNMPSendTrap();
		#endif
		
    // If the local IP address has changed (ex: due to DHCP lease change)
    // write the new IP address to the LCD display, UART, and Announce 
    // service
		if(dwLastIP != AppConfig.MyIPAddr.Val) {
			dwLastIP = AppConfig.MyIPAddr.Val;
			
			#if defined(STACK_USE_UART)
				putrsUART((ROM char*)"\r\nNew IP Address: ");
			#endif

//			DisplayIPValue(AppConfig.MyIPAddr);
			sprintf(buffer,"New IP: %u.%u.%u.%u",AppConfig.MyIPAddr.v[0],AppConfig.MyIPAddr.v[1],
				AppConfig.MyIPAddr.v[2],AppConfig.MyIPAddr.v[3]);
			LCDPutStringXY(0,6,buffer);

			#if defined(STACK_USE_UART)
				putrsUART((ROM char*)"\r\n");
			#endif

			#if defined(STACK_USE_ANNOUNCE)
				AnnounceIP();
			#endif

      #if defined(STACK_USE_ZEROCONF_MDNS_SD)
				mDNSFillHostRecord();
			#endif
			}	

#endif

		}			//main loop


  return (-1);
	}

int handleTastiera(void) {
	BYTE ch,i,j;
	static BYTE autorepeatDivider=0;

		if(!autorepeatDivider) {
			if(configParms.repeat)
				autorepeatDivider=(30 /*era 36 ma fottiti */ /configParms.repeat);
			else
				autorepeatDivider=8;		// tanto per
			}
		else {
			autorepeatDivider--;
			goto no_repeat;
			}	

		if(!kbKeys[0])
			goto check_ril;

		if(configParms.keyclick) {
#ifdef USA_BUZZER_PWM 		
			OC2CON1 |= 0x0006;   // on
					__delay_ms(25);
			OC2CON1 &= ~0x0006;   // off
#else
			LATGbits.LATG15=1;   // on
					__delay_ms(25);
			LATGbits.LATG15=0;   // off
#endif
			}

		{
			j=0;


			if(!configParms.repeat) {
				if(!(FLAGS & (1 << TRIGK))) {		// se trig, SEMPRE
					if(!memcmp(kbKeys,oldkbKeys,MAX_TASTI_CONTEMPORANEI)) {
						goto no_repeat;
						}	
					}	
				}

			for(i=j; i<MAX_TASTI_CONTEMPORANEI; i++) {
				if(kbKeys[i]) {
					ch=GetKBchar(kbKeys[i]);
//								LCDPutChar(ch);

			//COMPLETARE con gestione numeri e traduzione ecc
			//  Codice operazione: da 000 a 099 se digitato da tastiera
			//  da 100 a 104 se inserito con tasti pittogra
			//  da 105 a 110 se con i tasti da F1 a F6



					}
				}
			}
	
check_ril:
		if(!kbKeys[0]) {		// al rilascio di tutto...
			autorepeatDivider=0;
			FLAGS &= ~((1<<6) | (1<<7));
			if(configParms.keyclick) {
/*
#ifdef USA_BUZZER_PWM 		
				OC2CON1 &= ~0x0006;   // off
#else
				LATGbits.LATG15=0;   // off
#endif */
				}
			if(oldkbKeys[0]) {
				}	
			}	
		memcpy(oldkbKeys,kbKeys,MAX_TASTI_CONTEMPORANEI);
		if(FLAGS & (1 << TRIGK)) {		// 
			FLAGS &= ~(1 << TRIGK);
			}			//TRIGK
no_repeat: ;
	}

BYTE GetKBchar(BYTE n) {			// lascio tanto per somiglianza con altri codici..
	static BYTE kbLayout[5*5+1]= {
		KB_DUMMY,

		KB_OK,			// row0:col0
		KB_MENU_ESC,
		KB_TAZZAPICCOLA,
		KB_UNUSED,
		KB_TARA,

		KB_MENO,		// row0:col1
		KB_UP,
		KB_TAZZAMEDIA,
		KB_RPM,
		KB_SACCHETTOGRANDE,

		KB_PIU,			// row0:col2
		KB_DOWN,
		KB_TAZZAGRANDE,
		KB_TEMPERATURA,
		KB_SACCHETTOMEDIO,
		
		KB_UNUSED,	// row0:col3
		KB_UNUSED,
		KB_BILANCIA,
		KB_MANUALE,
		KB_SACCHETTOPICCOLO,
	
		KB_UNUSED,	// row0:col4
		KB_UNUSED,
		KB_UNUSED,
		KB_UNUSED,
		KB_UNUSED
		};

	return kbLayout[n];
	}
BYTE GetKBcharNext(void) {

	return 0;
	}


int handleBIOS(BYTE q) {		// se q != 255, si posiziona su item desiderato
	BYTE currItem=0,oldItem;
	char buffer[64];
	struct CONFIG_PARMS newConfigTerminale;
	BYTE n;
	WORD timeOutCnt=0;
	BYTE	longPressCnt=0;


	// settare risoluzione grande? ma poi si intruppa quando fa il restore non-destructive... boh
	memcpy(&newConfigTerminale,&configParms,sizeof(configParms));

reprint_menu:
	oldItem=255;
	if(q!=255)
		currItem=q;
 	SetColor(WHITE /*BLUE*/);
//	SetTextColorPalette(_TESTO_BLU);			// back...
//	ClearDevice();
	LCDCls();
	SetTextColorPalette(_TESTO_BIANCO);			// 
// 	SetColor(WHITE);
  SetFont((void *)&font32);
	LCDPutStringCenter(0,"Setup");
	refreshScreen();

	timeOutCnt=30*10;

//	KBclear();		 no...
	for(;;) {
		if(currItem!=oldItem) {
//									OutTextXY(GetRealOrgX()+10,10,"                              ");
//	  	SetColor(BLUE);
//			Bar(GetRealOrgX()+10,7,GetRealMaxX()+40 /*boh!*/,13+getCharSizeY());
//	  	SetColor(WHITE);
			LCDPutStringXY(0,1,(char *)"            ");
			LCDPutStringXY(0,2,(char *)"            ");
			LCDPutStringXY(0,3,(char *)"            ");
			LCDPutStringXY(0,1,(char *)lcdItems[currItem].name);
			showAParm(currItem,buffer);
			LCDPutStringXY(/* qua cos� lcdItems[currItem].size+1*/ 0,3,buffer);
			refreshScreen();
			oldItem=currItem;
			}

	  __delay_ms(100);
		scanKBD();
		switch(menuLevel) {			// FINIRE con gerarchia, 
			case MENU_IDLE:		// SCHERMATA_SPLASH idle, qualsiasi tasto
				break;
			case MENU_MAIN:		// main
				switch(kbKeys[0]) {
					case KB_OK:
						switch(currSchermItem) {
							case 0:
								currSchermata=SCHERMATA_SPLASH;
								break;
							}
						break;		
					}		
				break;		
			}	
		if((FLAGS & (1 << TRIGK)) || kbKeys[0]==KB_PIU || kbKeys[0]==KB_MENO) {
		timeOutCnt=30*10;
		if(longPressCnt<60)			// andiam solo fino a 50 cmq
			longPressCnt++;
		switch(kbKeys[0]) {
			case KB_PIU:			//
				if(!lcdItems[currItem].var)			// se � disattivata, salto (v. sotto)
					break;
				switch(lcdItems[currItem].varSize) {
					case 0:
						(*((BYTE *)lcdItems[currItem].var))++;
						(*((BYTE *)lcdItems[currItem].var)) &= 1;
						break;
					case 1:
						switch(currItem) {
							case 29:		// caso particolare contrasto
								if((*((BYTE *)lcdItems[currItem].var)) < 250)
									(*((BYTE *)lcdItems[currItem].var))+=5;
//								OLED_set_contrast(*((BYTE *)(lcdItems[currItem].var)));
								break;
							default:
								if((*((BYTE *)lcdItems[currItem].var)) < lcdItems[currItem].valMax)
									(*((BYTE *)lcdItems[currItem].var))++;
								break;
							}	
						break;
					case 2:
						switch(currItem) {
							case 16:
							case 17:
							case 18:
							case 19:
							case 20:
							case 21:
							case 22:		// caso particolare macinatura e tempi motore
								if(longPressCnt<10) {
									if((*((WORD *)lcdItems[currItem].var)) < (lcdItems[currItem].valMax-1))
										(*((WORD *)lcdItems[currItem].var))+=1;
									}	
								else if(longPressCnt<30) {
									if((*((WORD *)lcdItems[currItem].var)) < (lcdItems[currItem].valMax-5))
										(*((WORD *)lcdItems[currItem].var))+=5;
									}	
								else if(longPressCnt<50) {
									if((*((WORD *)lcdItems[currItem].var)) < (lcdItems[currItem].valMax-10))
										(*((WORD *)lcdItems[currItem].var))+=10;
									}	
								else {
									if((*((WORD *)lcdItems[currItem].var)) < (lcdItems[currItem].valMax-100))
										(*((WORD *)lcdItems[currItem].var))+=100;
									}
								break;
							default:
								if((*((WORD *)lcdItems[currItem].var)) < lcdItems[currItem].valMax)
									(*((WORD *)lcdItems[currItem].var))++;
								break;
							}
						break;
					case 4:
						if((*((DWORD *)lcdItems[currItem].var)) < lcdItems[currItem].valMax)
							(*((DWORD *)lcdItems[currItem].var))++;
						break;
					}
				oldItem=254; 		// tanto per.. forza redraw!
				break;
			case KB_MENO:			//
				if(!lcdItems[currItem].var)			// se � disattivata, salto (v. sotto)
					break;
				switch(lcdItems[currItem].varSize) {
					case 0:
						(*((BYTE *)lcdItems[currItem].var))--;
						(*((BYTE *)lcdItems[currItem].var)) &= 1;
						break;
					case 1:
						switch(currItem) {
							case 29:		// caso particolare contrasto
								if((*((BYTE *)lcdItems[currItem].var)) > 5)
									(*((BYTE *)lcdItems[currItem].var))-=5;
//								OLED_set_contrast(*((BYTE *)(lcdItems[currItem].var)));
								break;
							default:
								(*((BYTE *)lcdItems[currItem].var))--;
								if((*((BYTE *)lcdItems[currItem].var)) > lcdItems[currItem].valMax)
									(*((BYTE *)lcdItems[currItem].var))=lcdItems[currItem].valMin;
								break;
							}	
						break;
					case 2:
						switch(currItem) {
							case 16:
							case 17:
							case 18:
							case 19:
							case 20:
							case 21:
							case 22:		// caso particolare macinatura e tempi motore
								if(longPressCnt<5) {
									if((*((WORD *)lcdItems[currItem].var)) > (lcdItems[currItem].valMin+1))
										(*((WORD *)lcdItems[currItem].var))-=1;
									}	
								else if(longPressCnt<30) {
									if((*((WORD *)lcdItems[currItem].var)) > (lcdItems[currItem].valMin+5))
										(*((WORD *)lcdItems[currItem].var))-=5;
									}	
								else if(longPressCnt<50) {
									if((*((WORD *)lcdItems[currItem].var)) > (lcdItems[currItem].valMin+10))
										(*((WORD *)lcdItems[currItem].var))-=10;
									}	
								else {
									if((*((WORD *)lcdItems[currItem].var)) > (lcdItems[currItem].valMin+100))
										(*((WORD *)lcdItems[currItem].var))-=100;
									}	
								break;
							default:
								if((*((WORD *)lcdItems[currItem].var)) > lcdItems[currItem].valMin)
									(*((WORD *)lcdItems[currItem].var))--;
								break;
							}	
						break;
					case 4:
						if((*((DWORD *)lcdItems[currItem].var)) > lcdItems[currItem].valMin)
							(*((DWORD *)lcdItems[currItem].var))--;
						break;
					}
				oldItem=254; 		// tanto per.. forza redraw!
				break;
			case KB_UP:
				if(currItem>0)
					currItem--;
				break;
			case KB_DOWN:
				if(currItem<(ULTIMA_SCHERMATA_PROGRAMMAZIONE-PRIMA_SCHERMATA_PROGRAMMAZIONE-1))
					currItem++;
				break;
			case KB_MENU_ESC:		// no save
				if(MostraSiNo(NULL)) {
					memcpy(&configParms,&newConfigTerminale,sizeof(configParms));
					n=0;
					goto fine;
					}
				else {
			  	goto reprint_menu;
					}
				break;
			case KB_OK:		// 
				if(MostraSiNo(NULL)) {
					saveSettings();
					n=1;
					goto fine;
					}
				else {
			  	goto reprint_menu;
					}
				break;
			}
			}
		else
			longPressCnt=0;

		timeOutCnt--;
		if(!timeOutCnt) {
			n=0;
			break;
			}

		} 
fine:
	return n;
	}

int showBIOS(void) {			// per ora non usato ma mai dire mai, per i menu gerarchici
	BYTE i,n;
	char buffer[64];

	n=1;
	FLAGS |= (1 << 8);
	for(i=0; i<(ULTIMA_SCHERMATA_PROGRAMMAZIONE-PRIMA_SCHERMATA_PROGRAMMAZIONE); i++) {
//  	SetColor(BLUE);
//	SetBkColor(_TESTO_BLU);			// 
//		ClearDevice();
 // 	SetColor(WHITE);
		SetTextColorPalette(_TESTO_BIANCO);			// 
		LCDCls();
	  SetFont((void *)&font32);
		LCDPutStringXY(0,3,(char *)lcdItems[i].name);
		showAParm(i,buffer);
		LCDPutStringXY(lcdItems[i].size+1,3,buffer);		// mettere u.m.
		refreshScreen();
	  __delay_ms(1000);
		scanKBD();
		if(kbKeys[0]==KB_OK)  {		// ENTER boh per uscire!
			n=0;
			goto fine;
			}
		}
fine:
	FLAGS &= ~(1 << 8);
	return n;
	}

int showAParm(BYTE n,char *buf) {
	int i=0;

	switch(lcdItems[n].varSize) {
		case 0:
			i=sprintf(buf,"%s",*((BYTE *)lcdItems[n].var) ? "si" : "no");
			break;
		case 1:
			switch(n) {
				case 28:		// caso particolare backlight
					i=*((BYTE *)lcdItems[n].var);
					i=sprintf(buf,"%s",i==2 ? "TIMED" : (i ? "ENABLED" : "DISABLED"));
					break;	
				default:
					if(lcdItems[n].unita)
						i=sprintf(buf,"%u %s  ",*((BYTE *)lcdItems[n].var),lcdItems[n].unita);
					else
						i=sprintf(buf,"%u  ",*((BYTE *)lcdItems[n].var));
					break;	
				}	
			break;
		case 2:
			if(lcdItems[n].unita)
				i=sprintf(buf,"%u %s  ",*((WORD *)lcdItems[n].var),lcdItems[n].unita);		// abbondanza per cancellare
			else
				i=sprintf(buf,"%u  ",*((WORD *)lcdItems[n].var));
			break;
		case 4:
			if(lcdItems[n].unita)
				i=sprintf(buf,"%lu %s  ",*((DWORD *)lcdItems[n].var),lcdItems[n].unita);
			else
				i=sprintf(buf,"%lu  ",*((DWORD *)lcdItems[n].var));
			break;
		}

	return i;
	}



WORD myOutChar(XCHAR ch) {		// (NO non-weak) per aggiungere mirror in RAM! e per gestire font ridefiniti
	int i,x;
// con questa siamo a 1.6mSec per char circa - 13/10/14



	SetArea(GetX(),GetY(),GetX()+getCharSizeX()-1,GetY()+getCharSizeY()-1);

//fare check, e cancellare il "sotto" solo se � cambiato??
	x=getSizeX();
	i=LCDX+LCDY*x;

	if(!(FLAGS & (1 << 8))) {		// per gestire sovrapposizione setup..

		}

//  SetColor((lcdAttrib[i] & 1) ? BLACK : WHITE);

	return subOutChar(ch);

	}


WORD subOutChar(XCHAR ch) {
	int i;
  WORD  temp;
  WORD  mask, restoremask;
  SHORT xCnt, yCnt, xFontSize, yFontSize;
	WORD *theFont;


 
  restoremask = 0x8000; // 
    
	i=getSizeX();
	i=LCDX+LCDY*i;

	if(myFont==font32) {
		yFontSize=32;
		xFontSize=16;
		theFont=(WORD *)&font32;
		theFont+=((ch-' ')*32);
		}
	else {
		yFontSize=16;
		xFontSize=12;
		theFont=(WORD *)&font16;
		theFont+=((ch-' ')*16);
		}

	WriteCommand(CMD_WR_MEMSTART);
	DisplayEnable();
	DisplaySetData(); 

  for(yCnt=0; yCnt<yFontSize; yCnt++) {
    mask = 0;

    for(xCnt=0; xCnt<xFontSize; xCnt++) {
      if(!mask) {		// 
        temp = *theFont++;
				temp=(LOBYTE(temp) << 8) | HIBYTE(temp);
        mask = restoremask;
        }
			
// provo con PutPixel ottimizzata, spostando setarea all'inizio
      if(temp & mask) 
			  SetColor(WHITE);
			else
			  SetColor(BLACK);

				WriteColor(_color);
              
      mask >>= 1;
      }
    }

		DisplayDisable();

  // move cursor
//  _cursorX = x - (!configParms.screenSize ? 16 : 12) /*boh...*/;

  // restore color


  return 1;
	}

WORD subOutCharTransparent(XCHAR ch) {
	int i,x,y;
  WORD  temp;
  WORD  mask, restoremask;
  SHORT xCnt, yCnt, xFontSize, yFontSize;
	WORD *theFont;


 
  restoremask = 0x8000; // 
    
	i=getSizeX();
	i=LCDX+LCDY*i;

	if(myFont==font32) {
		yFontSize=32;
		xFontSize=16;
		theFont=(WORD *)&font32;
		theFont+=((ch-' ')*32);
		}
	else {
		yFontSize=16;
		xFontSize=12;
		theFont=(WORD *)&font16;
		theFont+=((ch-' ')*16);
		}

  y = GetY();
  for(yCnt=0; yCnt<yFontSize; yCnt++) {
    mask = 0;
    x = GetX();

    for(xCnt=0; xCnt<xFontSize; xCnt++) {
      if(!mask) {		// 
        temp = *theFont++;
				temp=(LOBYTE(temp) << 8) | HIBYTE(temp);
        mask = restoremask;
        }
			
// provo con PutPixel ottimizzata, spostando setarea all'inizio
      if(temp & mask) 
			  PutPixel(x,y);
              
      mask >>= 1;
			x++;
      }
		y++;
    }

  return 1;
	}

WORD OutCharRenderRAM(XCHAR ch, BYTE *theFont,BYTE reverse) {
  BYTE        temp;
  BYTE        mask;
  BYTE  restoremask;
  SHORT xCnt, yCnt, xFontSize, yFontSize;
    
  restoremask = 0x80; // POI PASSARE a word ?

// STRIMINZISCO per mancanza RAM! poi rimettere a posto (anche redef char)    
		yFontSize=8 /*16*/;
		xFontSize=6 /*12*/;

  if(IsDeviceBusy())
    return (0);

	WriteCommand(CMD_WR_MEMSTART);
	DisplayEnable();
	DisplaySetData(); 

  for(yCnt=0; yCnt<yFontSize; yCnt++) {
    mask = 0;

    for(xCnt=0; xCnt<xFontSize; xCnt++) {
      if(!mask) {		// 
        temp = *theFont;
        mask = restoremask;
        }

			if(reverse) {
// o si potrebbe plottare sia neri che bianchi, evitando di sbianchettare prima... per� poi salta il superimpose con la grafica
	      if(temp & mask) 
				  SetColor(BLACK);
				else
				  SetColor(WHITE);

					WriteColor(_color);
//2x in orizzontale...
					WriteColor(_color);
				}
			else {
	      if(temp & mask) 
				  SetColor(WHITE);
				else
				  SetColor(BLACK);

					WriteColor(_color);
//2x in orizzontale...
					WriteColor(_color);
        }
            
      mask >>= 1;
      }

//... e 2x in verticale...
    mask = 0;

    for(xCnt=0; xCnt<xFontSize; xCnt++) {
      if(!mask) {		// 
        temp = *theFont++;
        mask = restoremask;
        }

			if(reverse) {
// o si potrebbe plottare sia neri che bianchi, evitando di sbianchettare prima... per� poi salta il superimpose con la grafica
	      if(temp & mask) 
				  SetColor(BLACK);
				else
				  SetColor(WHITE);

					WriteColor(_color);
//2x in orizzontale...
					WriteColor(_color);
				}
			else {
	      if(temp & mask) 
				  SetColor(WHITE);
				else
				  SetColor(BLACK);

					WriteColor(_color);
//2x in orizzontale...
					WriteColor(_color);
        }
            
      mask >>= 1;
      }

    }


  // move cursor
//  _cursorX = x - (!configParms.screenSize ? 8 : 6) /*boh...*/;
				DisplayDisable();

  return 1;
	}

void OutCharCursor(BYTE ch) {
  SHORT xCnt, yCnt, x, y, xFontSize, yFontSize;

int i;

	ch+=155;


	{
  WORD temp;
  WORD mask,restoremask;
	const WORD *theFont;

//fondamentalmente copiata da outchar e outcharrenderRAM, ma duplicata per i cursori
	if(myFont==font32) {
		yFontSize=32;
		xFontSize=16;
		theFont=(WORD *)&font32;
		theFont+=((ch-' ')*32);
		}
	else {
		yFontSize=16;
		xFontSize=12;
		theFont=(WORD *)&font16;
		theFont+=((ch-' ')*16);
		}

  restoremask = 0x8000;

  y = curPosY;
  for(yCnt=0; yCnt<yFontSize; yCnt++) {
    x = curPosX;
    mask = 0;

    for(xCnt=0; xCnt<xFontSize; xCnt++) {
      if(!mask) {		// 
        temp = *theFont++;
				temp=(LOBYTE(temp) << 8) | HIBYTE(temp);
        mask = restoremask;
        }
			
      if(temp & mask) {
        PutPixel(x, y);
        }
              
      mask >>= 1;
      x++;
      }
    y++;
    }
	}
	}

void LCDPutChar(BYTE ch) {

  MoveTo(curPosX,curPosY);
	if(FLAGS & (1 << 4))
		drawCursor(0);
  switch(ch) {
		case CR:
			curPosX=GetRealOrgX(); LCDX=0;
			break;
		case LF:
			curPosY+=getCharSizeY();	LCDY++;
			// questo � SEMPRE 
			if(LCDY>=getSizeY()) {
//				LCDscrollY();
				LCDXY(LCDX,getSizeY()-1);
				}
			break;
		default:
			if(ch>=32 && ch<127) {
//			  SetColor(attribGrafici.reverse ? BLACK : WHITE);
//			  SetFont(!configParms.screenSize ? (void *)&Font15 : (void *)&Font30);
				myOutChar(ch);
printed:
				curPosX+=getCharSizeX();
				LCDX++;
				if(LCDX>=getSizeX()) {
					curPosX=GetRealOrgX();
					LCDX=0;
					curPosY+=getCharSizeY();
					LCDY++;
					if(LCDY>=getSizeY()) {
//							LCDscrollY();
						LCDXY(LCDX,getSizeY()-1);
						}
					}		//LCDX
				else if(LCDY==getSizeY() && LCDX==1) {		
//					LCDscrollY();
					LCDXY(0,getSizeY()-1);
					myOutChar(ch);
					LCDXY(1,getSizeY()-1);
					}
				}
			else if(ch>=128) {
//			  SetColor(attribGrafici.reverse ? BLACK : LIGHTGRAY);
//			  SetFont(!configParms.screenSize ? (void *)&Font15 : (void *)&Font30);
				myOutChar(ch);
				goto printed;
				}

			break;
		}
	}

void refreshScreen(void) {
	}

void LCDPutString(const char *s) {

	if(FLAGS & (1 << 4))
		drawCursor(0);
	while(*s) {
	  MoveTo(curPosX,curPosY);
	  switch(*s) {
			case CR:
				curPosX=GetRealOrgX(); LCDX=0;
				break;
			case LF:
				curPosY+=getCharSizeY();	LCDY++;
				// questo � SEMPRE 
				if(LCDY>=getSizeY()) {
//					LCDscrollY();
					LCDXY(LCDX,getSizeY()-1);
					}
				break;
			default:
				if(((BYTE)*s)>=32 && ((BYTE)*s)<127) {
//				  SetColor(attribGrafici.reverse ? BLACK : WHITE);
//				  SetFont(!configParms.screenSize ? (void *)&Font15 : (void *)&Font30);
			  	myOutChar(*s);
printed:
					curPosX+=getCharSizeX();
					LCDX++;
					if(LCDX>=(getSizeX())) {
						curPosX=GetRealOrgX();
						LCDX=0;
						curPosY+=getCharSizeY();
						LCDY++;
						if(LCDY>=getSizeY()) {
//								LCDscrollY();
							LCDXY(LCDX,getSizeY()-1);
							}
						}		//LCDX
					else if(LCDY==getSizeY() && LCDX==1) {		
//						LCDscrollY();
						LCDXY(0,getSizeY()-1);
						myOutChar(*s);
						LCDXY(1,getSizeY()-1);
						}
					}
				else if(((BYTE)*s)>=128) {
//				  SetColor(attribGrafici.reverse ? BLACK : LIGHTGRAY);
//				  SetFont(!configParms.screenSize ? (void *)&Font15 : (void *)&Font30);
					myOutChar((BYTE)*s);									// 
					goto printed;
					}

				break;
			}
		s++;
		}
	}

void LCDCls(void) {

  SetColor(BLACK);
	ClearDevice();
	LCDHome();
	}

void LCDHome(void) {

	curPosX=GetRealOrgX();
	curPosY=GetRealOrgY();
	LCDX=LCDY=0;
	}

void LCDXY(BYTE x,BYTE y) {

	if(x>=getSizeX())
		x=getSizeX()-1;
	if(y>=getSizeY())
		y=getSizeY()-1;
	curPosX=GetRealOrgX()+(x*getCharSizeX());
	curPosY=GetRealOrgY()+(y*getCharSizeY());
  MoveTo(curPosX,curPosY);
	LCDX=x; LCDY=y;
	}

void LCDXYGraph(WORD x,WORD y) {			// da usare solo per label semplice

	if(x>=GetMaxX())
		x=GetMaxX()-1;
	if(y>=GetMaxY())
		y=GetMaxY()-1;
	curPosX=GetRealOrgX()+x;
	curPosY=GetRealOrgY()+y;
  MoveTo(curPosX,curPosY);
	LCDX=x/getCharSizeX(); LCDY=y/getCharSizeY();			// tanto per rimanere allineati!
	}


void LCDPutStringXY(BYTE x, BYTE y, const char *s) {
	
	LCDXY(x,y);
	LCDPutString(s);
	}

void LCDPutStringCenter(BYTE y,const char *text) {
  SHORT  width;

//	SetFont((void *)font);
//  width = GetTextWidth(text, (void *)font);
  width = strlen(text);

  LCDPutStringXY(((getSizeX()) - width) / 2, y, text);
	}


int MostraSiNo(const char *title) {
	int n=-1;
	
//  SetColor(BLUE);
//  ClearDevice();
//  SetColor(WHITE);
	LCDCls();

	LCDPutStringCenter(2,(char *)(title ? title : "Sei sicuro?"));
	LCDPutStringXY(4,4,"si");
	LCDPutStringXY(9,4,"no");

	refreshScreen();

	do {
		scanKBD(); 
		__delay_ms(100);
		ClrWdt();
		if(FLAGS & (1 << TRIGK)) {
			if(kbKeys[0]==KB_OK)
				n=1;
			else if(kbKeys[0]==KB_MENU_ESC)
				n=0;
			}
		} while(n==-1);	

	return n;
	}


void drawCursor(BYTE m) {
	BYTE n;

	if(FLAGS & (1<<6) && FLAGS & (1<<7)) {
		n=4;
		}
	else if(FLAGS & (1<<6)) {
		n=5;
		}
	else if(FLAGS & (1<<7)) {
		n=3;
		}	
	else {
		n=2;
		}	
	if(m) {
	  SetColor(WHITE);
		OutCharCursor(n);	    
		FLAGS |= (1 << 4);
		}
	else {
		int i=LCDX+LCDY*getSizeX();
		clearChar();

		FLAGS &= ~(1 << 4);
		}

	}


void clearChar(void) {

  SetColor(BLACK);
	Bar(curPosX,curPosY,curPosX+getCharSizeX(),curPosY+getCharSizeY());
	}



// -----------
void resetSettings(void) {
	FSFILE *f;
	char buf[64];

	configParms.backLight=1;
	configParms.splash=1;
	configParms.test=1;
	configParms.tVibro1[0]=5;
	configParms.tVibro1[1]=5;
	configParms.tVibro2[0]=10;
	configParms.tVibro2[1]=10;

	EEFSCreateMBR(1,254);

/*{
int i;
	I2CRead16Seq(0,16);
	sprintf(buf,"%02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X \r\n",
	I2CBuffer[0],I2CBuffer[1],I2CBuffer[2],I2CBuffer[3],I2CBuffer[4],I2CBuffer[5],I2CBuffer[6],I2CBuffer[7],
		I2CBuffer[8],I2CBuffer[9],I2CBuffer[10],I2CBuffer[11]);
	LCDPutString(buf);
	for(i=54; i<512; i+=12) {
		I2CRead16Seq(i,16);
		sprintf(buf,"%u:%02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X\r\n",i,
		I2CBuffer[0],I2CBuffer[1],I2CBuffer[2],I2CBuffer[3],I2CBuffer[4],I2CBuffer[5],I2CBuffer[6],I2CBuffer[7],
			I2CBuffer[8],I2CBuffer[9],I2CBuffer[10],I2CBuffer[11]);
		LCDPutString(buf);
	}	
}
__delay_ms(4000);*/


//__delaywdt_ms(100);
__delay_ms(100);

	EEFSformat(1,0x4448,"bwEE");

	/*	{
int i;
char buf[64];
	for(i=512; i<700; i+=12) {
		I2CRead16Seq(i,16);
		sprintf(buf,"%u:%02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X\r\n",i,
		I2CBuffer[0],I2CBuffer[1],I2CBuffer[2],I2CBuffer[3],I2CBuffer[4],I2CBuffer[5],I2CBuffer[6],I2CBuffer[7],
			I2CBuffer[8],I2CBuffer[9],I2CBuffer[10],I2CBuffer[11]);
		LCDPutString(buf);
	}	
}*/

	EEFSInit();			// forza il Mount di nuovo
	__delay_ms(50);

//sprintf(buf,"M,err:%u ",EEFSerror());
//	LCDPutString(buf);
/*	f=EEFSfopen("settings.ini",FS_APPEND);
sprintf(buf,"F=%u,err:%u ",f,EEFSerror());
	LCDPutString(buf);
	if(f) {
	LCDPutString("creo file");
		EEFSfwrite("dario=1\r\n",9,1,f);
		EEFSfclose(f);
		}*/
//flushData();		// sembra che serva... v. sopra: aprire il file, o scorrere DIR... la prima non va la seconda s�

//__delay_ms(1000);

	}

void saveSettings(void) {
	int i;
	FSFILE *f;

  SetColor(BRIGHTRED);
	LCDPutStringCenter(6,"SAVING DATA..");

	f=EEFSfopen(SETTINGS_INI,FS_WRITE);
char buf[64];
sprintf(buf,"F=%u,err:%u ",f,EEFSerror());
	LCDPutString(buf);
	if(f) {
		EEFSfwrite("[opzioni]\r\n",11,1,f);
		EEFSwriteParmN(f,"backlight",configParms.backLight);
		EEFSwriteParmN(f,"splash",configParms.splash);
		EEFSwriteParmN(f,"test",configParms.test);
		EEFSfwrite("[parametri]\r\n",13,1,f);
		EEFSwriteParmN(f,"TVibro1",configParms.tVibro1[0]);
		EEFSwriteParmN(f,"TVibro2",configParms.tVibro2[0]);
		EEFSfclose(f);
		}

//  SetColor(attribGrafici.reverse ? WHITE : BLACK);
//	ClearDevice();
	}

void loadSettings(void) {
	int i,addr;
	BYTE *p;

	configParms.backLight=1;
	configParms.splash=1;
	configParms.test=1;

	if(!EEFSInit()) {
		resetSettings();
		saveSettings();
		}

	}

void exportSettingsSD(void) {		// copia INI da EEprom a SD
	int ch;
	FSFILE *f1,*f2;

	f1=EEFSfopen(SETTINGS_INI,FS_READ);
	if(f1) {
		f2=FSfopen(SETTINGS_INI,FS_WRITE);
		if(f2) {
			while((ch=EEFSgetc(f1)) != EOF) {
				FSputc(ch,f2);
				}	
			FSfclose(f2);
			}
		EEFSfclose(f1);
		}
	}

int exportSettingsUSB(void) {		// copia INI da EEprom a USB
	int ch;
	FSFILE *f1,*f2;

	f1=EEFSfopen(SETTINGS_INI,FS_READ);
	if(f1) {
		f2=USBFSfopen(SETTINGS_INI,FS_WRITE);
		if(f2) {
			while((ch=EEFSgetc(f1)) != EOF) {
				USBFSputc(ch,f2);
				}	
			USBFSfclose(f2);
			}
		EEFSfclose(f1);
		}

//  SetColor(attribGrafici.reverse ? WHITE : BLACK);
//	ClearDevice();
	return f1 && f2;
	}

int importSettingsUSB(void) {		// copia INI da USB a EEprom 
	int ch;
	FSFILE *f1,*f2;

	f1=USBFSfopen(SETTINGS_INI,FS_READ);
	if(f1) {
		f2=EEFSfopen(SETTINGS_INI,FS_WRITE);
		if(f2) {
			while((ch=USBFSgetc(f1)) != EOF) {
				EEFSputc(ch,f2);
				}	
			EEFSfclose(f2);
			}
		USBFSfclose(f1);
		}

//  SetColor(attribGrafici.reverse ? WHITE : BLACK);
//	ClearDevice();
	return f1 && f2;
	}

DWORD copyFileUSBToSD(const char *source,const char *dest) {
	int ch;
	DWORD l=0;
	FSFILE *f1,*f2;

	f1=USBFSfopen(source,FS_READ);
	if(f1) {
		f2=FSfopen(dest,FS_WRITE);
		if(f2) {
			while((ch=USBFSgetc(f1)) != EOF) {
				FSputc(ch,f2);
				l++;
				}	
			FSfclose(f2);
			}
		USBFSfclose(f1);
		}

//  SetColor(attribGrafici.reverse ? WHITE : BLACK);
//	ClearDevice();
	return l;
	}



int FSReadLine(FSFILE *f,char *buf) {
	int i,j=0;
	char ch;

  do {
		i=FSfread(&ch,1,1,f);
		if(i>0) {
			switch(ch) {
				case 13:		//Linux??
					break;
				case 10:
					i=0;
					break;
				default:
					buf[j++]=ch;
					break;
				}
			}
		} while(i>0);
	buf[j]=0;
	return j;
	}

int getParmN(const char *buf,const char *parm,int *val) {
	char *p;

	if((p=strchr(buf,'='))) {
		*p=0;
		if(!stricmp(buf,parm)) {
			if(val)
				*val=atoi(p+1);
			*p='=';
			return 1;
			}
		else			
			*p='=';
		}
	return 0;
	}

int getParmS(const char *buf,const char *parm,char *val) {
	char *p;

	if((p=strchr(buf,'='))) {
		*p=0;
		if(!stricmp(buf,parm)) {
			if(val)
				strcpy(val,p+1);
			*p='=';
			return 1;
			}
		else			
			*p='=';
		}
	return 0;
	}

int FSwriteParmN(FSFILE *f,const char *parm,int val) {

	FSfprintf(f,"%s=%d\r\n",parm,val);
	}

int FSwriteParmS(FSFILE *f,const char *parm,const char *val) {

	FSfprintf(f,"%s=%s\r\n",parm,val);
	}

int EEFSwriteParmN(FSFILE *f,const char *parm,int val) {

	EEFSfprintf(f,"%s=%d\r\n",parm,val);
	}

int EEFSwriteParmS(FSFILE *f,const char *parm,const char *val) {

	EEFSfprintf(f,"%s=%s\r\n",parm,val);
	}

BYTE GetProfileInt(const char *gruppo,const char *chiave,int *value) {
	FSFILE *f;
	char buf[64];
	int n;

	if(f=FSfopen(MENU_INI,FS_READ)) {
		if(cercaGruppo(f,gruppo)) {
			n=strlen(chiave);
			while(FSReadLine(f,buf)>0) {
				if(!strnicmp(buf,chiave,n)) {
					if(value)
						*value=atoi(buf+n+1);
					FSfclose(f);
					return 1;
					}	
				}	
			}
		FSfclose(f);
		}	
	return 0;
	}

BYTE GetProfileString(const char *gruppo,const char *chiave,char *value) {
	FSFILE *f;
	char buf[64];
	int n;

	if(f=FSfopen(MENU_INI,FS_READ)) {
		if(cercaGruppo(f,gruppo)) {
			n=strlen(chiave);
			while(FSReadLine(f,buf)>0) {
				if(!strnicmp(buf,chiave,n)) {
					if(value)
						strcpy(value,buf+n+1);
					FSfclose(f);
					return 1;
					}	
				}	
			}
		FSfclose(f);
		}	
	return 0;
	}

BYTE WriteProfileInt(const char *gruppo,const char *chiave,int value) {
	FSFILE *f;

	if(f=FSfopen(MENU_INI,FS_READWRITE)) {
		if(cercaGruppo(f,gruppo)) {
			FSwriteParmN(f,chiave,value);
			}
		FSfclose(f);
		}	

	return 0;
	}

BYTE WriteProfileString(const char *gruppo,const char *chiave,char *value) {
	FSFILE *f;

	if(f=FSfopen(MENU_INI,FS_READWRITE)) {
		if(cercaGruppo(f,gruppo)) {
			FSwriteParmS(f,chiave,value);
			}
		FSfclose(f);
		}	

	return 0;
	}

BYTE cercaGruppo(FSFILE *f,const char *gruppo) {
	char buf[64];
	int n;

	FSfseek(f,0,SEEK_SET);
	while(FSReadLine(f,buf)>0) {
		if(buf[0]=='[') {
			n=strlen(gruppo);
			if(!strnicmp(buf+1,gruppo,n)) {
				if(buf[n+1]==']') {
					return 1;
					}	
				}	
			}
		}
	}

signed char stricmp(const char *s,const char *d) {
/*
	while(*s && *d) {
		if(tolower(*s) != tolower(*d))
			return tolower(*s) - tolower(*d);
		s++;
		d++;
		ClrWdt();
		}
	return 0;
*/

  while(tolower(*s) == tolower(*d++)) {
		if(*s++ == '\0') {
			return 0;
			}
		}

	return *s - *d;
	}

signed char strnicmp(const char *s,const char *d,int n) {

  while(tolower(*s) == tolower(*d++) && n--) {
		if(*s++ == '\0') {
			return 0;
			}
		}

	return *s - *d;
	}


#ifdef USA_ETHERNET
/*********************************************************************
 * Function:        void InitAppConfig(void)
 *
 * PreCondition:    MPFSInit() is already called.
 *
 * Input:           None
 *
 * Output:          Write/Read non-volatile config variables.
 *
 * Side Effects:    None
 *
 * Overview:        None
 *
 * Note:            None
 ********************************************************************/
// MAC Address Serialization using a MPLAB PM3 Programmer and 
// Serialized Quick Turn Programming (SQTP). 
// The advantage of using SQTP for programming the MAC Address is it
// allows you to auto-increment the MAC address without recompiling 
// the code for each unit.  To use SQTP, the MAC address must be fixed
// at a specific location in program memory.  Uncomment these two pragmas
// that locate the MAC address at 0x1FFF0.  Syntax below is for MPLAB C 
// Compiler for PIC18 MCUs. Syntax will vary for other compilers.
//#pragma romdata MACROM=0x1FFF0
static ROM BYTE SerializedMACAddress[6] = {MY_DEFAULT_MAC_BYTE1, MY_DEFAULT_MAC_BYTE2, MY_DEFAULT_MAC_BYTE3, MY_DEFAULT_MAC_BYTE4, MY_DEFAULT_MAC_BYTE5, MY_DEFAULT_MAC_BYTE6};
//#pragma romdata

static void InitAppConfig(void) {
	unsigned char vNeedToSaveDefaults = 0;
	
	while(1) {
		// Start out zeroing all AppConfig bytes to ensure all fields are 
		// deterministic for checksum generation
		memset((void*)&AppConfig, 0x00, sizeof(AppConfig));
		
		AppConfig.Flags.bIsDHCPEnabled = FALSE /*TRUE*/;
		AppConfig.Flags.bInConfigMode = TRUE;
		memcpypgm2ram((void*)&AppConfig.MyMACAddr, (ROM void*)SerializedMACAddress, sizeof(AppConfig.MyMACAddr));
//		{
//			_prog_addressT MACAddressAddress;
//			MACAddressAddress.next = 0x157F8;
//			_memcpy_p2d24((char*)&AppConfig.MyMACAddr, MACAddressAddress, sizeof(AppConfig.MyMACAddr));
//		}
		AppConfig.MyIPAddr.Val = MY_DEFAULT_IP_ADDR_BYTE1 | MY_DEFAULT_IP_ADDR_BYTE2<<8ul | MY_DEFAULT_IP_ADDR_BYTE3<<16ul | MY_DEFAULT_IP_ADDR_BYTE4<<24ul;
		AppConfig.DefaultIPAddr.Val = AppConfig.MyIPAddr.Val;
		AppConfig.MyMask.Val = MY_DEFAULT_MASK_BYTE1 | MY_DEFAULT_MASK_BYTE2<<8ul | MY_DEFAULT_MASK_BYTE3<<16ul | MY_DEFAULT_MASK_BYTE4<<24ul;
		AppConfig.DefaultMask.Val = AppConfig.MyMask.Val;
		AppConfig.MyGateway.Val = MY_DEFAULT_GATE_BYTE1 | MY_DEFAULT_GATE_BYTE2<<8ul | MY_DEFAULT_GATE_BYTE3<<16ul | MY_DEFAULT_GATE_BYTE4<<24ul;
		AppConfig.PrimaryDNSServer.Val = MY_DEFAULT_PRIMARY_DNS_BYTE1 | MY_DEFAULT_PRIMARY_DNS_BYTE2<<8ul  | MY_DEFAULT_PRIMARY_DNS_BYTE3<<16ul  | MY_DEFAULT_PRIMARY_DNS_BYTE4<<24ul;
		AppConfig.SecondaryDNSServer.Val = MY_DEFAULT_SECONDARY_DNS_BYTE1 | MY_DEFAULT_SECONDARY_DNS_BYTE2<<8ul  | MY_DEFAULT_SECONDARY_DNS_BYTE3<<16ul  | MY_DEFAULT_SECONDARY_DNS_BYTE4<<24ul;
	
			// Load the default NetBIOS Host Name
		memcpypgm2ram(AppConfig.NetBIOSName, (ROM void*)MY_DEFAULT_HOST_NAME, 16);
		FormatNetBIOSName(AppConfig.NetBIOSName);
		AppConfig.Port=HTTP_PORT;
		strcpy(AppConfig.nome_admin,"admin");
		strcpy(AppConfig.pasw_admin,"admin");


		#if defined(WF_CS_TRIS)
			// Load the default SSID Name
			WF_ASSERT(sizeof(MY_DEFAULT_SSID_NAME) <= sizeof(AppConfig.MySSID));
			memcpypgm2ram(AppConfig.MySSID, (ROM void*)MY_DEFAULT_SSID_NAME, sizeof(MY_DEFAULT_SSID_NAME));
			AppConfig.SsidLength = sizeof(MY_DEFAULT_SSID_NAME) - 1;
	
      AppConfig.SecurityMode = MY_DEFAULT_WIFI_SECURITY_MODE;
      AppConfig.WepKeyIndex  = MY_DEFAULT_WEP_KEY_INDEX;
	        
      #if (MY_DEFAULT_WIFI_SECURITY_MODE == WF_SECURITY_OPEN)
          memset(AppConfig.SecurityKey, 0x00, sizeof(AppConfig.SecurityKey));
          AppConfig.SecurityKeyLength = 0;

      #elif MY_DEFAULT_WIFI_SECURITY_MODE == WF_SECURITY_WEP_40
          memcpypgm2ram(AppConfig.SecurityKey, (ROM void*)MY_DEFAULT_WEP_KEYS_40, sizeof(MY_DEFAULT_WEP_KEYS_40) - 1);
          AppConfig.SecurityKeyLength = sizeof(MY_DEFAULT_WEP_KEYS_40) - 1;
	
      #elif MY_DEFAULT_WIFI_SECURITY_MODE == WF_SECURITY_WEP_104
			    memcpypgm2ram(AppConfig.SecurityKey, (ROM void*)MY_DEFAULT_WEP_KEYS_104, sizeof(MY_DEFAULT_WEP_KEYS_104) - 1);
			    AppConfig.SecurityKeyLength = sizeof(MY_DEFAULT_WEP_KEYS_104) - 1;
	
      #elif (MY_DEFAULT_WIFI_SECURITY_MODE == WF_SECURITY_WPA_WITH_KEY)       || \
            (MY_DEFAULT_WIFI_SECURITY_MODE == WF_SECURITY_WPA2_WITH_KEY)      || \
            (MY_DEFAULT_WIFI_SECURITY_MODE == WF_SECURITY_WPA_AUTO_WITH_KEY)
			    memcpypgm2ram(AppConfig.SecurityKey, (ROM void*)MY_DEFAULT_PSK, sizeof(MY_DEFAULT_PSK) - 1);
			    AppConfig.SecurityKeyLength = sizeof(MY_DEFAULT_PSK) - 1;
	
      #elif (MY_DEFAULT_WIFI_SECURITY_MODE == WF_SECURITY_WPA_WITH_PASS_PHRASE)     || \
            (MY_DEFAULT_WIFI_SECURITY_MODE == WF_SECURITY_WPA2_WITH_PASS_PHRASE)    || \
            (MY_DEFAULT_WIFI_SECURITY_MODE == WF_SECURITY_WPA_AUTO_WITH_PASS_PHRASE)
          memcpypgm2ram(AppConfig.SecurityKey, (ROM void*)MY_DEFAULT_PSK_PHRASE, sizeof(MY_DEFAULT_PSK_PHRASE) - 1);
          AppConfig.SecurityKeyLength = sizeof(MY_DEFAULT_PSK_PHRASE) - 1;
	
      #else 
          #error "No security defined"
      #endif /* MY_DEFAULT_WIFI_SECURITY_MODE */
	
		#endif

		// Compute the checksum of the AppConfig defaults as loaded from ROM
		wOriginalAppConfigChecksum = CalcIPChecksum((BYTE*)&AppConfig, sizeof(AppConfig));

		{
			NVM_VALIDATION_STRUCT NVMValidationStruct;

			// Check to see if we have a flag set indicating that we need to 
			// save the ROM default AppConfig values.
			if(vNeedToSaveDefaults)
				SaveAppConfig(&AppConfig);
		
			// Read the NVMValidation record and AppConfig struct out of EEPROM/Flash
			{
// FINIRE				XEEReadArray(0x0000, (BYTE*)&NVMValidationStruct, sizeof(NVMValidationStruct));
//				XEEReadArray(sizeof(NVMValidationStruct), (BYTE*)&AppConfig, sizeof(AppConfig));
			int i,addr;
			BYTE *p;

			addr=MPFS_RESERVE_BLOCK;
			i=sizeof(NVMValidationStruct);
			p=(BYTE *)&NVMValidationStruct;
			do {
				I2CRead16Seq(addr,min(i,EEPROM_CHUNK));			// 
				memcpy(p,I2CBuffer,min(i,EEPROM_CHUNK));
				addr+=EEPROM_CHUNK;
				p+=EEPROM_CHUNK;
				i-=EEPROM_CHUNK;
				} while(i>0);
			i=sizeof(APP_CONFIG);
			p=(BYTE *)&AppConfig;
			do {
				I2CRead16Seq(addr,min(i,EEPROM_CHUNK));			// 
				memcpy(p,I2CBuffer,min(i,EEPROM_CHUNK));
				addr+=EEPROM_CHUNK;
				p+=EEPROM_CHUNK;
				i-=EEPROM_CHUNK;
				} while(i>0);
			}
	

			// Check EEPROM/Flash validitity.  If it isn't valid, set a flag so 
			// that we will save the ROM default values on the next loop 
			// iteration.
			if((NVMValidationStruct.wConfigurationLength != (sizeof(AppConfig))) ||
			   (NVMValidationStruct.wOriginalChecksum != wOriginalAppConfigChecksum) ||
			   (NVMValidationStruct.wCurrentChecksum != CalcIPChecksum((BYTE*)&AppConfig, sizeof(AppConfig))))
			{
				// Check to ensure that the vNeedToSaveDefaults flag is zero, 
				// indicating that this is the first iteration through the do 
				// loop.  If we have already saved the defaults once and the 
				// EEPROM/Flash still doesn't pass the validity check, then it 
				// means we aren't successfully reading or writing to the 
				// EEPROM/Flash.  This means you have a hardware error and/or 
				// SPI configuration error.
				if(vNeedToSaveDefaults) {

					while(1) {
			//			XEEReadArray(0x0000, (BYTE*)&NVMValidationStruct, sizeof(NVMValidationStruct));
			//			XEEReadArray(sizeof(NVMValidationStruct), (BYTE*)&AppConfig, sizeof(AppConfig));
						ClrWdt();
						}
					}
				
				// Set flag and restart loop to load ROM defaults and save them
				vNeedToSaveDefaults = 1;
				continue;
				}
			
			// If we get down here, it means the EEPROM/Flash has valid contents 
			// and either matches the ROM defaults or previously matched and 
			// was run-time reconfigured by the user.  In this case, we shall 
			// use the contents loaded from EEPROM/Flash.
			break;
			}
		break;
		}
	}

void SaveAppConfig(const APP_CONFIG *ptrAppConfig) {
	NVM_VALIDATION_STRUCT NVMValidationStruct;
	WORD addr;
	int i;
	BYTE*p;

	// Ensure adequate space has been reserved in non-volatile storage to 
	// store the entire AppConfig structure.  If you get stuck in this while(1) 
	// trap, it means you have a design time misconfiguration in TCPIPConfig.h.
	// You must increase MPFS_RESERVE_BLOCK to allocate more space.
	#if defined(STACK_USE_MPFS2)
		if(sizeof(NVMValidationStruct) + sizeof(AppConfig) > MPFS_RESERVE_BLOCK)
			while(1);
	#endif

	// Get proper values for the validation structure indicating that we can use 
	// these EEPROM/Flash contents on future boot ups
	NVMValidationStruct.wOriginalChecksum = wOriginalAppConfigChecksum;
	NVMValidationStruct.wCurrentChecksum = CalcIPChecksum((BYTE*)ptrAppConfig, sizeof(APP_CONFIG));
	NVMValidationStruct.wConfigurationLength = sizeof(APP_CONFIG);
// TOCCO solo il totale... va bene cos�!

	// Write the validation struct and current AppConfig contents to EEPROM/Flash
//FINIRE
//	I2CWritePage16(0,(BYTE*)&NVMValidationStruct, sizeof(NVMValidationStruct));				// 
//	I2CWritePage16(sizeof(NVMValidationStruct),(BYTE*)ptrAppConfig, sizeof(APP_CONFIG));
	addr=MPFS_RESERVE_BLOCK;
	i=sizeof(NVMValidationStruct);
	p=(BYTE *)&NVMValidationStruct;
	do {
		memcpy(I2CBuffer,p,min(i,EEPROM_CHUNK));
9		I2CWritePage16(addr,min(i,EEPROM_CHUNK));				// 
		addr+=EEPROM_CHUNK;
		p+=EEPROM_CHUNK;
		i-=EEPROM_CHUNK;
		} while(i>0);
	i=sizeof(APP_CONFIG);
	p=(BYTE *)ptrAppConfig;
	do {
		memcpy(I2CBuffer,p,min(i,EEPROM_CHUNK));
		I2CWritePage16(addr,min(i,EEPROM_CHUNK));				// 
		addr+=EEPROM_CHUNK;
		p+=EEPROM_CHUNK;
		i-=EEPROM_CHUNK;
		} while(i>0);

// OCCHIO che a scrivere 256byte senza cambiare pagina, si sovrascrive quella corrente!

	}
#endif


/****************************************************************************
  Function:
    DWORD   PIC24RTCCGetDate( void )

  Description:
    This routine reads the date from the RTCC module.

  Precondition:
    The RTCC module has been initialized.


  Parameters:
    None

  Returns:
    DWORD in the format <xx><YY><MM><DD>

  Remarks:
    To catch roll-over, we do two reads.  If the readings match, we return
    that value.  If the two do not match, we read again until we get two
    matching values.

    For the PIC32MX, we use library routines, which return the date in the
    PIC32MX format.
  ***************************************************************************/

#if defined( __C30__ )
DWORD PIC24RTCCGetDate( void ) {
  DWORD_VAL   date1;
  DWORD_VAL   date2;

  do {
    while (RCFGCALbits.RTCSYNC);

    RCFGCALbits.RTCPTR0 = 1;
    RCFGCALbits.RTCPTR1 = 1;
    date1.w[1] = RTCVAL;
    date1.w[0] = RTCVAL;

    RCFGCALbits.RTCPTR0 = 1;
    RCFGCALbits.RTCPTR1 = 1;
    date2.w[1] = RTCVAL;
    date2.w[0] = RTCVAL;

    } while (date1.Val != date2.Val);

  return date1.Val;
	}
#endif


/****************************************************************************
  Function:
    DWORD   PIC24RTCCGetTime( void )

  Description:
    This routine reads the time from the RTCC module.

  Precondition:
    The RTCC module has been initialized.

  Parameters:
    None

  Returns:
    DWORD in the format <xx><HH><MM><SS>

  Remarks:
    To catch roll-over, we do two reads.  If the readings match, we return
    that value.  If the two do not match, we read again until we get two
    matching values.

    For the PIC32MX, we use library routines, which return the time in the
    PIC32MX format.
  ***************************************************************************/

#if defined( __C30__ )
DWORD PIC24RTCCGetTime( void ) {
  DWORD_VAL   time1;
  DWORD_VAL   time2;

  do {
    while (RCFGCALbits.RTCSYNC);

    RCFGCALbits.RTCPTR0 = 1;
    RCFGCALbits.RTCPTR1 = 0;
    time1.w[1] = RTCVAL;
    time1.w[0] = RTCVAL;

    RCFGCALbits.RTCPTR0 = 1;
    RCFGCALbits.RTCPTR1 = 0;
    time2.w[1] = RTCVAL;
    time2.w[0] = RTCVAL;

    } while (time1.Val != time2.Val);

    return time1.Val;
	}
#endif


/****************************************************************************
  Function:
    void PIC24RTCCSetDate( WORD xx_year, WORD month_day )

  Description:
    This routine sets the RTCC date to the specified value.


  Precondition:
    The RTCC module has been initialized.

  Parameters:
    WORD xx_year    - BCD year in the lower byte
    WORD month_day  - BCD month in the upper byte, BCD day in the lower byte

  Returns:
    None

  Remarks:
    For the PIC32MX, we use library routines.
  ***************************************************************************/

#if defined( __C30__ )
void PIC24RTCCSetDate( WORD xx_year, WORD month_day ) {
  
	UnlockRTCC();
  RCFGCALbits.RTCPTR0 = 1;
  RCFGCALbits.RTCPTR1 = 1;
  RTCVAL = xx_year;
  RTCVAL = month_day;
	}
#endif


/****************************************************************************
  Function:
    void PIC24RTCCSetTime( WORD weekDay_hours, WORD minutes_seconds )

  Description:
    This routine sets the RTCC time to the specified value.

  Precondition:
    The RTCC module has been initialized.

  Parameters:
    WORD weekDay_hours      - BCD weekday in the upper byte, BCD hours in the
                                lower byte
    WORD minutes_seconds    - BCD minutes in the upper byte, BCD seconds in
                                the lower byte

  Returns:
    None

  Remarks:
    For the PIC32MX, we use library routines.
  ***************************************************************************/

#if defined( __C30__ )
void PIC24RTCCSetTime( WORD weekDay_hours, WORD minutes_seconds) {

  UnlockRTCC();
  RCFGCALbits.RTCPTR0 = 1;
  RCFGCALbits.RTCPTR1 = 0;
  RTCVAL = weekDay_hours;
  RTCVAL = minutes_seconds;
	}
#endif

/****************************************************************************
  Function:
    void UnlockRTCC( void )

  Description:
    This function unlocks the RTCC so we can write a value to it.

  Precondition:
    None

  Parameters:
    None

  Return Values:
    None

  Remarks:
    For the PIC32MX, we use library routines.
  ***************************************************************************/

#define RTCC_INTERRUPT_REGISTER IEC3
#define RTCC_INTERRUPT_VALUE    0x2000

#if defined( __C30__ )
void UnlockRTCC( void ) {
  BOOL interruptsWereOn;

  interruptsWereOn = FALSE;
  if((RTCC_INTERRUPT_REGISTER & RTCC_INTERRUPT_VALUE) == RTCC_INTERRUPT_VALUE) {
    interruptsWereOn = TRUE;
    RTCC_INTERRUPT_REGISTER &= ~RTCC_INTERRUPT_VALUE;
    }

  // Unlock the RTCC module
  __asm__ ("mov #NVMKEY,W0");
  __asm__ ("mov #0x55,W1");
  __asm__ ("mov #0xAA,W2");
  __asm__ ("mov W1,[W0]");
  __asm__ ("nop");
  __asm__ ("mov W2,[W0]");
  __asm__ ("bset RCFGCAL,#13");
  __asm__ ("nop");
  __asm__ ("nop");

  if(interruptsWereOn) {
    RTCC_INTERRUPT_REGISTER |= RTCC_INTERRUPT_VALUE;
    }
	}

#endif

BYTE to_bcd(BYTE n) {
	
	return (n % 10) | ((n / 10) << 4);
	}

BYTE from_bcd(BYTE n) {
	
	return (n & 15) + (10*(n >> 4));
	}





// ----------------------------------------------------------------------

unsigned char WriteSPI2M(unsigned char data_out) {

#ifdef __PIC32MX__
    BYTE   clear;
    putcSPI((BYTE)data_out);
    clear = getcSPI();
    return ( 0 );                // return non-negative#
#else
    BYTE   clear;
    SPI2BUF = data_out;          // write byte to SSP1BUF register
    while(!SPI2STAT_RBF); // wait until bus cycle complete
    clear = SPI2BUF;
    return 0;                // return non-negative#
#endif
	}

BYTE ReadSPI2M(void) {

#ifdef __C32__
    putcSPI((BYTE)0xFF);
    return (BYTE)getcSPI();
#else
    SPIBUF = 0xFF;                              //Data Out - Logic ones
    while(!SPISTAT_RBF);                     //Wait until cycle complete
    return(SPIBUF);                             //Return with byte read
#endif
	}

void scanKBD(void) {
	BYTE *p,n;
	BYTE i;
	BYTE *rowStatePtr;
	BYTE kbtemp;
	BYTE kbScanY;
	BYTE kbScanCode;		// il num. progressivo del tasto scandito


//return;  //FINIRE!

	ClrWdt();
//	memcpy(oldkbKeys,kbKeys,MAX_TASTI_CONTEMPORANEI);		meglio sopra...
	memset(kbKeys,0,MAX_TASTI_CONTEMPORANEI);


  SPI2CON1 = 0x0000;              // power on state
  SPI2CON1 |= MASTER_ENABLE_ON | PRI_PRESCAL_1_1 | SEC_PRESCAL_1_1;          // select serial mode 
  SPI2CON1bits.CKP = 1;
  SPI2CON1bits.CKE = 0;
  SPI2CLOCK = 0;
  SPI2OUT = 0;                  // define SDO1 as output (master or slave)
  SPI2IN = 1;                  // define SDI1 as input (master or slave)
  SPI2ENABLE = 1;             // enable synchronous serial port


	p=(BYTE *)rowState;

	SPI2_CS=0;
	WriteSPI2M(1);
	for(i=0; i<32; i++) {
		*p++=ReadSPI2M();
		}
	SPI2_CS=1;

	kbScanCode=1;			// parto da 1!! (se no lo zero=non valido taglierebbe via ROW0:COL0)
	for(i=0; i<10; i++) {					// analizzo i tasti meccanici
		ClrWdt();

		if(rowState[i])
			checkKey(kbScanCode);

		kbScanCode++;
		}			// for(rowStatePtr...

	if(memcmp(kbKeys,oldkbKeys,MAX_TASTI_CONTEMPORANEI))
		FLAGS |= (1 << TRIGK);

	return;


	TRISBbits.TRISB4=1;
	TRISBbits.TRISB5=1;
	TRISEbits.TRISE9=1;
	TRISEbits.TRISE8=1;
	TRISAbits.TRISA0=1;

	LATBbits.LATB3=0;		// prima, riscrivo output...
	LATBbits.LATB2=0;
	LATBbits.LATB1=0;
	LATBbits.LATB0=0;
	LATGbits.LATG13=0;

	TRISBbits.TRISB3=1;
	TRISBbits.TRISB2=1;
	TRISBbits.TRISB1=1;
	TRISBbits.TRISB0=1;
	TRISGbits.TRISG13=1;

	kbScanY=0b00011110;
	kbScanCode=1;			// parto da 1!! (se no lo zero=non valido taglierebbe via ROW0:COL0)
	

// faccio la scansione, scrivendo nelle colonne e leggendo dalle righe;
//		poi salvo i valori nell'array da 8 byte rowState

	do {

		switch(kbScanY) {
			case 0b00011110:
				TRISBbits.TRISB3=0;
				break;
			case 0b00011101:
				TRISBbits.TRISB2=0;
				break;
			case 0b00011011:
				TRISBbits.TRISB1=0;
				break;
			case 0b00010111:
				TRISBbits.TRISB0=0;
				break;
			case 0b00001111:
				TRISGbits.TRISG13=0;
				break;
			}

		__delay_us(2);									// ritardino...
		ClrWdt();

		n=0;
		n |= PORTBbits.RB4;
		n |= (PORTBbits.RB5 << 1);
		n |= (PORTEbits.RE9 << 2);
		n |= (PORTEbits.RE8 << 3);
		n |= (PORTAbits.RA0 << 4);

		*p++=n;

		kbScanY <<= 1;
		kbScanY |= 1;								// entra 1
		kbScanY &= 0b00011111;

		TRISBbits.TRISB3=1;
		TRISBbits.TRISB2=1;
		TRISBbits.TRISB1=1;
		TRISBbits.TRISB0=1;
		TRISGbits.TRISG13=1;

		} while(kbScanY != 0b00011111);

// risistemiamo la situaz. uscite
	TRISBbits.TRISB3=1;
	TRISBbits.TRISB2=1;
	TRISBbits.TRISB1=1;
	TRISBbits.TRISB0=1;
	TRISGbits.TRISG13=1;




// ora vado a controllare i tasti premuti, facendo anche attenzione ai tasti ripetuti:
//		non devono essere piu' di 2 nella colonna 7 (modifier)
//		 e in generale non devono esserci tasti fantasma (quelli "a quadrato")

/*SetColor(BLACK);
LCDCls();
LCDPutString(":");*/
	for(rowStatePtr=rowState; rowStatePtr < (rowState+5); rowStatePtr++) {	// puntatore alle letture fatte prima


/*{char mybuf[8];
sprintf(mybuf,"%02X ",*rowStatePtr);
LCDPutString(mybuf);
}*/
		for(kbtemp=1; kbtemp!=0b00100000; kbtemp <<= 1 /* entra 0 */) {					// analizzo le righe...
			ClrWdt();

			if(!(kbtemp & *rowStatePtr))
				checkKey(kbScanCode);

			kbScanCode++;
			} // 5 righe

		}			// for(rowStatePtr...


/*{char mybuf[8];
int i;
LCDPutString("; ");
for(i=0; i<MAX_TASTI_CONTEMPORANEI; i++) {
sprintf(mybuf,"%u ",kbKeys[i]);
LCDPutString(mybuf);
}
}
LCDPutString("\n");*/

	if(memcmp(kbKeys,oldkbKeys,MAX_TASTI_CONTEMPORANEI))
		FLAGS |= (1 << TRIGK);

	}

BYTE checkKey(BYTE kbScanCode) {
	BYTE i;

	for(i=0; i<MAX_TASTI_CONTEMPORANEI; i++) {
		ClrWdt();
		if(!kbKeys[i]) {				//	cerco un posto vuoto x metterci lo scan code
			kbKeys[i]=kbScanCode;
			return 1;
			}
		}

	checkKeys_doError();
	return 0;
	}

void checkKeys_doError(void) {	// non c'e' piu' posto (max 4 tasti contemporanei!) DARE ERRORE??

// suonare male!
	}

void setKBDLed(void) {
	BYTE *p,n;
	BYTE i;

	ClrWdt();

  SPI2CON1 = 0x0000;              // power on state
  SPI2CON1 |= MASTER_ENABLE_ON | PRI_PRESCAL_1_1 | SEC_PRESCAL_1_1;          // select serial mode 
  SPI2CON1bits.CKP = 1;
  SPI2CON1bits.CKE = 0;
  SPI2CLOCK = 0;
  SPI2OUT = 0;                  // define SDO1 as output (master or slave)
  SPI2IN = 1;                  // define SDI1 as input (master or slave)
  SPI2ENABLE = 1;             // enable synchronous serial port



	SPI2_CS=0;
	WriteSPI2M(1);
	for(i=0; i<32; i++) {
		WriteSPI2M(0);
		}
	SPI2_CS=1;
	}

	
void SetTextColor(BYTE r,BYTE g,BYTE b) {

//	fontColor=RGBConvert(r,g,b);		//
//	SetColor(fontColor);
	}

void SetBkColor(BYTE r,BYTE g,BYTE b) {

//	bkColor=RGBConvert(r,g,b);		// 
//	_backcolor=bkColor;
	}

void SetTextColorPalette(int n) {

//	fontColor=textColors[n & 15];
//	SetColor(fontColor);
	}

void SetBkColorPalette(int n) {

//	bkColor=textColors[n & 15];
//	_backcolor=bkColor;
	}



void SetPWM(BYTE q,BYTE val) {
	WORD n;

	statoPWM[q]=val;

	n=70000000L/PWM_FREQ;

	switch(q) {
		case 0:
		  /* set PWM duty cycle */
		  OC1R    = (val*(DWORD)n)/100; 
		  OC1RS   = n;
			break;
		
		case 1:
		  OC2R    = (val*(DWORD)n)/100; 
		  OC2RS   = n;
			break;
		}
	}

void ModBusSetRele(int m) {			// lasciata tanto per provare... :)

	}

signed char ModBusSetMotore(unsigned int m) {

	}

int ModBusReadMotoreState(void) {

	}

int ModBusReadMotoreTemperature(void) {

	}


int ModbusSendRTU(BYTE dst,BYTE func,WORD reg,WORD val) {
	}

char ModbusReceiveRTU(unsigned char *response, BYTE maxLen) {
	}



void charPause35(void) {

	ClrWdt();
	}


// usare cmd :  {FB;2A;<Length>|C8:THR=10|<Crc>} sets the hour to 16.		(pag.6 solarmax_protocol)
// e risp. {2A;FB;<Length>|C8:Ok|<Crc>} or {2A;FB;<Length>|C8:Ko|<Crc>}

WORD getChksum16(BYTE *p,BYTE len) {		//CRC16
	WORD c;
	int i,j;

	c=0xffff;
	while(len--) {
		c ^= *p;
		for(i=0; i<8; i++) {
			j=c & 1;
			c >>= 1;
			if(j) {
				c ^= 0xa001;
				}
			}

		p++;
		}

	return c;
	}

WORD getChksumASCII(BYTE *p,int len) {		//LRC, non usiamo qua
	WORD c;

	c=0;
	while(len--) {
		c += *p;
		p++;
		}

	return c;
	}


#include <math.h>
double IEEE754_2_double(DWORD d) {
	double f;
	DWORD i,j;
	BYTE exponent;

	struct IEEE754 {			// RIGHT-aligned!
		unsigned long mantissa:23;
		unsigned long exponent:8;
		unsigned long sign:1;
		} __attribute__ ((packed))  d1;

	*(DWORD *)&d1=d;

	exponent=d1.exponent-127;

	f=0.0;
	f=pow(2,exponent);		// 1 implicito
	for(i=exponent-1, j=0x400000; i>=0; i--, j>>=1) {
		if(d1.mantissa & j)
			f += pow(2,i);
		}
	for(; j>0; i--,j>>=1) {
		if(d1.mantissa & j)
			f += pow(2,i);
		}

	if(d1.sign)
		f=-f;
	
	return f;
	}

double IEEE754_64_2_double(DWORD d) { //finire!
	double f;
	DWORD i,j;
	BYTE exponent;

	struct IEEE754 {			// RIGHT-aligned!
		unsigned long mantissa:23;
		unsigned long exponent:8;
		unsigned long sign:1;
		} __attribute__ ((packed))  d1;

	*(DWORD *)&d1=d;

	exponent=d1.exponent-127;

	f=0.0;
	f=pow(2,exponent);		// 1 implicito
	for(i=exponent-1, j=0x400000; i>=0; i--, j>>=1) {
		if(d1.mantissa & j)
			f += pow(2,i);
		}
	for(; j>0; i--,j>>=1) {
		if(d1.mantissa & j)
			f += pow(2,i);
		}

	if(d1.sign)
		f=-f;
	
	return f;
	}


long BilanciaLeggiPeso(void) {
	}

long BilanciaTara(void) {
	}

int BilanciaSend(BYTE dst,char *func,BYTE doQuery,WORD parm1) {

	}

BYTE BilanciaReceive(BYTE isBool,long *value) {

	}


//-------------------------------------------------------------------------------------

static FSFILE *f;
WORD ExternalMemoryCallback(GFX_EXTDATA* memory, LONG offset, WORD nCount, void* buffer) {
	WORD n=0;

//	LED2_IO = 1;
	if(memory) {
		if(!f)
			f=FSfopen((char *)memory,FS_READ);			// provo a cache-are e lasciare aperto...
		if(f) {
//			if(f->seek  != offset)		// ottimizzo NO V. SOTTO
				FSfseek(f,offset,SEEK_SET);
			n=FSfread(buffer,1,nCount,f);
			}
		}
	else {
		FSfclose(f);
		f=NULL;
		}
//	LED2_IO = 0;
	return n;
	}

WORD ExternalMemoryCallbackCont(WORD nCount, void* buffer) {

//	LED2_IO ^= 1;
	if(f) {
		return FSfread(buffer,1,nCount,f);
		}
	return 0;
	}

void setTimerBuzzer(int mSec) {

#ifdef USA_BUZZER_PWM 		// se no � on-off
	OC2CON1 |= 0x0006;   // on
#else
	LATGbits.LATG15=1;   // on
#endif

	OpenTimer4(T4_ON & T4_IDLE_CON & T4_GATE_OFF & T4_32BIT_MODE_OFF & T4_PS_1_256 & T4_SOURCE_INT,
		mSec*64);		//mSec! (il timer va a 64uSec)
//	ConfigIntTimer4(T4_INT_PRIOR_4 & T4_INT_ON);

	EnableIntT4;
	}






/****************************************************************************
  Function:
    BOOL USB_ApplicationEventHandler( BYTE address, USB_EVENT event,
                void *data, DWORD size )

  Summary:
    This is the application event handler.  It is called when the stack has
    an event that needs to be handled by the application layer rather than
    by the client driver.

  Description:
    This is the application event handler.  It is called when the stack has
    an event that needs to be handled by the application layer rather than
    by the client driver.  If the application is able to handle the event, it
    returns TRUE.  Otherwise, it returns FALSE.

  Precondition:
    None

  Parameters:
    BYTE address    - Address of device where event occurred
    USB_EVENT event - Identifies the event that occured
    void *data      - Pointer to event-specific data
    DWORD size      - Size of the event-specific data

  Return Values:
    TRUE    - The event was handled
    FALSE   - The event was not handled

  Remarks:
    The application may also implement an event handling routine if it
    requires knowledge of events.  To do so, it must implement a routine that
    matches this function signature and define the USB_HOST_APP_EVENT_HANDLER
    macro as the name of that function.
  ***************************************************************************/

BOOL USB_ApplicationEventHandler( BYTE address, USB_EVENT event, void *data, DWORD size ) {
  
	switch(event) {
        case EVENT_VBUS_REQUEST_POWER:
            // The data pointer points to a byte that represents the amount of power
            // requested in mA, divided by two.  If the device wants too much power,
            // we reject it.
            return TRUE;

        case EVENT_VBUS_RELEASE_POWER:
            // Turn off Vbus power.
            // The PIC24F with the Explorer 16 cannot turn off Vbus through software.

            //This means that the device was removed
            deviceAttached = FALSE;
            return TRUE;
            break;

        case EVENT_HUB_ATTACH:
            return TRUE;
            break;

        case EVENT_UNSUPPORTED_DEVICE:
            return TRUE;
            break;

        case EVENT_CANNOT_ENUMERATE:
            //UART2PrintString( "\r\n***** USB Error - cannot enumerate device *****\r\n" );
            return TRUE;
            break;

        case EVENT_CLIENT_INIT_ERROR:
            //UART2PrintString( "\r\n***** USB Error - client driver initialization error *****\r\n" );
            return TRUE;
            break;

        case EVENT_OUT_OF_MEMORY:
            //UART2PrintString( "\r\n***** USB Error - out of heap memory *****\r\n" );
            return TRUE;
            break;

        case EVENT_UNSPECIFIED_ERROR:   // This should never be generated.
            //UART2PrintString( "\r\n***** USB Error - unspecified *****\r\n" );
            return TRUE;
            break;

        default:
            break;
    }

  return FALSE;
	}


//---------------------------------------------------------------------------------------------------
#ifdef USA_JPEG
#include "jpeg/cdjpeg.h"		/* Common decls for cjpeg/djpeg applications */
#include "jpeg/jversion.h"		/* for version message */
//#include "jpeg/jpeglib.h"

BYTE *BuildJPEG(void* bitmap, BYTE stretch, BOOL reverseV, int quality) {
//FINIRE se serve!
	register int i;
  BYTE *d=NULL,*s;
	DWORD l;
	JSAMPROW rowPointer[1];
  int rowStride;
  struct jpeg_decompress_struct srcinfo;
  struct jpeg_compress_struct dstinfo;
	
  jpeg_create_compress(&dstinfo);

//		set_dest(d,b.bmWidth*b.bmHeight*3+5000);

//	dstinfo.image_width=b.bmWidth;
//	dstinfo.image_height=b.bmHeight;
	dstinfo.input_components=3;
	dstinfo.in_color_space=JCS_RGB;

	jpeg_set_defaults(&dstinfo);

	jpeg_set_quality(&dstinfo,quality,TRUE);

	jpeg_start_compress(&dstinfo,TRUE);

	rowStride = dstinfo.image_width * 3;

	if(reverseV) {
		while(dstinfo.next_scanline< dstinfo.image_height) {
			rowPointer[0]= &s[(dstinfo.image_height-dstinfo.next_scanline-1)*rowStride];
			jpeg_write_scanlines(&dstinfo,rowPointer, 1);
			}
		}
	else {
		while(dstinfo.next_scanline < dstinfo.image_height) {
			rowPointer[0]= &s[dstinfo.next_scanline*rowStride];
			jpeg_write_scanlines(&dstinfo,rowPointer, 1);
			}
		}

//		co->finishCompress();
	jpeg_finish_compress(&dstinfo);
//		*len=dstinfo.dest->data_count;
//		destroyCompress();
  jpeg_destroy_compress(&dstinfo);

	return d;
	}


BYTE *PutJPEGFile(SHORT left, SHORT top, const char *input_file, 
									BITMAPINFOHEADER *bmp, BYTE *theBytes,
									int colorBits,int ditherMode,BOOL reverseV) {

  /* We use our private extension JPEG error handler.
   * Note that this struct must live as long as the main JPEG parameter
   * struct, to avoid dangling-pointer problems.
   */
  /* More stuff */
  struct jpeg_decompress_struct srcinfo;
//  JSAMPARRAY buffer;		/* Output row buffer */
	JSAMPROW rowPointer[1];
  int rowStride,rowStrideTrue;		/* physical row width in output buffer, e corretta DWORD */
	BYTE *myBuf,*p;


  /* Step 1: allocate and initialize JPEG decompression object */

  /* We set up the normal JPEG error routines, then override error_exit. */
  //cinfo.err = jpeg_std_error(&jerr.pub);
  //jerr.pub.error_exit = my_error_exit;

  /* Now we can initialize the JPEG decompression object. */
  jpeg_create_decompress(&srcinfo);

  /* Step 2: specify data source (eg, a file) */

  jpeg_stdio_src(&srcinfo, input_file);
//  set_source(f);
//  stdioSrc(infile);

  /* Step 3: read file parameters with jpeg_read_header() */

  jpeg_read_header(&srcinfo,TRUE);
  /* We can ignore the return value from jpeg_read_header since
   *   (a) suspension is not possible with the stdio data source, and
   *   (b) we passed TRUE to reject a tables-only JPEG file as an error.
   * See libjpeg.doc for more info.
   */

  /* Step 4: set parameters for decompression */

  /* In this example, we don't need to change any of the defaults set by
   * jpeg_read_header(), so we do nothing here.
   */

	srcinfo.desired_number_of_colors = 1 << colorBits;
	if(ditherMode)
		srcinfo.dither_mode=(J_DITHER_MODE)ditherMode;

  /* Step 5: Start decompressor */

  jpeg_start_decompress(&srcinfo);
  /* We can ignore the return value since suspension is not possible
   * with the stdio data source.
   */



  /* We may need to do some setup of our own at this point before reading
   * the data.  After jpeg_start_decompress() we have the correct scaled
   * output image dimensions available, as well as the output colormap
   * if we asked for color quantization.
   * In this example, we need to make an output work buffer of the right size.
   */ 
  /* JSAMPLEs per row in output buffer */
  rowStride = srcinfo.output_width * srcinfo.output_components;
	rowStrideTrue=(rowStride + 3) & 0xfffffffc;			// bitmap dovra' essere DWORD-aligned
	bmp->biSize=sizeof(BITMAPINFOHEADER);
	bmp->biBitCount=srcinfo.output_components*8;
	bmp->biPlanes=1			/* de-> */ ;
	bmp->biWidth=srcinfo.output_width;
	bmp->biHeight=srcinfo.output_height;
	bmp->biSizeImage=rowStrideTrue*srcinfo.output_height;
	bmp->biClrUsed=bmp->biClrImportant=srcinfo.desired_number_of_colors;
	bmp->biCompression=0;
	bmp->biXPelsPerMeter=bmp->biYPelsPerMeter=0;

  if(bmp) {
		theBytes=(BYTE *)malloc(bmp->biSizeImage);
		if(reverseV)
			p=theBytes+bmp->biSizeImage-rowStrideTrue;
		else
			p=theBytes;
		}

  /* Make a one-row-high sample array that will go away when done with image */
  myBuf=(BYTE *)malloc(rowStride);
	rowPointer[0]=myBuf;

  /* Step 6: while (scan lines remain to be read) */
  /*           jpeg_read_scanlines(...); */

  /* Here we use the library's state variable cinfo.output_scanline as the
   * loop counter, so that we don't have to keep track ourselves.
   */
  while(srcinfo.output_scanline < srcinfo.output_height) {
    /* jpeg_read_scanlines expects an array of pointers to scanlines.
     * Here the array is only one element long, but you could ask for
     * more than one scanline at a time if that's more convenient.
     */
    jpeg_read_scanlines(&srcinfo,rowPointer, 1);
    /* Assume put_scanline_someplace wants a pointer and sample count. */
//    put_scanline_someplace(buffer[0], row_stride);

		SetArea(left, top+srcinfo.output_scanline /*y_inc dovrebbe andare*/, GetMaxX(), GetMaxY());
//		memcpy(p,myBuf,rowStride);		to buffer...
		WriteCommand(CMD_WR_MEMSTART);
		DisplayEnable();
		DisplaySetData(); 
		DWORD temp,*pData=myBuf;
		WORD x;
		for(x=0; x<srcinfo.output_width; x++){
			temp = *pData++;
			SetColor(RGBConvert2416(temp)); 
			// Write pixel to screen       
//			for(stretchX=0; stretchX<stretch; stretchX++) {
			#if defined(USE_16BIT_PMP)
				WriteData(_color);
			#else			//if defined(USE_8BIT_PMP)
				WriteColor(_color);
			#endif
//				}
			}
		DisplayDisable();

	  if(bmp) {
			if(reverseV)
				p-=rowStrideTrue;
			else
				p+=rowStrideTrue;
			}
		}

	free(myBuf);

  /* Step 7: Finish decompression */

  jpeg_finish_decompress(&srcinfo);
  /* We can ignore the return value since suspension is not possible
   * with the stdio data source.
   */

  /* Step 8: Release JPEG decompression object */

  /* This is an important step since it will release a good deal of memory. */
  jpeg_destroy_decompress(&srcinfo);

  /* After finish_decompress, we can close the input file.
   * Here we postpone it until after no more JPEG errors are possible,
   * so as to simplify the setjmp error logic above.  (Actually, I don't
   * think that jpeg_destroy can do an error exit, but why assume anything...)
   */
//  fclose(infile);
	ExternalMemoryCallback(NULL,0,0,NULL);		// pulisco buffer

  /* At this point you may want to check to see whether any corrupt-data
   * warnings occurred (test whether jerr.pub.num_warnings is nonzero).
   */

  /* And we're done! */
  return theBytes;
	}


#endif


const struct LCD_ITEM lcdItems[ULTIMA_SCHERMATA_PROGRAMMAZIONE-PRIMA_SCHERMATA_PROGRAMMAZIONE] = 
{
//	BYTE x,y;	BYTE size;	WORD valMin,valMax;	WORD *var;	BYTE varSize;	char *name;
//varsize=0 e val min/max 0..1 indicano ON/OFF!
	0,0,11,0,50,(WORD *)&configParms.tVibro1[0],1,"Vibro1/pre:","d.s",		// v. sopra
	0,1,11,0,50,(WORD *)&configParms.tVibro1[1],1,"Vibro1/post:","d.s",
	0,2,11,0,50,(WORD *)&configParms.tVibro2[0],1,"Vibro2/pre:","d.s",
	0,3,11,0,50,(WORD *)&configParms.tVibro2[1],1,"Vibro2/post:","d.s",
	0,4,11,0,1,(WORD *)&configParms.mVibro1[0],0, "Vibro1\r\nristretto:",NULL,
	0,5,11,0,1,(WORD *)&configParms.mVibro1[1],0, "Vibro1\r\n1 tazza:",NULL,
	0,6,11,0,1,(WORD *)&configParms.mVibro1[2],0, "Vibro1\r\n2 tazze:",NULL,
	0,7,11,0,1,(WORD *)&configParms.mVibro1[3],0, "Vibro1\r\n1/4 sacco:",NULL,
	0,8,11,0,1,(WORD *)&configParms.mVibro1[4],0, "Vibro1\r\n1/2 sacco:",NULL,
	0,9,11,0,1,(WORD *)&configParms.mVibro1[5],0, "Vibro1\r\n1 sacco:",NULL,
	0,10,11,0,1,(WORD *)&configParms.mVibro2[0],0,"Vibro2\r\nristretto:",NULL,
	0,11,11,0,1,(WORD *)&configParms.mVibro2[1],0,"Vibro2\r\n1 tazza:",NULL,
	0,12,11,0,1,(WORD *)&configParms.mVibro2[2],0,"Vibro2\r\n2 tazze:",NULL,
	0,13,11,0,1,(WORD *)&configParms.mVibro2[3],0,"Vibro2\r\n1/4 sacco:",NULL,
	0,14,11,0,1,(WORD *)&configParms.mVibro2[4],0,"Vibro2\r\n1/2 sacco:",NULL,
	0,15,11,0,1,(WORD *)&configParms.mVibro2[5],0,"Vibro2\r\n1 sacco:",NULL,
//questi passano a centesimi/sec
	0,16,14,0,30000,(WORD *)&configParms.tMacinatura[0],2,"Tempo macinaristretto:","c.s",		// 12 char per riga
	0,17,14,0,30000,(WORD *)&configParms.tMacinatura[1],2,"Tempo macina1 tazza:","c.s",
	0,18,14,0,30000,(WORD *)&configParms.tMacinatura[2],2,"Tempo macina2 tazze:","c.s",
	0,19,14,0,30000,(WORD *)&configParms.tMacinatura[3],2,"Tempo macina1/4 sacco:","c.s",
	0,20,14,0,30000,(WORD *)&configParms.tMacinatura[4],2,"Tempo macina1/2 sacco:","c.s",
	0,21,14,0,30000,(WORD *)&configParms.tMacinatura[5],2,"Tempo macina1 sacco:","c.s",

	0,22,13,300,1500,(WORD *)&configParms.vMacinatura,2,"Vel.macina:","rpm",
	0,23,7,0,100,(WORD *)&configParms.vVibro[0],1,"Vel.vibro1:","%",
	0,24,7,0,100,(WORD *)&configParms.vVibro[1],1,"Vel.vibro2:","%",
	0,25,6,0,1,(WORD *)&configParms.needPortafiltro,0,"Filtro:",NULL,	
	0,26,8,0,1,(WORD *)&configParms.needSacchetto,0,"Sacchetto:",NULL,
// inserire ioni negativi
	0,27,10,0,1,(WORD *)&configParms.test,0,"SELF TEST:",NULL,
	0,28,10,0,2,(WORD *)&configParms.backLight,1,"Backlight:",NULL,
	0,29,10,0,2,(WORD *)&configParms.backLight,1,"Contrasto:",NULL,
	0,30,14,0,1,(WORD *)&configParms.setupEnabled,0,"SETUP ENABLED:",NULL,
	0,31,14,0,1,(WORD *)&configParms.splash,0,"Splash screen:",NULL
	};

